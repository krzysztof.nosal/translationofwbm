export declare enum VersionComparisonResult {
    Higher = 1,
    Equal = 0,
    Lower = -1
}
declare const _default: (version: string, toVersion: string) => VersionComparisonResult;
/**
 * Compares two versions. Versions are Equal if their common higher version
 * components are equal. I.e. '1.0.1' is equal to '1.0', as '1.0' is interpreted
 * as '1.0.x'.
 *
 * @param version   Dot-notated version string like 0.2.3 or 1.24.99.2
 * @param toVersion Version to compare to
 *
 * @returns The result of the comparision.
 *
 * @throws {TypeError}, when the given strings are not valid version strings
 */
export default _default;
