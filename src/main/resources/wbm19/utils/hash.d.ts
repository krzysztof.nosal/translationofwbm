/**
 * Hash code creation for a string
 *
 * @see https://stackoverflow.com/a/7616484
 *
 * @param value The string to create a hash for
 */
export default function hash(value: string): number;
