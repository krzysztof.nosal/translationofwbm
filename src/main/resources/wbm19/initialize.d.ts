import { Core } from "wbm-core/core";
import { Base } from "wbm-base/base";
export default function initialize(core: Core, base: Base): Promise<void>;
