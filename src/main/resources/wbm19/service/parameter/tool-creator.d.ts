import { IdToMapping, IdTo, IdToLoadTool, IdToPostProcessor, IdToSaveTool, IdToPreProcessor, IdToCallTool } from "./types/id-mapping-types";
import { StatusError } from "wbm-base/type/error";
export interface ConstKeyValues {
    [constKey: string]: string;
}
export declare type IdToConstKeyValues = IdTo<ConstKeyValues | StatusError>;
export declare type IdToCommand = IdTo<Command | StatusError>;
export declare type IdToConstantIds = IdTo<string[] | StatusError>;
export interface Command {
    command: string;
    multiline: boolean;
    noReturn: boolean;
}
export declare type VarKeyValues = ConstKeyValues;
export declare type IdToVarKeyValues = IdTo<VarKeyValues | StatusError>;
export declare class PfcToolCreator {
    static resolveCommand(rawCommand: string, constValuesForKey: ConstKeyValues): string | StatusError;
    static getConstantIds(ids: string[], idToMapping: IdToMapping): IdToConstantIds;
    static resolveConstants(ids: string[], idToMapping: IdToMapping): Promise<IdToConstKeyValues>;
    private static resolveConstantIds;
    private static generateConstKeyValues;
    private static replaceAsteriskWithNumberInCommand;
    static createCommand(ids: string[], idToMapping: IdToMapping, idToConstKeyValues: IdToConstKeyValues, idToVarValues?: IdToVarKeyValues): IdToCommand;
    static mergeToLoadTool(ids: string[], idToCommand: IdToCommand, idToPostprocessor: IdToPostProcessor): IdToLoadTool;
    static mergeToSaveTool(ids: string[], idToCommand: IdToCommand, idToPreProcessor: IdToPreProcessor): IdToSaveTool;
    static mergeToCallTool(ids: string[], idToCommand: IdToCommand, idToPreProcessor: IdToPreProcessor): IdToCallTool;
    static resolveIdsForConstKeyValues(ids: string[], idToConstKeyValues: IdToConstKeyValues): string[];
    static resolveMappingsForIds(resolvedIds: string[], idToMapping: IdToMapping): IdToMapping;
    static mergeIdsAndResolvedIds(ids: string[], resolvedIds: string[]): string[];
    static createLoadTool(ids: string[], idToMapping: IdToMapping): Promise<IdToLoadTool>;
    static createSaveTool(ids: string[], idToMapping: IdToMapping): Promise<IdToSaveTool>;
    static createCallTool(ids: string[], idToMapping: IdToMapping): Promise<IdToCallTool>;
}
