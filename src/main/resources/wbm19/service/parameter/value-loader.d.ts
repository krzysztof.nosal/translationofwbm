import { Mapping } from "./types/mapping";
import { Cache } from "./cache";
import { IdToLoadTool, IdTo, IdToValue } from "./types/id-mapping-types";
import { StatusError } from "wbm-base/type/error";
import { IdToExecutionResult } from "./command-executor";
export declare type IdToRawValue = IdTo<string | StatusError>;
export declare class PfcValueLoader {
    private static temporaryValueCache?;
    static createTemporaryCache(): void;
    static deleteTemporaryCache(): void;
    static checkTemporaryCache(ids: string[]): {
        cachedIds: string[];
        notCachedIds: string[];
    };
    static loadValues(ids: string[], mappingCache: Cache<Mapping[]>, valueCache: Cache<any>, renewSession?: boolean): Promise<IdToValue>;
    static generateLoadToolsFromMappingCache(ids: string[], mappingCache: Cache<Mapping[]>, valueCache: Cache<any>): Promise<IdToLoadTool>;
    static getRawValuesFromExecutionResults(ids: string[], idToExecutionResult: IdToExecutionResult): IdToRawValue;
    static getValuesForRawValues(ids: string[], rawValuesForIds: IdToRawValue, loadToolsForId: IdToLoadTool): IdToValue;
}
