import { IdToValue } from "./types/id-mapping-types";
import { Mapping } from "service/parameter/types/mapping";
import { Parameter } from "wbm-base/type/parameter/parameter";
import { ValueCache } from "./types/cache-types";
export declare enum ValueCacheUsage {
    never = 0,
    strict = 1,
    always = 2
}
export declare class PfcReader {
    private valueCache;
    private parameterInfos;
    private mappingCache;
    private static _singleton;
    constructor(valueCache: ValueCache, mappings: Mapping[], parameters: Parameter[]);
    static readonly singleton: PfcReader;
    private createMappingCache;
    private useTempValueCache;
    private runningRequest?;
    read(ids: string[], cacheUsage?: ValueCacheUsage, renewSession?: boolean): Promise<IdToValue>;
    private updateValueCache;
    private checkParameterExistsInCache;
    private valueCacheLookUp;
    getParameterInfoFromCache(id: string): Parameter;
    private removeIdsFoundInValueCache;
    private checkIdExistsInParameter;
}
