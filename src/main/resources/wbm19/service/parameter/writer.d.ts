import { IdToValue } from "./types/id-mapping-types";
import { Mapping } from "service/parameter/types/mapping";
import { Parameter } from "wbm-base/type/parameter/parameter";
import { ValueCache } from "./types/cache-types";
export declare enum WriteMode {
    cacheOnly = 0,
    commit = 1
}
export declare enum CacheLookUpMode {
    current = 0,
    original = 1
}
export declare enum CacheUpdateMode {
    current = 0,
    original = 1,
    both = 2
}
export declare enum CacheLookUpCondition {
    always = 0,
    strict = 1
}
export declare class PfcWriter {
    private valueCache;
    private parameterInfos;
    private mappingCache;
    private static _singleton;
    constructor(valueCache: ValueCache, mappings: Mapping[], parameters: Parameter[]);
    static readonly singleton: PfcWriter;
    private createMappingCache;
    private runningRequest?;
    write(ids: string[], values?: IdToValue, mode?: WriteMode): Promise<IdToValue>;
    private updateValueCache;
    private valueCacheLookUp;
    getParameterInfoFromCache(id: string): Parameter;
    private checkParameterExistsInCache;
}
