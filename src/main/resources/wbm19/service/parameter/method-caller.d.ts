import { Value } from "./types/id-mapping-types";
import { Mapping } from "service/parameter/types/mapping";
import { Method } from "wbm-base/type/parameter/method";
import { StatusError } from "wbm-base/type/error";
import { VarKeyValues } from "./tool-creator";
import { ValueCache } from "./types/cache-types";
export declare class PfcMethodCaller {
    private valueCache;
    private methodInfos;
    private mappingCache;
    private static _singleton;
    constructor(valueCache: ValueCache, mappings: Mapping[], methods: Method[]);
    static readonly singleton: PfcMethodCaller;
    private createMappingCache;
    private runningRequest?;
    exec(id: string, renewSession?: boolean, args?: VarKeyValues): Promise<Value | StatusError>;
}
