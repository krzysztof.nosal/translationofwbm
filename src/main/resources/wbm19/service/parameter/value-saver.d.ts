import { Mapping } from "./types/mapping";
import { Cache } from "./cache";
import { IdToValue } from "./types/id-mapping-types";
import { IdToExecutionResult } from "./command-executor";
import { ValueCache } from './types/cache-types';
export declare class PfcValueSaver {
    static saveValues(ids: string[], mappingCache: Cache<Mapping[]>, valueCache: ValueCache, renewSession?: boolean): Promise<IdToValue>;
    static getValuesForExecutionResults(ids: string[], idToExecutionResult: IdToExecutionResult, valueCache: ValueCache): IdToValue;
}
