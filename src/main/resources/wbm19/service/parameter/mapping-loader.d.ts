import { Mapping } from './types/mapping';
import { Cache } from './cache';
import { StatusError } from 'wbm-base/type/error';
import { IdToMapping, IdTo, IdToId } from './types/id-mapping-types';
export declare type Score = number;
export declare namespace Score {
    const bestPossible: Score;
    const invalid: Score;
}
export declare type IdToRawMappings = IdTo<Mapping[] | StatusError>;
export declare type IdToMappingScores = IdTo<Score[] | StatusError>;
export declare type IdToConstCounts = IdTo<number[] | StatusError>;
export declare enum MappingType {
    load = 0,
    save = 1,
    method = 2
}
export declare class PfcMappingLoader {
    static getLoadMappingForIds(ids: string[], mappingCache: Cache<Mapping[]>, valueCache: Cache<any>): IdToMapping;
    static getSaveMappingForIds(ids: string[], mappingCache: Cache<Mapping[]>, valueCache: Cache<any>): IdToMapping;
    static getCallMappingForIds(ids: string[], mappingCache: Cache<Mapping[]>, valueCache: Cache<any>): IdToMapping;
    static getNumericComponentFromIds(ids: string[]): IdTo<number>;
    static replacePlaceholdersInMappingWithIndex(mapping: Mapping, index: number): void;
    static resolvePlaceholderInMapping(ids: string[], idsToMappings: IdToRawMappings): IdToRawMappings;
    static getAllMappingsFromCache(ids: string[], mappingCache: Cache<Mapping[]>, mappingType: MappingType): IdToRawMappings;
    static getCacheKeysForIds(ids: string[]): IdToId;
    static getResolvedMapping(ids: string[], idsToMappings: IdToRawMappings, valueCache: Cache<any>, mappingType: MappingType): IdToMapping;
    static findBestMapping(ids: string[], idsToMappings: IdToRawMappings, valueCache: Cache<any>, mappingType: MappingType): IdToMapping;
    static getMappingScores(ids: string[], idsToMappings: IdToRawMappings, valueCache: Cache<any>, mappingType: MappingType): IdToMappingScores;
    static getNumberOfConstantsNotInCache(ids: string[], idsToMappings: IdToRawMappings, valueCache: Cache<any>): IdToConstCounts;
    private static getLoadMappingScore;
    private static getSaveMappingScore;
    private static getMethodMappingScore;
    static calculateScoreForMappings(ids: string[], idsToMappings: IdToRawMappings, idToConstCounts: IdToConstCounts, mappingType: MappingType): IdToMappingScores;
}
