import { IdToMapping, IdToValue, IdTo, IdToPostProcessor, IdToPreProcessor, VarKeyValues } from "./types/id-mapping-types";
import { StatusError } from "wbm-base/type/error";
import { Cache } from "./cache";
export declare type PostTransformer = (rawValue: string | StatusError) => IdToValue;
export declare type PreTransformer = (values: IdToValue) => VarKeyValues;
export declare type Converter = (rawValuesForIds: IdToValue) => IdToValue;
export declare type IdsToPostTransformer = IdTo<PostTransformer | StatusError>;
export declare type IdsToPreTransformer = IdTo<PreTransformer | StatusError>;
export declare type IdsToConverter = IdTo<Converter | StatusError>;
export declare class PfcProcessorLoader {
    static customTransformationsCache: Cache<PostTransformer | PreTransformer>;
    private static loadCustomTransformer;
    static createPostTransformerFromMapping(ids: string[], idToMapping: IdToMapping): Promise<IdsToPostTransformer>;
    static createPreTransformerFromMapping(ids: string[], idToMapping: IdToMapping): Promise<IdsToPreTransformer>;
    static createConverterFromMapping(ids: string[], idToMapping: IdToMapping): IdsToConverter;
    static mergeTransformerAndConverterToPostProcessor(ids: string[], idToMapping: IdToMapping, idToTransformer: IdsToPostTransformer, idToConverter: IdsToConverter): IdToPostProcessor;
    static mergeTransformerAndConverterToPreProcessor(ids: string[], idToMapping: IdToMapping, idToTransformer: IdsToPreTransformer, idToConverter: IdsToConverter): IdToPreProcessor;
    static createPostProcessor(ids: string[], idToMapping: IdToMapping): Promise<IdToPostProcessor>;
    static createPreProcessor(ids: string[], idToMapping: IdToMapping): Promise<IdToPreProcessor>;
}
