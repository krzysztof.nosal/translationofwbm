import { IdToTool, IdTo } from "./types/id-mapping-types";
import { StatusError } from "wbm-base/type/error";
import { IdToVarKeyValues } from "./tool-creator";
export interface RequestBody {
    aDeviceParams: {
        name: string;
        parameter: string[];
        multiline?: boolean;
    }[];
}
export declare type IdToError = IdTo<StatusError>;
export interface ExecutionResult {
    status: 0;
    resultString: string;
}
export declare type IdToExecutionResult = IdTo<ExecutionResult | StatusError>;
export interface ResponseBody {
    aDeviceResponse: ({
        status: 0;
        resultString: string;
        errorText?: undefined;
    } | {
        status: number;
        errorText: string;
        resultString?: undefined;
    })[];
}
export interface RequestOptimization {
    [index: number]: number[];
}
export declare class PfcCommandExecutor {
    static buildRequestBody(ids: string[], tools: IdToTool): RequestBody;
    static optimizeRequestBody(requestBody: RequestBody): RequestOptimization;
    static deoptimizeResponseBody(responseBody: ResponseBody, optimization: RequestOptimization): ResponseBody;
    static sendRequestWithBody(requestBody: RequestBody, renewSession?: boolean, timeout?: number): Promise<ResponseBody>;
    static filterIdsByExistingTool(ids: string[], idToTool: IdToTool): string[];
    static getExecutionResultsFromResponseBody(ids: string[], requestBody: RequestBody, responseBody: ResponseBody): IdToExecutionResult;
    static executeTools(ids: string[], idToTool: IdToTool, renewSession?: boolean, idToVarKeyValues?: IdToVarKeyValues): Promise<IdToExecutionResult>;
    static replaceVariablesInCommand(ids: string[], idToTool: IdToTool, idToVarKeyValues: IdToVarKeyValues): IdToTool;
}
