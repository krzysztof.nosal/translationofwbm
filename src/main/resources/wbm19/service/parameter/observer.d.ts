import { ValueItem } from "../parameter";
import { IdTo } from "./types/id-mapping-types";
import { Observation, ObservationOptions, Observer } from "wbm-base/type/parameter/observe";
import { Parameter } from "wbm-base/type/parameter/parameter";
import { PfcReader } from "./reader";
export interface Observings {
    observer: Observer<any>;
    observation: Observation<any>;
    options: ObservationOptions;
    parameter: Parameter;
}
export interface IntervalCache {
    id: string;
    intervalId: number;
}
export declare class PFCObserver {
    static AddObservings(parameterId: string, observer: Observer<any>, observation: Observation<any>, options: ObservationOptions, parameter: Parameter, observings: IdTo<Observings[]>): void;
    private intervalCache;
    static UpdateChangeOnObservers(id: string, newEntry: ValueItem, oldEntry: ValueItem, observings: IdTo<Observings[]> | undefined): void;
    static SampleObserver(id: string, pfcReader: PfcReader, observings: IdTo<Observings[]> | undefined, interval: number, oldEntry: ValueItem): void;
    static CallObserversUpdateObservations(observingsForId: Observings[], id: string, newEntry: ValueItem, oldEntry: ValueItem): void;
    static UpdateHistory(observation: Observation<any>, options: ObservationOptions, oldEntry: ValueItem, newEntry: ValueItem): void;
}
