import { Cache } from '../cache';
export declare type ValueCache = Cache<{
    value: any;
    originalValue: any;
}>;
