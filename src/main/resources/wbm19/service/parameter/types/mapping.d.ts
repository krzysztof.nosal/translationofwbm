/**
 * Represents a mapping informations of a specific parameter by providing meta
 * information about it.
 */
export declare interface Mapping {
    /**
     * configtool template commandline string
     */
    command: string;
    /**
     * paths of parameter ids which values replaces the constants in configtool command string
     */
    constants?: {
        [key: string]: string;
    };
    /**
     * options for configtool command e.g. multiline: true
     */
    options?: {
        [key: string]: string | boolean;
    };
    /**
     * Use 'value', if the resposne value is assigned directly to all associated parameters
     * Use 'mapping', if the response value is a flat json object, which values are mapped
     *   according to this.mapping
     * Use 'custom' if a custom functionm will be provided during runtime
     */
    type: 'value' | 'mapping' | 'custom';
    /**
     * Id of parameter should be read by configtool (usually  a keychain in dot
     * notation, such as 'a.unique.parameter.id' or 'networking.ipaddress')
     */
    reads?: string[];
    /**
     * Id of parameter should be written by configtool (usually a keychein in dot
     * notation, such as 'a.unique.parameter.id' or ' networking.ipaddress')
     */
    writes?: string[];
    /**
     * Id of methods that are called with the given configtool
     */
    executes?: string[];
    /**
     * Mendatory, if this.type === 'mapping'
     */
    mapping?: {
        [parameterid: string]: string;
    };
    /**
     * Name of custom transform, if type is 'custom'
     */
    custom?: string;
    /**
     * array of parameter parent ids which can be browsed by configtool call
     */
    browses?: string[];
    /**
     * array of conversion pairs for any parameter id whose value could be
     * converted. Conversions apply before a value is written to xor after it has
     * been fetched from device, depending of the kind of tool (read|write).
     */
    conversion?: {
        [parameterId: string]: {
            from: any;
            to: any;
        }[];
    };
    /**
     * Determine, that the configtool will not return, for example when the webserver
     * will be restarted during its execution. The WBM will not wait for a response in
     * that case and just continue in assumption that the execution was successful.
     *
     * An empty raw string value will be used for this configtool as a fake response.
     */
    noReturn?: boolean;
}
