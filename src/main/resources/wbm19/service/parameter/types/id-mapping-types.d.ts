import { StatusError } from "wbm-base/type/error";
import { Mapping } from "./mapping";
export interface IdTo<T> {
    [id: string]: T;
}
export declare type IdToId = IdTo<string>;
export declare type IdToMapping = IdTo<Mapping | StatusError>;
export declare type Value = string | boolean | number;
export declare type IdToValue = IdTo<Value | StatusError>;
export declare type PostProcessor = (rawValue: string | StatusError) => IdToValue;
export declare type IdToPostProcessor = IdTo<PostProcessor | StatusError>;
export declare type VarKeyValues = {
    [varKey: string]: string;
};
export declare type PreProcessor = (values: IdToValue) => VarKeyValues;
export declare type IdToPreProcessor = IdTo<PreProcessor | StatusError>;
export interface Tool {
    command: string;
    multiline?: boolean;
    noReturn?: boolean;
}
export declare type IdToTool = IdTo<Tool | StatusError>;
export interface LoadTool extends Tool {
    postProcess: PostProcessor;
}
export declare type IdToLoadTool = IdTo<LoadTool | StatusError>;
export interface SaveTool extends Tool {
    preProcess: PreProcessor;
}
export declare type IdToSaveTool = IdTo<SaveTool | StatusError>;
export interface CallTool extends Tool {
    preProcess?: PreProcessor;
}
export declare type IdToCallTool = IdTo<CallTool | StatusError>;
