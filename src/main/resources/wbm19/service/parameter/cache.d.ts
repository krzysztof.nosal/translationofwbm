export declare type SubscriptionCallback<T> = (key: string, newValue: T, oldValue: T) => void;
/**
 * A simple cache implementtaion to handle key value pairs
 */
export declare class Cache<T> {
    /**
     * interal storage object
     */
    private cache;
    /**
     * Create a new empty cache
     */
    constructor();
    /**
     * All existing keys in the cache
     */
    readonly keys: string[];
    /**
     * Set a value for a key
     * @param key
     * @param value
     */
    set(key: string, value: T): void;
    /**
     * remove a value and key from the cache
     * @param key
     */
    unset(key: string): void;
    /**
     * get the value for a key
     * @param key
     */
    get(key: string): T;
    /**
     * check if a key is existing in the cache
     * @param key
     */
    has(key: string): boolean;
    private subscriptions;
    /**
     * register to changes in this cache
     */
    subscribe(type: 'change', callback: SubscriptionCallback<T>): void;
    /**
     * clear the cache
     */
    clear(): void;
    merged(withCache: Cache<T>): Cache<T>;
}
