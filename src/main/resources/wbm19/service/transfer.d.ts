import { TransferService } from "wbm-base/service/transfer";
import { PhpAuthenticationService } from "service/authentication";
import { LoggerService } from "../../node_modules/wbm-base/dist/service/logger";
export interface ChunkedFile {
    name: string;
    chunks: Blob[];
    destination?: string;
}
declare type FileOrFileObject = File | {
    file: File;
    name?: string;
};
export declare class PfcTransferService implements TransferService {
    private authenticationService;
    private successedUploads;
    private lastTransferToken;
    private loggerService;
    constructor(authenticationService: PhpAuthenticationService, loggerService: LoggerService);
    upload(files: FileOrFileObject[], destination?: string): Promise<string[]>;
    cleanUp(): Promise<void>;
    calculateUploadSize(files: FileOrFileObject[]): number;
    prepareTransfer(transferSize?: number): Promise<{
        chunkSize: number;
        transferToken: string;
        transferPath: string;
    }>;
    cacheUploadedFile(file: File, destination: string): void;
    chunkedFilesUpload(files: FileOrFileObject[], chunkSize: number, transferToken: string, destination?: string): Promise<string[]>;
    createChunkedFile(file: FileOrFileObject, chunkSize: number, destination?: string): ChunkedFile;
    uploadChunk(chunk: ChunkedFile, transferToken: string, fileSize: number): Promise<string>;
    download(args: {
        token: string;
    }): Promise<void>;
    download(args: {
        filepath: string;
    }): Promise<void>;
    download(filepath: string): Promise<void>;
}
export {};
