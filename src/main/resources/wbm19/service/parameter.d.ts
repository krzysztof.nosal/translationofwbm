import { ParameterService } from 'wbm-base/service/parameter';
import { Parameter } from 'wbm-base/type/parameter/parameter';
import { Method } from 'wbm-base/type/parameter/method';
import { Type } from 'wbm-base/type/parameter/types';
import { ReadResult } from 'wbm-base/type/parameter/read';
import { WriteResult } from 'wbm-base/type/parameter/write';
import { Transaction, TransactionResult } from 'wbm-base/type/parameter/transaction';
import { Observation, ChangeObservationOptions, SampleObservationOptions, MethodObservationOptions, ParameterObserver, MethodObserver, ParameterSampleObserver } from 'wbm-base/type/parameter/observe';
import { FetchResult } from 'wbm-base/type/parameter/fetch';
import { CommitResult } from 'wbm-base/type/parameter/commit';
import { Cache } from './parameter/cache';
import { LocaleService } from 'wbm-base/service/locale';
import { LoggerService } from 'wbm-base/service/logger';
import { StatusService } from 'wbm-base/service/status';
import { PhpAuthenticationService } from './authentication';
import { MethodArguments, MethodResult } from 'wbm-base/type/parameter/method';
import { ValueCache } from './parameter/types/cache-types';
export interface ValueItem {
    value: any;
    originalValue: any;
}
export declare class PfcParameterService implements ParameterService {
    /**
     * A cache for parameter info objects, that may be shared between
     * subservices.
     */
    private parameterCache;
    /**
     * A cache for parameter info objects, that may be shared between
     * subservices.
     */
    private methodCache;
    /**
     * A cache for parameter values, which may be shared between sub-services.
     */
    private valueCache;
    /**
     * A promise, which has to be resolved, before any operation, that relies on retrieved
     * parameter and mapping infos, should start its work.
     * In any of those implementations use an `await this.init;` statement in the beginning
     * of your code.
     */
    private init;
    private localeService;
    private loggerService;
    private statusService;
    private authenticationService;
    private pfcReader;
    private pfcWriter;
    private pfcCaller;
    /**
     * Override base constructor from ParameterService interface
     *
     * @param localeService The localization service is required to translate parameter meta-data as needed
     */
    constructor(localeService: LocaleService, loggerService: LoggerService, statusService: StatusService, authenticationService: PhpAuthenticationService);
    /**
     * wbm-pfc specific constructor with dependency injection which may be used by unit-tests
     * in particular
     *
     * @param localeService The localization service is required to translate parameter meta-data as needed
     * @param override The members to be overriden
     */
    constructor(localeService: LocaleService, loggerService: LoggerService, statusService: StatusService, authenticationService: PhpAuthenticationService, override: {
        parameterCache?: Cache<Parameter>;
        methodCache?: Cache<Method>;
        valueCache?: ValueCache;
    });
    private setupStatusService;
    /**
     * Loads the parameter infos from the PHP backend
     * @returns Parameter infos as a plain array
     */
    private loadParametersAndMethods;
    /**
     * Loads the mapping infos from the PHP backend
     * @returns mapping infos as a plain array
     */
    private loadMappings;
    /**
     * Evaluates parameter infos and mappings to create [PfcParameter] and [Tool] objects before
     * adding them in the read and write mapping tables or dynamic mapping info cache.
     *
     * @param parameterInfos The parameter info objects, as received from the backend as a plain array
     * @param methodInfos   The method infos as received from the backend as a plain array
     */
    private processParametersAndMethods;
    get(id: string): Promise<Parameter>;
    read(parameterIds: string | string[]): Promise<ReadResult[]>;
    browse(baseId: string, deep?: boolean): Promise<string[]>;
    fetch(parameterOrBaseIds: string | string[]): Promise<FetchResult[]>;
    write<T>(parameterValues: {
        parameterId: string;
        value: T;
    }[]): Promise<WriteResult[]>;
    write<T>(parameterId: string, value: T): Promise<WriteResult>;
    commit(idOrIds: string | string[]): Promise<CommitResult[]>;
    private runningTransactions;
    private waitingForTransactions;
    private readonly isTransationRunning;
    private readTransactions?;
    transaction(transaction: Transaction): Promise<TransactionResult>;
    private observings?;
    observe<T extends Type>(parameterId: string, options: ChangeObservationOptions, observer: ParameterObserver<T>): Promise<Observation<T>>;
    observe<T extends {
        [id: string]: Type;
    }>(parameterId: string, options: SampleObservationOptions, observer: ParameterSampleObserver<T>): Promise<Observation<T>>;
    observe<T extends Type>(methodId: string, options: MethodObservationOptions, observer: MethodObserver<T>): Promise<Observation<T>>;
    reloadParameters(): Promise<void>;
    execute<T extends Type>(methodId: string, args?: MethodArguments): Promise<MethodResult<T>>;
}
