import { PluginService } from 'wbm-base/service/plugin';
import { Plugin } from 'wbm-base/type/plugin';
import { StatusError } from 'wbm-base/type/error';
import { LoggerService } from 'wbm-base/service/logger';
import { StatusService } from 'wbm-base/service/status';
import { PhpAuthenticationService } from './authentication';
import { PfcParameterService } from './parameter';
export declare type PluginLoadFunction = (...args: any[]) => Promise<any>;
export declare class PhpPluginService implements PluginService {
    private loggerService;
    private statusService;
    private authenticationService;
    private parameterService;
    constructor(loggerService: LoggerService, statusService: StatusService, authenticationService: PhpAuthenticationService, parameterService: PfcParameterService);
    private setupStatusService;
    /**
     * Contains all loaded plugins by name.
     */
    private plugins;
    /**
     * Contains all scripts by plugin name.
     */
    private scripts;
    getLoaded(): Plugin[];
    isLoaded(name: string, version?: string): boolean;
    getResourceUrl(name: string): string;
    register(name: string, load: PluginLoadFunction): void;
    private preparePlugin;
    private isPlatformCompatible;
    private isVersionCompatible;
    load(): Promise<(Plugin | StatusError)[]>;
    private arePluginFeaturesSupported;
}
