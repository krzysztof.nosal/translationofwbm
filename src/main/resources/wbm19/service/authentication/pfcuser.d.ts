import { User } from 'wbm-base/type/user/user';
export interface PFCUser extends User {
    /**
     * CSRF token that would send from Server
     */
    csrf: string | false;
    /**
     * Timestamp in ms for check
     */
    timestamp: number;
    /**
     * If session is not expired this is true
     * else false
     */
    sessionExists: boolean;
}
