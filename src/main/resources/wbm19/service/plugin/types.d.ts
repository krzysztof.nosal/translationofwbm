export interface PluginManifest {
    name: string;
    version: string;
    base: string;
    description: string;
    requires: string;
    provides: string[];
    files: string[];
    platform: string[];
    license: string;
    thirdParty?: string;
    copyright: string;
    features?: string[];
}
export interface Response {
    plugins: PluginManifest[];
}
