/**
 * Use an Injector instance to inject a script, which is available at some URL.
 */
export declare class Injector {
    /**
     * Holds the container, where any script will be appended to.
     */
    private container;
    /**
     * Create a new injector with the given container element.
     *
     * @param container The container to be used for any script elements
     */
    constructor(container: HTMLElement);
    /**
     * Inject a new script by appending a new script element to the injector's
     * container.
     *
     * @param url the URL at which the script is available
     *
     * @returns A promise, which resolves with the new script element, as soon
     *          as the script has been executed. The promise rejects, if any error
     *          occurs, i.e. when the server responds with an error.
     *
     * @todo Ensure, that any server errors are handled properly
     */
    inject(url: string): Promise<HTMLScriptElement>;
}
