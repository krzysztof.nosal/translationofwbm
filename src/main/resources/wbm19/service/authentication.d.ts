import { AuthenticationService } from 'wbm-base/service/authentication';
import { PFCUser } from './authentication/pfcuser';
import { LoggerService } from 'wbm-base/service/logger';
import { StatusService } from 'wbm-base/service/status';
export declare class PhpAuthenticationService implements AuthenticationService {
    private expireTime;
    /**
     * Handler id for session timer
     */
    private sessionTimer;
    private loggerService;
    private statusService;
    constructor(loggerService: LoggerService, statusService: StatusService);
    private static _singleton;
    static readonly singleton: PhpAuthenticationService;
    /**
     * Fetches some data from backen with user criteria
     * @param url url string to load from
     * @param body some information as fetch body
     */
    csrfFetch(url: string, body?: any, renewsession?: boolean): Promise<Response>;
    csrfHeadFetch(url: string, headerProperties?: {
        [key: string]: string;
    }, body?: any): Promise<Response>;
    private setupStatusService;
    /**
     * loads the actual logged in user from saved js Cookie
     */
    getActiveUser(): PFCUser | undefined;
    updateActiveUser(user: Partial<PFCUser>): void;
    /**
     * Login for the given username with password
     * @param username Userprofile name. Leave empty for guest login.
     * @param password Password for userprofile
     */
    login(username?: string, password?: string): Promise<PFCUser>;
    private createPFCUser;
    private getUserRoles;
    /**
     * Logout a user profile
     * @param user Userprofile to Logout
     */
    logout(user: PFCUser): Promise<void>;
    private invalidateSessionAndReload;
    private startSessionTimer;
    private stopSessionTimer;
    /**
     * Returns the actual session state for active user.
     *
     * Returns false if state is expired otherwise true
     */
    getSessionState(): boolean | undefined;
    /**
     * Deletes the actual user cookie for js
     */
    clearActiveUser(): void;
}
