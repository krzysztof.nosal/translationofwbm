import { LocaleService } from 'wbm-base/service/locale';
import { Language } from 'wbm-base/type/locale/language';
import { Dictionary } from 'wbm-base/type/locale/dictionary';
import { Localized } from 'wbm-base/type/locale/localized';
export declare class PhpLocaleService implements LocaleService {
    readonly dictionary: Dictionary;
    /**
     * The current language, whcih is fixed to englisch for the very moment
     */
    readonly language: Language;
    constructor(language: Language, dictionary: Dictionary);
    translate(key: string): string | null;
    localized({ key, fallback }: Localized): string;
}
