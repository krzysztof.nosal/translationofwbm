/**
 * This is your plugins entry file.
 *
 * Export any API that is provided by your plugin here...
 */
import { PFCBase } from './base';
declare const globals: {
    base: PFCBase;
};
export = globals;
