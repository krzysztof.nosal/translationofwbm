import { Base } from 'wbm-base/base';
import { PfcParameterService } from './service/parameter';
import { PhpAuthenticationService } from './service/authentication';
import { PhpPluginService } from './service/plugin';
import { PhpLocaleService } from './service/locale';
import { ModalPresenterService } from 'wbm-base/service/modal-presenter';
import { viewGenerator, subframeGenerator } from 'wbm-core';
import { PfcTransferService } from './service/transfer';
import { BaseManifest } from 'wbm-base/service/manifest';
export declare class PFCBase implements Base {
    transfer: PfcTransferService;
    localization: PhpLocaleService;
    viewGenerator: typeof viewGenerator;
    subframeGenerator: typeof subframeGenerator;
    parameter: PfcParameterService;
    plugin: PhpPluginService;
    authentication: PhpAuthenticationService;
    modalPresenter: ModalPresenterService;
    logger: import("wbm-core/dist/services/logger").Logger;
    status: import("wbm-core/dist/services/status-code-service").StatusService;
    manifest: {
        getInfo(): Promise<BaseManifest>;
    };
    browser: import("wbm-base/dist/service/browser").BrowserService;
}
