
export function getMediumDescriptionForDeviceMedium(mediumName: string) {
    
    if (mediumName === 'sd-card') return 'Memory Card';
    
    if (/^internal-flash.*$/.test(mediumName)) return 'Internal Flash';
    
    // medium name should be the fallback
    return mediumName;
}

