type DeviceType = 'memory-card' | 'usb' | 'internal' | 'unknown';

export function getDeviceTypeForDeviceMedium(mediumName: string): DeviceType {
    if (mediumName === 'sd-card') return 'memory-card';    
    if (/^internal-flash.*$/.test(mediumName)) return 'internal';
    return 'unknown';
}

