import { getMediumDescriptionForDeviceMedium } from "./medium-description";


export default function (value: string|Error) {
    if (value instanceof Error) {
        return {
            'filesystem.activedevicedescription': value
        }
    } 
    else {
        let result = {} as any;
        result['filesystem.activedevicedescription'] = getMediumDescriptionForDeviceMedium(value);
        return result;
    }
};