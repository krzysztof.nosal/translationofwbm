import { getMediumDescriptionForDeviceMedium,  } from "./medium-description";
import { getDeviceTypeForDeviceMedium } from './device-type';

interface DeviceData {
    deviceName: string, // /dev/bdfkjbsdf
    deviceMedium: string, // 'sd-card', 'internal-flash-emmc', etc
    deviceLabel: string // Name of SD-Card or internal device
}


export default function (value: string|Error) {
    if (value instanceof Error) {
        return {
            'filesystem.devices.*.name': value,
            'filesystem.devices.*.path': value,
            'filesystem.devices.*.medium': value,
            'filesystem.devices.*.label': value,
            'filesystem.devices.*.description': value,
            'filesystem.devices.*.external': value
        }
    } else {
        try {
            const deviceDataList = JSON.parse(value) as DeviceData[];
            let result = {} as any;
        
            const baseId = 'filesystem.devices';
            deviceDataList.forEach((deviceData, index) => {
                result[`${baseId}.${index}.name`] = ((path) => path[path.length-1])(deviceData.deviceName.split('/'));
                result[`${baseId}.${index}.path`] = deviceData.deviceName;
                result[`${baseId}.${index}.medium`] = deviceData.deviceMedium;
                result[`${baseId}.${index}.label`] = deviceData.deviceLabel;
                result[`${baseId}.${index}.description`] = getMediumDescriptionForDeviceMedium(deviceData.deviceMedium);
                result[`${baseId}.${index}.type`] = getDeviceTypeForDeviceMedium(deviceData.deviceMedium);
            })
            
            return result;
        } catch(e) {
            return {
                'filesystem.devices.*.name': e,
                'filesystem.devices.*.path': e,
                'filesystem.devices.*.medium': e,
                'filesystem.devices.*.label': e,
                'filesystem.devices.*.description': e,
                'filesystem.devices.*.type': e
            }
        }
    }
};