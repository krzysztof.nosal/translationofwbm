/*!
 * wbm-massstorage@1.1.0
 * 
 *   Copyright © 2020 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     UNLICENSED
 * 
 *   Contributors:
 *     
 * 
 *   Description:
 *     Mass storage configuration
 * 
 *   
 */
this["/get-filesystem-data"]=function(e){var t={};function i(r){if(t[r])return t[r].exports;var n=t[r]={i:r,l:!1,exports:{}};return e[r].call(n.exports,n,n.exports,i),n.l=!0,n.exports}return i.m=e,i.c=t,i.d=function(e,t,r){i.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},i.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},i.t=function(e,t){if(1&t&&(e=i(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(i.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var n in e)i.d(r,n,function(t){return e[t]}.bind(null,n));return r},i.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return i.d(t,"a",t),t},i.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},i.p="",i(i.s=3)}([function(e,t,i){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.getMediumDescriptionForDeviceMedium=function(e){return"sd-card"===e?"Memory Card":/^internal-flash.*$/.test(e)?"Internal Flash":e}},function(e,t,i){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.getDeviceTypeForDeviceMedium=function(e){return"sd-card"===e?"memory-card":/^internal-flash.*$/.test(e)?"internal":"unknown"}},,function(e,t,i){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var r=i(0),n=i(1);t.default=function(e){if(e instanceof Error)return{"filesystem.devices.*.name":e,"filesystem.devices.*.path":e,"filesystem.devices.*.medium":e,"filesystem.devices.*.label":e,"filesystem.devices.*.description":e,"filesystem.devices.*.external":e};try{var t=JSON.parse(e),i={},s="filesystem.devices";return t.forEach((function(e,t){var c;i[s+"."+t+".name"]=(c=e.deviceName.split("/"))[c.length-1],i[s+"."+t+".path"]=e.deviceName,i[s+"."+t+".medium"]=e.deviceMedium,i[s+"."+t+".label"]=e.deviceLabel,i[s+"."+t+".description"]=r.getMediumDescriptionForDeviceMedium(e.deviceMedium),i[s+"."+t+".type"]=n.getDeviceTypeForDeviceMedium(e.deviceMedium)})),i}catch(e){return{"filesystem.devices.*.name":e,"filesystem.devices.*.path":e,"filesystem.devices.*.medium":e,"filesystem.devices.*.label":e,"filesystem.devices.*.description":e,"filesystem.devices.*.type":e}}}}]).default;
//# sourceMappingURL=get-filesystem-data.js.map