export default function(value: string | Error) {

    if (value instanceof Error) {
        return { 
            'runtime.codesys2.available': value,
            'runtime.eruntime.available': value,
            'runtime.availableversions.*.id': value,
            'runtime.availableversions.*.name': value 
        }
    }

    const namesForIds: any = {
        '0': 'none',
        '2': 'CODESYS 2',
        '3': 'e!RUNTIME'
    };

    const result = {} as any;

    let index = 0;
    for (const id of ['0', '2', '3']) {
        const available = value.indexOf(id) >= 0;
        if (id === '2') {
            result['runtime.codesys2.available'] = available;
        }
        if (id === '3') {
            result['runtime.eruntime.available'] = available;
        }
        if (available) {
            result[`runtime.availableversions.${index}.name`] = namesForIds[id];
            result[`runtime.availableversions.${index}.id`] = id;
            index++;
        } 
        
    }
     

    return result;
}
