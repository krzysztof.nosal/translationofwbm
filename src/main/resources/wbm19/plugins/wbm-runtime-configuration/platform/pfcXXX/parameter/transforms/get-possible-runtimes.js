/*!
 * wbm-runtime-configuration@1.0.0
 * 
 *   Copyright © 2019 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     
 * 
 *   Description:
 *     e!RUNTIME Configuration options
 * 
 *   
 */
this["/get-possible-runtimes"]=function(e){function r(t){if(n[t])return n[t].exports;var i=n[t]={i:t,l:!1,exports:{}};return e[t].call(i.exports,i,i.exports,r),i.l=!0,i.exports}var n={};return r.m=e,r.c=n,r.d=function(e,n,t){r.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:t})},r.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return r.d(n,"a",n),n},r.o=function(e,r){return Object.prototype.hasOwnProperty.call(e,r)},r.p="",r(r.s=0)}([function(e,r,n){"use strict";function t(e){if(e instanceof Error)return{"runtime.codesys2.available":e,"runtime.eruntime.available":e,"runtime.availableversions.*.id":e,"runtime.availableversions.*.name":e};for(var r={0:"none",2:"CODESYS 2",3:"e!RUNTIME"},n={},t=0,i=0,a=["0","2","3"];i<a.length;i++){var u=a[i],o=e.indexOf(u)>=0;"2"===u&&(n["runtime.codesys2.available"]=o),"3"===u&&(n["runtime.eruntime.available"]=o),o&&(n["runtime.availableversions."+t+".name"]=r[u],n["runtime.availableversions."+t+".id"]=u,t++)}return n}Object.defineProperty(r,"__esModule",{value:!0}),r.default=t}]).default;
//# sourceMappingURL=get-possible-runtimes.js.map