import { createBridgeLabel, isLegacyInterfaceName } from './interface-name';
import { sortInterfaces } from './interface-sort-helper';



export default function(rawValueOrError: string|Error): {[key: string]: any} {

    if (rawValueOrError instanceof Error) {
        return {
            "firewall.icmpbroadcastprotection.enabled": rawValueOrError,
            "firewall.connectionlimit.udp.value": rawValueOrError,
            "firewall.connectionlimit.tcp.value": rawValueOrError,
            "firewall.connectionlimit.tcp.unit": rawValueOrError,
            "firewall.connectionlimit.udp.unit": rawValueOrError,

            "firewall.interfaces.*.enabled": rawValueOrError,
            "firewall.interfaces.*.label": rawValueOrError,
            "firewall.interfaces.*.name": rawValueOrError,
            "firewall.interfaces.*.icmpbroadcastprotection.echolimit.value": rawValueOrError,
            "firewall.interfaces.*.icmpbroadcastprotection.echolimit.unit": rawValueOrError,

            "firewall.userfilter.*.index": rawValueOrError,
            "firewall.userfilter.*.interface": rawValueOrError,
            "firewall.userfilter.*.interfacelabel": rawValueOrError,
            "firewall.userfilter.*.protocol": rawValueOrError,
            "firewall.userfilter.*.source.address": rawValueOrError,
            "firewall.userfilter.*.source.mask": rawValueOrError,
            "firewall.userfilter.*.source.port": rawValueOrError,
            "firewall.userfilter.*.destination.address": rawValueOrError,
            "firewall.userfilter.*.destination.mask": rawValueOrError,
            "firewall.userfilter.*.destination.port": rawValueOrError,
            "firewall.userfilter.*.policy": rawValueOrError
        }
    }

    const rawValue = rawValueOrError as string;

    const xmlDoc = $.parseXML(rawValue);

    return {
        ...extractGlobalFirewallOptions(xmlDoc),
        ...extractInterfaceOptions(xmlDoc),
        ...extractUserFilters(xmlDoc)
    }
}



function extractGlobalFirewallOptions(xmlDoc: XMLDocument): {[key: string]: any} {
    const echoNode = xmlDoc.querySelector('firewall > ipv4 > echo');
    const broadcastProtection = echoNode && echoNode.getAttribute('broadcast_protection');

    const climitsNode = xmlDoc.querySelector('firewall > ipv4 > climits');
    let [udpLimitValue, udpLimitUnit] = ['', 'second'];
    let [tcpLimitValue, tcpLimitUnit] = ['', 'second'];
    if(climitsNode && climitsNode.getAttribute('udp')) {
        [udpLimitValue, udpLimitUnit] = climitsNode && climitsNode.getAttribute('udp')!.split('/');
    }
    if(climitsNode && climitsNode.getAttribute('tcp')) {
        [tcpLimitValue, tcpLimitUnit] = climitsNode && climitsNode.getAttribute('tcp')!.split('/');
    }

    const result: {[key: string]: any} = {
        "firewall.icmpbroadcastprotection.enabled": broadcastProtection || new Error('broadcast protection value was not found in xml'),
        "firewall.connectionlimit.udp.value": udpLimitValue,
        "firewall.connectionlimit.tcp.value": tcpLimitValue,
        "firewall.connectionlimit.udp.unit": udpLimitUnit,
        "firewall.connectionlimit.tcp.unit": tcpLimitUnit
    };

    return result;
}

function extractInterfaceOptions(xmlDoc: XMLDocument): {[key: string]: any} {
    const echoNode = xmlDoc.querySelector('firewall > ipv4 > echo');
    const requestNodes = echoNode && echoNode.querySelectorAll('request')!;
    const openNode = xmlDoc.querySelector('firewall > ipv4 > input > open');

    let interfaces: {
        interface: string,
        limit: string,
        policy: string,
        burst: string
    }[] = [];
    if (requestNodes) {
        for (let i = 0; i < requestNodes.length; i++) {
            const request = requestNodes.item(i);
            const iface = request.getAttribute('if');
            const limit = request.getAttribute('limit');
            const policy = request.getAttribute('policy');
            const burst = request.getAttribute('burst');
            iface && interfaces.push({
                interface: iface,
                limit: limit || '',
                policy: policy || '',
                burst: burst || '0'
            });
        }
    }

    interfaces = sortInterfaces(interfaces);
    interfaces = interfaces.filter(itf => ! isLegacyInterfaceName(itf.interface));

    const result: {[key: string]: any} = {};
    interfaces.forEach((iface, index) => {

        const interface_name = iface.interface;

        const nameId = `firewall.interfaces.${index}.name`;
        result[nameId] = interface_name;


        const labelId = `firewall.interfaces.${index}.label`;
        result[labelId] = createBridgeLabel(interface_name, 'Interface');

        const enabledId = `firewall.interfaces.${index}.enabled`;
        const openIfaceNode = (openNode && openNode.querySelector(`interface[if="${iface.interface}"]`));
        result[enabledId] = (openIfaceNode && openIfaceNode.getAttribute('state') === 'on') ? false : true;

        const icmpEnabledId = `firewall.interfaces.${index}.icmpbroadcastprotection.enabled`;
        result[icmpEnabledId] = iface.policy === 'drop';

        const limitValueId = `firewall.interfaces.${index}.icmpbroadcastprotection.echolimit.value`;
        const limitUnitId = `firewall.interfaces.${index}.icmpbroadcastprotection.echolimit.unit`;
        const [limitValue, limitUnit] = iface.limit.split('/');
        result[limitValueId] = limitValue;
        result[limitUnitId] = limitUnit;

        const burstId = `firewall.interfaces.${index}.icmpbroadcastprotection.burstlimit.value`;
        result[burstId] = iface.burst;
    });

    return result;
}


function extractUserFilters(xmlDoc: XMLDocument): {[key: string]: any} {
    const ruleNodes = xmlDoc.querySelectorAll('firewall > ipv4 > input > filter > rule');

    let result: {[key: string]: string} = {};

    if (ruleNodes) {
        for (let i = 0; i < ruleNodes.length; i++) {
            const ruleNode = ruleNodes.item(i);
            const ifaceName = ruleNode.getAttribute('if') || 'Any';

            if(isLegacyInterfaceName(ifaceName)) continue;

            result = {
                ...result,
                [`firewall.userfilter.${i}.index`]:               `${i+1}`,
                [`firewall.userfilter.${i}.interface`]:           ifaceName,
                [`firewall.userfilter.${i}.interfacelabel`]:      createBridgeLabel(ifaceName),
                [`firewall.userfilter.${i}.protocol`]:            ruleNode.getAttribute('proto') || '',
                [`firewall.userfilter.${i}.source.address`]:      ruleNode.getAttribute('src_ip') || '',
                [`firewall.userfilter.${i}.source.mask`]:         ruleNode.getAttribute('src_mask') || '',
                [`firewall.userfilter.${i}.source.port`]:         ruleNode.getAttribute('src_port') || '',
                [`firewall.userfilter.${i}.destination.address`]: ruleNode.getAttribute('dst_ip') || '',
                [`firewall.userfilter.${i}.destination.mask`]:    ruleNode.getAttribute('dst_mask') || '',
                [`firewall.userfilter.${i}.destination.port`]:    ruleNode.getAttribute('dst_port') || '',
                [`firewall.userfilter.${i}.policy`]:              ruleNode.getAttribute('policy') || ''
            };
        }
    }

    return result;
}