export interface SortableByInterface {
    interface: string
}

export function sortInterfaces<T extends SortableByInterface>(interfaces: T[]): T[] {
    return interfaces.sort((a,b) => a.interface.localeCompare(b.interface, undefined, {numeric: true, sensitivity: 'base'}));
}