/*!
 * wbm-firewall@1.2.3
 * 
 *   Copyright © 2020 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     
 * 
 *   Description:
 *     Firewall Settings
 * 
 *   
 */
this["/interface-name"]=function(e){var r={};function t(n){if(r[n])return r[n].exports;var o=r[n]={i:n,l:!1,exports:{}};return e[n].call(o.exports,o,o.exports,t),o.l=!0,o.exports}return t.m=e,t.c=r,t.d=function(e,r,n){t.o(e,r)||Object.defineProperty(e,r,{enumerable:!0,get:n})},t.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},t.t=function(e,r){if(1&r&&(e=t(e)),8&r)return e;if(4&r&&"object"==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(t.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:e}),2&r&&"string"!=typeof e)for(var o in e)t.d(n,o,function(r){return e[r]}.bind(null,o));return n},t.n=function(e){var r=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(r,"a",r),r},t.o=function(e,r){return Object.prototype.hasOwnProperty.call(e,r)},t.p="",t(t.s=0)}([function(e,r,t){"use strict";Object.defineProperty(r,"__esModule",{value:!0}),r.createBridgeLabel=function(e,r){void 0===r&&(r=null);var t,u=n.exec(e);if(u){var i=u[1].toLowerCase(),a=u[2];if(i in o){var f=a?" "+(parseInt(a)+1):"";return""+o[i]+f}}return""+(r?r+" ":"")+((t=e).charAt(0).toUpperCase()+t.slice(1))};var n=/^([a-zA-Z]+)([0-9]*)$/,o={br:"Bridge",wan:"WAN",vpn:"VPN"};r.isLegacyInterfaceName=function(e){return["X1","X2"].indexOf(e)>=0}}]).default;
//# sourceMappingURL=interface-name.js.map