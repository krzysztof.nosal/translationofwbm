import { sortInterfaces } from "./interface-sort-helper";
import { isLegacyInterfaceName } from "./interface-name";

const services = [
    { name: 'telnet',       jsonKey: 'telnet',          label: 'Telnet' },
    { name: 'ftp',          jsonKey: 'ftp',             label: 'FTP' },
    { name: 'ftps',         jsonKey: 'ftps',            label: 'FTPS' },
    { name: 'http',         jsonKey: 'http',            label: 'HTTP' },
    { name: 'https',        jsonKey: 'https',           label: 'HTTPS' },
    { name: 'iocheck',      jsonKey: 'iocheck',         label: 'I/O Check' },
    { name: 'codesysr',     jsonKey: 'codesysr',        label: 'PLC Runtime' },
    { name: 'codesysw',     jsonKey: 'codesysw',        label: 'PLC WebVisu - direct link (port 8080)' },
    { name: 'ssh',          jsonKey: 'ssh',             label: 'SSH' },
    { name: 'tftp',         jsonKey: 'tftp',            label: 'TFTP' },
    { name: 'dhcpd',        jsonKey: 'dhcpd',           label: 'BootP/DHCP' },
    { name: 'dns',          jsonKey: 'dns',             label: 'DNS' },
    { name: 'modbustcp',    jsonKey: 'modbus_tcp',      label: 'Modbus® TCP' },
    { name: 'modbusudp',    jsonKey: 'modbus_udp',      label: 'Modbus® UDP' },
    { name: 'snmp',         jsonKey: 'snmp',            label: 'SNMP' },
    { name: 'opcua',        jsonKey: 'opcua',           label: 'OPC UA' },
    { name: 'dnp3',         jsonKey: 'dnp3',            label: 'DNP3' },
    { name: 'iec608705104', jsonKey: 'iec60870_5_104',  label: 'IEC60870-5-104' },
    { name: 'iec61850mms',  jsonKey: 'iec61850_mms',    label: 'IEC61850 (port 102)' },
    { name: 'profinet',     jsonKey: 'profinet',        label: 'PROFINET IO' },
];


export default function(rawValueOrError: string|Error): {[key: string]: any} {

    if (rawValueOrError instanceof Error) {

        const enabledIds = services.map(service => `firewall.interfaces.*.services.${service.name}.enabled`);
        const labelIds = services.map(service => `firewall.interfaces.*.services.${service.name}.label`);

        enabledIds.concat(labelIds).reduce(
            (result,id) => ({...result, [id]: rawValueOrError}), {} as any
        );
    }

    const jsonObject = JSON.parse(rawValueOrError as string).interfaces;

    var interfaces = sortInterfaces(Object.keys(jsonObject).map(iface => ({interface: iface})));

    interfaces = interfaces.filter(i => ! isLegacyInterfaceName(i.interface));

    console.log(interfaces);

    const result: {[key: string]: any} = {};
    interfaces.forEach((iface, index) => {

        services.forEach(service => {
            const labelId = `firewall.interfaces.${index}.services.${service.name}.label`;
            const enabledId = `firewall.interfaces.${index}.services.${service.name}.enabled`;
            result[labelId] = service.label;

            try {
                const state = jsonObject[iface.interface].services[service.jsonKey].state
                result[enabledId] = state === 'on' ? true : false;
            } catch (e) {
                result[enabledId] = e;
            }

        });

    });

    return result;


}