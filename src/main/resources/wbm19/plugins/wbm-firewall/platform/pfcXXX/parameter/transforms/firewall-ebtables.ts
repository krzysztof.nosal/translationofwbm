import { createBridgeLabel, isLegacyInterfaceName } from "./interface-name";
import { sortInterfaces } from "./interface-sort-helper";


export default function(rawValueOrError: string|Error): {[key: string]: any} {

    if (rawValueOrError instanceof Error) {
        return {
            "firewall.macaddressfilter.enabled": rawValueOrError,
            "firewall.macaddressfilter.interfaces.*.label": rawValueOrError,
            "firewall.macaddressfilter.interfaces.*.name": rawValueOrError,
            "firewall.macaddressfilter.interfaces.*.enabled": rawValueOrError,
            "firewall.macaddressfilter.whitelist.*.enabled": rawValueOrError,
            "firewall.macaddressfilter.whitelist.*.address": rawValueOrError,
            "firewall.macaddressfilter.whitelist.*.mask": rawValueOrError
        }
    }

    const rawValue = rawValueOrError as string;

    const xmlDoc = $.parseXML(rawValue);

    return {
        ...extractMacAddressFilterOptions(xmlDoc),
        ...extractMacAddressWhitelist(xmlDoc)
    }
}



function extractMacAddressFilterOptions(xmlDoc: XMLDocument): {[key: string]: any} {
    const ethernetNode = xmlDoc.querySelector('firewall > ethernet');
    const mode = ethernetNode && ethernetNode.getAttribute('mode');
    const interfaceNodes = ethernetNode && ethernetNode.querySelectorAll('interfaces > interface')!;

    const result: {[key: string]: any} = {
        "firewall.macaddressfilter.enabled": mode || new Error('mac address filter state value was not found in xml'),
    };

    let interfaces: {
        interface: string,
        state: string
    }[] = [];
    if (interfaceNodes) {
        for (let i = 0; i < interfaceNodes.length; i++) {
            const node = interfaceNodes.item(i);
            const iface = node.getAttribute('if');
            const state = node.getAttribute('state');
            iface && interfaces.push({
                interface: iface,
                state: state || ''
            });
        }
    }

    interfaces = interfaces.filter(itf => ! isLegacyInterfaceName(itf.interface));

    interfaces = sortInterfaces(interfaces);

    interfaces.forEach((iface, index) => {
        const labelId = `firewall.macaddressfilter.interfaces.${index}.label`;
        result[labelId] = createBridgeLabel(iface.interface, 'Interface');
        const nameId = `firewall.macaddressfilter.interfaces.${index}.name`;
        result[nameId] = iface.interface;
        const enabledId = `firewall.macaddressfilter.interfaces.${index}.enabled`;
        result[enabledId] = iface.state;
    });

    return result;
}


function extractMacAddressWhitelist(xmlDoc: XMLDocument): {[key: string]: any} {
    const whitelistNodes = xmlDoc.querySelectorAll('firewall > ethernet > whitelist > host');

    const result: {[key: string]: any} = {};

    if (whitelistNodes) {
        for (let i = 0; i < whitelistNodes.length; i++) {
            const node = whitelistNodes.item(i);
            const mac = node.getAttribute('mac');
            const mask = node.getAttribute('mask');
            const state = node.getAttribute('state');

            const indexId = `firewall.macaddressfilter.whitelist.${i}.index`;
            result[indexId] = `${i+1}`;

            const enabledId = `firewall.macaddressfilter.whitelist.${i}.enabled`;
            result[enabledId] = state;

            const addressId = `firewall.macaddressfilter.whitelist.${i}.address`;
            result[addressId] = mac;

            const maskId = `firewall.macaddressfilter.whitelist.${i}.mask`;
            result[maskId] = mask;
        }
    } else {

        result["firewall.macaddressfilter.whitelist.*.index"] = new Error('mac address filter state value was not found in xml');
        result["firewall.macaddressfilter.whitelist.*.enabled"] = new Error('mac address filter state value was not found in xml');
        result["firewall.macaddressfilter.whitelist.*.address"] = new Error('mac address filter state value was not found in xml');
        result["firewall.macaddressfilter.whitelist.*.mask"] = new Error('mac address filter state value was not found in xml');

    }

    return result;
}


