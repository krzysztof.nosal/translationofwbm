

type VariablesForTheTool = {
    'tcpLimitValueUnit': string,
    'udpLimitValueUnit': string
}


/**
 * Transform values for max. udp and tcp connections to configtool input string.
 * Value and unit must be connected in one string, separated with '/'
 * If value is an empty string, input string for configtool must be '-'
 * 
 * NOTE: value for "union" is fix set to "second" in this transfor function.
 * Normally, we must use the constant value of "firewall.connectionlimit.tdp.unit" which we read from firewall
 * config before. But there is no way, to get any constant parameter values in this transform function.
 * Firewall actually always uses "second" as default. If no value is given, WBM always defines "second". 
 * By normal ways (wbm, configtools), the user has no possibility to change it.
 * To consider a possible change of unit would make firewall wbm page much more complicated. So we choose this
 * procedure here, until no errors occure.
 * 
 * @param value input values from from
 */
export default function (value: {[parameterId: string]: string}) {
    
    const toolVariables = {} as Partial<VariablesForTheTool>;

    toolVariables.tcpLimitValueUnit = '-';
    toolVariables.udpLimitValueUnit = '-';

    if(   (typeof value['firewall.connectionlimit.tcp.value'] !== undefined)
       && (value['firewall.connectionlimit.tcp.value'].length > 0)) {
        toolVariables.tcpLimitValueUnit = value['firewall.connectionlimit.tcp.value'] + '/second';
    }

    if(   (typeof value['firewall.connectionlimit.udp.value'] !== undefined)
       && (value['firewall.connectionlimit.udp.value'].length > 0)) {
        toolVariables.udpLimitValueUnit = value['firewall.connectionlimit.udp.value'] + '/second';
    }

    return toolVariables as VariablesForTheTool;
}
