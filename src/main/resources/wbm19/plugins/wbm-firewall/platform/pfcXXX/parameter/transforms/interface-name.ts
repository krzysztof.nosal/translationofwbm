export function createBridgeLabel(name: string, prefixForUnknownName: string | null = null): string {
    let match = BridgeNameSyntax.exec(name);
    if (match) {
        let prefix = match[1].toLowerCase();
        let number = match[2];

        if (prefix in labels) {
            let index = number ? ` ${parseInt(number) + 1}` : '';
            return `${labels[prefix]}${index}`
        }
    }

    let prefix = prefixForUnknownName ? `${prefixForUnknownName} ` : '';
    return `${prefix}${capitalizeFirstLetter(name)}`;
}

var BridgeNameSyntax: RegExp = /^([a-zA-Z]+)([0-9]*)$/;
var labels: { [prefix: string]: string; } = {
    "br": "Bridge",
    "wan": "WAN",
    "vpn": "VPN"
}

function capitalizeFirstLetter(str: string): string {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

export function isLegacyInterfaceName(name: string): boolean {
    return ['X1', 'X2'].indexOf(name) >= 0;
}
