/*!
 * wbm-runtime-information@1.1.2
 * 
 *   Copyright © 2020 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     
 * 
 *   Description:
 *     CODESYS and e!RUNTIME information.
 * 
 *   
 */
this["/get-possible-runtimes"]=function(e){var r={};function n(t){if(r[t])return r[t].exports;var i=r[t]={i:t,l:!1,exports:{}};return e[t].call(i.exports,i,i.exports,n),i.l=!0,i.exports}return n.m=e,n.c=r,n.d=function(e,r,t){n.o(e,r)||Object.defineProperty(e,r,{enumerable:!0,get:t})},n.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},n.t=function(e,r){if(1&r&&(e=n(e)),8&r)return e;if(4&r&&"object"==typeof e&&e&&e.__esModule)return e;var t=Object.create(null);if(n.r(t),Object.defineProperty(t,"default",{enumerable:!0,value:e}),2&r&&"string"!=typeof e)for(var i in e)n.d(t,i,function(r){return e[r]}.bind(null,i));return t},n.n=function(e){var r=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(r,"a",r),r},n.o=function(e,r){return Object.prototype.hasOwnProperty.call(e,r)},n.p="",n(n.s=0)}([function(e,r,n){"use strict";Object.defineProperty(r,"__esModule",{value:!0}),r.default=function(e){if(e instanceof Error)return{"runtime.codesys2.available":e,"runtime.eruntime.available":e,"runtime.availableversions.*.id":e,"runtime.availableversions.*.name":e};for(var r={0:"none",2:"CODESYS 2",3:"e!RUNTIME"},n={},t=0,i=0,u=["0","2","3"];i<u.length;i++){var o=u[i],a=e.indexOf(o)>=0;"2"===o&&(n["runtime.codesys2.available"]=a),"3"===o&&(n["runtime.eruntime.available"]=a),a&&(n["runtime.availableversions."+t+".name"]=r[o],n["runtime.availableversions."+t+".id"]=o,t++)}return n}}]).default;
//# sourceMappingURL=get-possible-runtimes.js.map