


interface RawTask { 
    taskName: string, 
    taskId: string, 
    cycleCount: string,
    cycleTime: string, 
    cycleTimeMin: string, 
    cycleTimeMax: string, 
    cycleTimeAcc: string, 
    status: string, 
    mode: string, 
    priority: string, 
    snUnit: string, 
    interval: string  
}

export default function(value: string | Error) {

    if (value instanceof Error) {
        return { 
            'runtime.codesys2.taskscount': value,
            'runtime.codesys2.tasks.*.index': value,
            'runtime.codesys2.tasks.*.name': value,
            'runtime.codesys2.tasks.*.id': value,
            'runtime.codesys2.tasks.*.cylecount': value,
            'runtime.codesys2.tasks.*.cyletime': value,
            'runtime.codesys2.tasks.*.minimumcyletime': value,
            'runtime.codesys2.tasks.*.maximumcyletime': value,
            'runtime.codesys2.tasks.*.averagecyletime': value,
            'runtime.codesys2.tasks.*.state': value,
            'runtime.codesys2.tasks.*.mode': value,
            'runtime.codesys2.tasks.*.priority': value,
            'runtime.codesys2.tasks.*.intervalunit': value,
            'runtime.codesys2.tasks.*.interval': value
        }
    }

    const tasks: RawTask[] = JSON.parse(value);
    const result: {[parameterId: string]: any} = {
        'runtime.codesys2.taskscount': tasks.length
    };

    tasks.forEach((task, index) => {
        result[`runtime.codesys2.tasks.${index}.index`]           = index;
        result[`runtime.codesys2.tasks.${index}.name`]            = task.taskName;
        result[`runtime.codesys2.tasks.${index}.id`]              = task.taskId;
        result[`runtime.codesys2.tasks.${index}.cylecount`]       = task.cycleCount;
        result[`runtime.codesys2.tasks.${index}.cyletime`]        = task.cycleTime;
        result[`runtime.codesys2.tasks.${index}.minimumcyletime`] = task.cycleTimeMin;
        result[`runtime.codesys2.tasks.${index}.maximumcyletime`] = task.cycleTimeMax;
        result[`runtime.codesys2.tasks.${index}.averagecyletime`] = task.cycleTimeAcc;
        result[`runtime.codesys2.tasks.${index}.state`]           = task.status;
        result[`runtime.codesys2.tasks.${index}.mode`]            = task.mode;
        result[`runtime.codesys2.tasks.${index}.priority`]        = task.priority;
        result[`runtime.codesys2.tasks.${index}.intervalunit`]    = task.snUnit;
        result[`runtime.codesys2.tasks.${index}.interval`]        = task.interval;
    });

    return result;
}
