/*!
 * wbm-runtime-information@1.1.2
 * 
 *   Copyright © 2020 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     
 * 
 *   Description:
 *     CODESYS and e!RUNTIME information.
 * 
 *   
 */
this["/get-task-list"]=function(e){var t={};function s(n){if(t[n])return t[n].exports;var r=t[n]={i:n,l:!1,exports:{}};return e[n].call(r.exports,r,r.exports,s),r.l=!0,r.exports}return s.m=e,s.c=t,s.d=function(e,t,n){s.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:n})},s.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},s.t=function(e,t){if(1&t&&(e=s(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(s.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var r in e)s.d(n,r,function(t){return e[t]}.bind(null,r));return n},s.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return s.d(t,"a",t),t},s.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},s.p="",s(s.s=1)}([,function(e,t,s){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.default=function(e){if(e instanceof Error)return{"runtime.codesys2.taskscount":e,"runtime.codesys2.tasks.*.index":e,"runtime.codesys2.tasks.*.name":e,"runtime.codesys2.tasks.*.id":e,"runtime.codesys2.tasks.*.cylecount":e,"runtime.codesys2.tasks.*.cyletime":e,"runtime.codesys2.tasks.*.minimumcyletime":e,"runtime.codesys2.tasks.*.maximumcyletime":e,"runtime.codesys2.tasks.*.averagecyletime":e,"runtime.codesys2.tasks.*.state":e,"runtime.codesys2.tasks.*.mode":e,"runtime.codesys2.tasks.*.priority":e,"runtime.codesys2.tasks.*.intervalunit":e,"runtime.codesys2.tasks.*.interval":e};var t=JSON.parse(e),s={"runtime.codesys2.taskscount":t.length};return t.forEach((function(e,t){s["runtime.codesys2.tasks."+t+".index"]=t,s["runtime.codesys2.tasks."+t+".name"]=e.taskName,s["runtime.codesys2.tasks."+t+".id"]=e.taskId,s["runtime.codesys2.tasks."+t+".cylecount"]=e.cycleCount,s["runtime.codesys2.tasks."+t+".cyletime"]=e.cycleTime,s["runtime.codesys2.tasks."+t+".minimumcyletime"]=e.cycleTimeMin,s["runtime.codesys2.tasks."+t+".maximumcyletime"]=e.cycleTimeMax,s["runtime.codesys2.tasks."+t+".averagecyletime"]=e.cycleTimeAcc,s["runtime.codesys2.tasks."+t+".state"]=e.status,s["runtime.codesys2.tasks."+t+".mode"]=e.mode,s["runtime.codesys2.tasks."+t+".priority"]=e.priority,s["runtime.codesys2.tasks."+t+".intervalunit"]=e.snUnit,s["runtime.codesys2.tasks."+t+".interval"]=e.interval})),s}}]).default;
//# sourceMappingURL=get-task-list.js.map