export default function (value: string) {
    //console.log("transform_operation_status got value: " + value);
    let result = {} as any;
    result['cc.operationstatus'] = value;
    return result;
}