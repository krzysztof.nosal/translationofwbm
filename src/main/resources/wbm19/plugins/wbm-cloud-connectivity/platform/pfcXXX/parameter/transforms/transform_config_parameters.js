/*!
 * wbm-cloud-connectivity@0.4.10
 * 
 *   Copyright © 2021 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     
 * 
 *   Description:
 *     Plugin for WBM-NG to configure and monitor Cloud Connectivity
 * 
 *   
 */
this["/transform_config_parameters"]=function(e){var t={};function c(n){if(t[n])return t[n].exports;var o=t[n]={i:n,l:!1,exports:{}};return e[n].call(o.exports,o,o.exports,c),o.l=!0,o.exports}return c.m=e,c.c=t,c.d=function(e,t,n){c.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:n})},c.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},c.t=function(e,t){if(1&t&&(e=c(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(c.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var o in e)c.d(n,o,function(t){return e[t]}.bind(null,o));return n},c.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return c.d(t,"a",t),t},c.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},c.p="",c(c.s=0)}([function(e,t,c){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.default=function(e){var t=$("#idOfTheSelectedConnection"),c="Connection"+t.val(),n=JSON.parse(e),o={};o["cc.cloudconnectionid"]=t.val(),o["cc.keepaliveinterval"]=n[c].KeepAliveInterval,o["cc.connectionenabled"]="true"==n[c].Enabled,o["cc.cloud"]=n[c].CloudType,o["cc.hostname"]=n[c].Host,o["cc.clientid"]=n[c].ClientId,o["cc.password"]=n[c].Password,o["cc.groupid"]=n[c].GroupId,o["cc.username"]=n[c].User,o["cc.enablewebsockets"]="MQTToverWS"==n[c].TransportProtocol,o["cc.enablecompression"]="GzipDefault"==n[c].Compression,o["cc.dataprotocol"]=n[c].MessagingProtocol,o["cc.cachemode"]=n[c].CacheMode,o["cc.cleansession"]="true"==n[c].CleanSession,o["cc.enabletls"]="true"==n[c].UseTLS,o["cc.portnumber"]=n[c].Port,o["cc.rootcafile"]=n[c].CaFile,o["cc.certfile"]=n[c].CertFile,o["cc.keyfile"]=n[c].KeyFile,o["cc.deviceinfo"]="true"==n[c].SendDeviceInfo,o["cc.devicestatus"]="true"==n[c].SendDeviceStatus,o["cc.standardcommands"]="true"==n[c].StandardCommandsEnabled,o["cc.lastwill"]="true"==n[c].LastWillEnabled,o["cc.lastwilltopic"]=n[c].LastWillTopic,o["cc.lastwillpayload"]=n[c].LastWillPayload,o["cc.lastwillqos"]=n[c].LastWillQoS,o["cc.lastwillretain"]="true"==n[c].LastWillRetain,o["cc.authentication"]=n[c].AuthenticationMethod,o["cc.messageproperty"]=n[c].MessageProperty;var r="#sub-menu-item-ccconnection"+t.val();$(r+" #theMassStorageIsReady").remove();var a=$('<input type="hidden" />');return a.attr("id","theMassStorageIsReady"),a.val(n[c].NonVolatileStorageIsAvailable),$(r).append(a),o}}]).default;
//# sourceMappingURL=transform_config_parameters.js.map