import { basename } from "path";
import { Parameter } from "wbm-base/dist/type/parameter/parameter";

export default function (value: string) {
    // NOTE: There is WBM-NG BUG: transformation gets called for every parameter, so unnecessary parsing of same JSON will occure !!!

    //console.log("transform_config_parameters got value: " + value);
    var cloudconnectionid = $('#idOfTheSelectedConnection');
    var connectionPropertyName = 'Connection' + cloudconnectionid.val();
    //console.log("Selected: " + connectionPropertyName);

    const json = JSON.parse(value);
    let result = {} as any;
    result['cc.cloudconnectionid'] = cloudconnectionid.val();
    result['cc.keepaliveinterval'] = json[connectionPropertyName].KeepAliveInterval;
    result['cc.connectionenabled'] = json[connectionPropertyName].Enabled == "true";
    result['cc.cloud'] = json[connectionPropertyName].CloudType;
    result['cc.hostname'] = json[connectionPropertyName].Host;
    result['cc.clientid'] = json[connectionPropertyName].ClientId;
    result['cc.password'] = json[connectionPropertyName].Password;
    result['cc.groupid'] = json[connectionPropertyName].GroupId;
    result['cc.username'] = json[connectionPropertyName].User;
    result['cc.enablewebsockets'] = json[connectionPropertyName].TransportProtocol == "MQTToverWS";
    result['cc.enablecompression'] = json[connectionPropertyName].Compression == "GzipDefault";
    result['cc.dataprotocol'] = json[connectionPropertyName].MessagingProtocol;
    result['cc.cachemode'] = json[connectionPropertyName].CacheMode;
    result['cc.cleansession'] = json[connectionPropertyName].CleanSession == "true";
    result['cc.enabletls'] = json[connectionPropertyName].UseTLS == "true";
    result['cc.portnumber'] = json[connectionPropertyName].Port;
    result['cc.rootcafile'] = json[connectionPropertyName].CaFile;
    result['cc.certfile'] = json[connectionPropertyName].CertFile;
    result['cc.keyfile'] = json[connectionPropertyName].KeyFile;
    result['cc.deviceinfo'] = json[connectionPropertyName].SendDeviceInfo == "true";
    result['cc.devicestatus'] = json[connectionPropertyName].SendDeviceStatus == "true";
    result['cc.standardcommands'] = json[connectionPropertyName].StandardCommandsEnabled == "true";
    result['cc.lastwill'] = json[connectionPropertyName].LastWillEnabled == "true";
    result['cc.lastwilltopic'] = json[connectionPropertyName].LastWillTopic;
    result['cc.lastwillpayload'] = json[connectionPropertyName].LastWillPayload;
    result['cc.lastwillqos'] = json[connectionPropertyName].LastWillQoS;
    result['cc.lastwillretain'] = json[connectionPropertyName].LastWillRetain == "true";
    result['cc.authentication'] = json[connectionPropertyName].AuthenticationMethod;
    result['cc.messageproperty'] = json[connectionPropertyName].MessageProperty;

    // We need following information within the DOM
    var infoElementId = 'theMassStorageIsReady';
    var parentSelector = '#sub-menu-item-ccconnection' + cloudconnectionid.val();
    $(parentSelector + ' #' + infoElementId).remove();
    var secretThing1 = $('<input type="hidden" />');
    secretThing1.attr('id', infoElementId);
    secretThing1.val(json[connectionPropertyName].NonVolatileStorageIsAvailable);
    $(parentSelector).append(secretThing1);
    
    return result;
}