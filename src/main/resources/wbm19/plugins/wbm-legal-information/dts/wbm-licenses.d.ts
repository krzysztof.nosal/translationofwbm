import { ViewController } from "wbm-base/type/view-controller";
import { Base } from "wbm-base/base";
export declare class WBMLicenses extends ViewController {
    constructor(base: Base);
    private didLoad;
    load(): Promise<void>;
}
