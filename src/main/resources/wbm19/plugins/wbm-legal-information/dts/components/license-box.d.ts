import { ViewController } from "wbm-base/type/view-controller";
import { Base } from "wbm-base/base";
import { LicenseLoader } from "../license-loader";
export declare class LicenseBox extends ViewController {
    private readonly packageId;
    title: string;
    private text;
    private loadingOrLoaded;
    private licenseLoader;
    constructor(base: Base, packageId: string, licenseLoader: LicenseLoader);
    load(): Promise<void>;
}
