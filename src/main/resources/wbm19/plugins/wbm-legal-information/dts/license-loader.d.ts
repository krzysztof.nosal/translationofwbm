export interface OSSPackageDescription {
    id: string;
    name: string;
    version: string;
}
export declare class LicenseLoader {
    private phpResourceUrl;
    constructor(phpResourceUrl: string);
    private data?;
    private loadingData?;
    private loadData;
    private loadOSSPackageLicense;
    getWagoSoftwareLicenseAgreement(): Promise<string>;
    getOSSDisclaimer(): Promise<string>;
    getOSSPackageList(): Promise<OSSPackageDescription[]>;
    getOSSPackageLicense(packageId: string): Promise<string>;
}
