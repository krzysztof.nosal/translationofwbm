import { ViewController } from "wbm-base/type/view-controller";
import { Base } from "wbm-base/base";
import { LicenseLoader } from "./license-loader";
export declare class OSSLicenses extends ViewController {
    private paragraph;
    private appendPackage;
    private licenseLoader;
    constructor(base: Base, licenseLoader: LicenseLoader);
    load(): Promise<void>;
}
