<?php

use PHPUnit\Framework\TestCase;

// used to override original
function override_file_get_contents() {

}

final class LicensesLoaderTest extends TestCase
{
    public function test_getOSSPackageForPackageId()
    {
        $packageId = 'license.packagename_1.2.3-packageversion';
        $expectedPackage = (object) [
            'id' => $packageId,
            'name' => 'packagename',
            'version' => '1.2.3-packageversion'
        ];

        $loader = new LicensesLoader('any/base/directory');
        $package = $loader->getOSSPackageForPackageId($packageId);

        $this->assertEquals(
            $expectedPackage,
            $package
        );
    }
}
