<?php

class LicensesLoader {

    private $wago_license_path;
    private $oss_disclaimer_path;
    private $oss_packages_base_path;

    public function __construct($basepath) {
        $this->wago_license_path = "$basepath/wago.txt";
        $this->oss_disclaimer_path = "$basepath/oss.txt";
        $this->oss_packages_base_path = "$basepath/oss";
    }

    public function getWagoLicenseAgreement() {
        return file_get_contents($this->wago_license_path);
    }

    public function getOSSDisclaimer() {
        return file_get_contents($this->oss_disclaimer_path);
    }

    public function getOSSPackageList() {
        $packages = (array) [];
        if (is_dir($this->oss_packages_base_path)) {
            foreach (scandir($this->oss_packages_base_path) as $file) {
                $filepath = "$this->oss_packages_base_path/$file";
                if (is_file($filepath)) {
                    array_push($packages, $this->getOSSPackageForPackagePath($filepath));
                }
            }
        }
        return $packages;
    }

    public function getOSSPackageForPackageId($packageId) {
        $package = str_replace('license.', '', $packageId);
        $package = str_replace('license-extension.', '', $package);
        $package = explode('_', $package);
        return (object) [
            'id' => $packageId,
            'name' => $package[0],
            'version' => $package[1]
        ];
    }

    public function getOSSPackageLicense($package) {
        $packagePath = $this->getOSSPackagePathForPackage($package);
        $content = file_get_contents($packagePath);
        return $content;
    }
    
    private function getOSSPackagePathForPackage($package) {
        $package = "$package->id.txt";
        return "$this->oss_packages_base_path/$package";
    }
    
    private function getOSSPackageForPackagePath($packagePath) {
        $id = basename($packagePath, '.txt');
        // we'll take the package filename without extension as ID
        return $this->getOSSPackageForPackageId($id);
    }
}






