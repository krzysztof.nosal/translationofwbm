<?php 

include_once './licenses.inc.php';

$loader = new LicensesLoader('/var/www/wbm/licenses');

$requestBody = json_decode(file_get_contents('php://input'), true);
$packageId = $requestBody['package'];

if (isset($packageId)) {

    $package = $loader->getOSSPackageForPackageId($packageId);
    $license = $loader->getOSSPackageLicense($package);

    $response = json_encode((object) [
        'package' => $package,
        'license' => $license
    ]);
    echo $response;
} 
else {

    $wago = $loader->getWagoLicenseAgreement();
    $oss = $loader->getOSSDisclaimer();
    $list = $loader->getOSSPackageList();
    
    $response = json_encode((object) [
        'wagoSoftwareLicenseAgreement' => $wago,
        'ossDisclaimer' => $oss,
        'ossPackages' => $list
    ]);
    echo $response;
}