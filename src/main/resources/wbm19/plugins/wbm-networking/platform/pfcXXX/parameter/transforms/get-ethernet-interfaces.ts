import { SpeedDuplexParameterValue, SpeedDuplexRaw } from './ethernet/speed-duplex';

interface InterfaceConfigData {
    autonegotiation: string;
    device: string;
    duplex: string;
    speed: number;
    state: string;
}

export default function getEthInterfaces(value: string | Error) {
    const ethernetBaseId = 'networking.ethernet.interfaces';

    if (value instanceof Error) {
        return {
            'networking.ethernet.interfaces.0.label': value,
            'networking.ethernet.interfaces.0.state': value,
            'networking.ethernet.interfaces.0.autonegotiation': value,
            'networking.ethernet.interfaces.0.speedduplex': value
        }
    }

    let interface_array = <InterfaceConfigData[]>JSON.parse(value);
    var naturalSort = require('typescript-natural-sort');
    var sorted_array = interface_array.sort((a, b) => naturalSort(a.device, b.device));

    const result = {} as any;
    sorted_array.forEach((iface, index) => {
        result[`${ethernetBaseId}.${index}.label`] = iface.device;
        result[`${ethernetBaseId}.${index}.state`] = iface.state === "up" ? true : false;
        if (iface.autonegotiation === 'on') {
            result[`${ethernetBaseId}.${index}.speedduplex`] = 'autonegotiation';
        }
        else {
            result[`${ethernetBaseId}.${index}.speedduplex`] = SpeedDuplexParameterValue.fromRawValues({ speed: iface.speed, duplex: iface.duplex } as SpeedDuplexRaw);
        }
    });
    return result;
}