import getEthInterfaces from "./get-ethernet-interfaces";
import { basename } from "path";


describe('service/parameter/mapping/custom/get-ethernet-interfaces', () => {

    describe('GetTheJSONString', () => {

        it('should return the JSON string from the config-tool', () => {


        });

    });


    describe('Map2Ports', () => {
        let configToolResponse = '["X1", "X2"]';

        it('should give the parameters for X1 and X2', () => {
            const parameter_result = getEthInterfaces(configToolResponse);
            expect(parameter_result['networking.ethernet.interfaces.0.label']).toEqual("X1");
            expect(parameter_result['networking.ethernet.interfaces.1.label']).toEqual("X2");
        });

    });


});