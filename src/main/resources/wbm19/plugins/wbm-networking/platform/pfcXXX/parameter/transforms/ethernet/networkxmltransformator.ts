import * as xmlConverter from 'xml-js';
import { ETHObject } from './ethobject';
import { NetworkSettings, TcpipSettings, EthernetSettings } from './networksetting';

export class NetworkXMLTransformator {
    
    constructor(){}
    /**
     *  transforms the given xmlstring into the Networksetting object
     * @param xmlstring 
     */
    public static transform(xmlstring:string): NetworkSettings {
        const convertedxml = this.convertXMLToETH_Object(xmlstring);
        const tcpipSettings = this.filterForIPSettings(convertedxml);
        const ethernetSettings = this.filterForEthSettings(convertedxml);
        return {
            tcpipSettings: this.loadTcpipSettings(tcpipSettings),
            ethernetSettings: this.loadEthernetSettings(ethernetSettings),
        };
    }

    /**
     * converts the given xml string into than ETH_Object
     * @param xmlString 
     */
    public static convertXMLToETH_Object(xmlString: string): ETHObject {
        return xmlConverter.xml2js(xmlString, {compact: true}) as ETHObject;
    }

    /**
     * filter all ip settings from given ETH_Object and returns an string array with filterd settings
     * @param eth_object 
     */
    public static filterForIPSettings(eth_object: ETHObject): any[]{
        return eth_object.interfaces.iface.filter(iface => 
            iface.hasOwnProperty('ip_settings')
        );
    }

    /**
     * filter all ip settings from given ETH_Object and returns an string array with filterd settings
     * @param eth_object 
     */
    public static filterForEthSettings(eth_object: ETHObject): any[]{
        return eth_object.interfaces.iface.filter(iface => 
            iface.hasOwnProperty('ethernet_settings')
        );
    }


    /**
     * generates Netwroksettings from given string array of type iface object
     * @param ifaces 
     */
    public static loadTcpipSettings(ifaces:any[]): TcpipSettings[]{
        return ifaces.map(iface => new TcpipSettings(iface));
    }

    /**
     * generates Netwroksettings from given string array of type iface object
     * @param ifaces 
     */
    public static loadEthernetSettings(ifaces:any[]): EthernetSettings[]{
        return ifaces.map(iface => new EthernetSettings(iface));
    }
}