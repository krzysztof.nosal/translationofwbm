import { BridgeConfiguration, createBridgeLabel } from "../../../../ethernet-component/bridge-config";

export default function getBridgeConfig(value: string|Error) {
    const bridgeBaseId = 'networking.ethernet.bridge';

    if (value instanceof Error) {
        return {
            [`${bridgeBaseId}.0.label`]: value,
            [`${bridgeBaseId}.0.name`]: value,
            [`${bridgeBaseId}.0.ports`]: value
        }
    }
    let config = <BridgeConfiguration> JSON.parse(value);

    const result = {} as any;

    Object.keys(config).sort().forEach((bridgeName, index) => {
        let ports = config[bridgeName];
        result[`${bridgeBaseId}.${index}.label`] = createBridgeLabel(bridgeName);
        result[`${bridgeBaseId}.${index}.name`] = bridgeName;
        result[`${bridgeBaseId}.${index}.ports`] = ports;
    });

    return result;
}