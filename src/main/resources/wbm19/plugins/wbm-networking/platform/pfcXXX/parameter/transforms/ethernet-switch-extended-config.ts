export default function switchExtendedConfig(value: string|Error) {

    if (value instanceof Error) {    
        return {
            'networking.ethernet.switch.extendedconfig': value
        }
    }
    else {
        const result = {} as any;

        result['networking.ethernet.switch.extendedconfig'] = true;

        // device is PAC (with marvel switch) - extended configuration is not available
        if(value.match(/^768-/)) {
            result['networking.ethernet.switch.extendedconfig'] = false;
        } 

        return result;
    }
};