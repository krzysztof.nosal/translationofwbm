export interface IpData {
    source: string;
    ipaddr: string;
    netmask: string;
    bcast: string;
}

export interface IpConfig {
    [bridge: string]: IpData;
}