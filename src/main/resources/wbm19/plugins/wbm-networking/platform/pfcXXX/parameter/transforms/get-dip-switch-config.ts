interface DipSwitchConfiguration {
    ipaddr: string;
    netmask: string;
    mode: string;
    value : number;
}

export default function getDipSwitchConfig(value: string|Error) {
    if (value instanceof Error) {
        return {
            'networking.tcpip.dipswitch.mode': "hw-not-available"
        }
    }

    let result = {} as any;
    let config = <DipSwitchConfiguration> JSON.parse(value);
    result['networking.tcpip.dipswitch.address'] = config.ipaddr;
    result['networking.tcpip.dipswitch.netmask'] = config.netmask;
    result['networking.tcpip.dipswitch.mode'] = config.mode;
    result['networking.tcpip.dipswitch.value'] = config.value;
    return result;
}