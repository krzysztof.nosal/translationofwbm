import {TcpipSettings, EthernetSettings} from './networksetting';

describe('service/parameter/mapping/custom/networksetting', () => {
    describe('TcpipSettings', () => {

        const ipsetting = {"ip_settings": {"show_in_wbm": {"_text": "1"},"port_name": {"_text": "X1"},"type": {"_text": "static"},"static_ipaddr": {"_text": "192.168.177.2"},"static_netmask": {"_text": "24"},"static_broadcast": {"_text": "192.168.42.255"}}};
        it('should create tcpip information from ip setting xml object', () => {
            const result = new TcpipSettings(ipsetting);
            expect(result.portname).toEqual("X1");
        });
        it('should have correct netmask', () =>{
            const result = new TcpipSettings(ipsetting);
            expect(result.static_netmask_cidr).toEqual('24');
            expect(result.static_netmask).toEqual('255.255.255.0')
        });
    });

    describe('EthernetSettings', () => {

        const ethsetting = {"state": {"_text": "enabled"}, "ethernet_settings": {"port_name":{"_text":"X1"},"autoneg":{"_text":"enabled"},"speed":{"_text":"100M"},"duplex":{"_text":"half"},"mac":{}}};
        it('should create ethernet information from ethernet settings xml object', () => {
            const result = new EthernetSettings(ethsetting);
            expect(result.portname).toEqual("X1");
        });
    });
});