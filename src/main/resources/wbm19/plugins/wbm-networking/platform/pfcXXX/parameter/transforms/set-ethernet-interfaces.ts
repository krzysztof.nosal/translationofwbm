import { SpeedDuplexParameterValue } from "./ethernet/speed-duplex";

type VariablesForTheTool = {
    'autonegotiation': 'on' | 'off'
    'duplex': 'full' | 'half'
    'speed': 10 | 100
    'state': 'up' | 'down'
}

export default function setEthInterfaces(value: { [parameterId: string]: any }) {
    const config = <VariablesForTheTool>{
        'speed': 100,
        'duplex': "full"
    };
    Object.keys(value).forEach(key => {
        let keyParts = key.split(".");
        switch (keyParts[keyParts.length - 1]) {
            case "state":
                config.state = value[key] ? "up" : "down";
                break;
            case "speedduplex":
                if (value[key] === "autonegotiation") {
                    config.autonegotiation = 'on';
                    break;
                }
                config.autonegotiation = 'off';
                const { speed, duplex } = SpeedDuplexParameterValue.getRawValues(value[key]);
                config.speed = speed;
                config.duplex = duplex;
                break;
            default:
                break;
        }
    });
    return config;
}