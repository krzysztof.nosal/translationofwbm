export default function (value: string|Error) {
    
    const baseId = 'networking.tcpip.dnsserver';
    if (value instanceof Error) {    
        return {
            [baseId]: value
        }
    } else {
        const ipAddresses = JSON.parse(value) as string[];
        let returnResult = {} as any;
        if( ipAddresses.length > 0){
            returnResult = ipAddresses.reduce((result: any, ipAddress, index) => {
                result[`${baseId}.${index}.ipaddress`] = ipAddress;
                result[`${baseId}.${index}.index`] = index + 1;
                return result;
            }, {} as any);

            returnResult[`${baseId}.noentry`] = 'elements exists';
        } else {
            returnResult[`${baseId}.noentry`] = 'none';
        }
        
        return returnResult;
    }
};