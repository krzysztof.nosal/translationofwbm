export default function (value: string|Error) {
    
    const baseId = 'networking.tcpip.dhcp.dnsserver';
    if (value instanceof Error) {    
        return {
            [baseId]: value
        }
    } else {
        let returnResult = {} as any;
               
        if(value == "") {
            returnResult[`${baseId}.noentry`] = 'none';
            return returnResult;
        }

        const ipAddresses = value.split('\t');
        if( ipAddresses.length > 0){
            returnResult = ipAddresses.reduce((result: any, ipAddress, index) => {
                
                result[`${baseId}.${index}.ipaddress`] = ipAddress;
                result[`${baseId}.${index}.index`] = index + 1;
                return result;
            }, {} as any);

            returnResult[`${baseId}.noentry`] = 'elements exists';
        }
        
        return returnResult;
    }
};