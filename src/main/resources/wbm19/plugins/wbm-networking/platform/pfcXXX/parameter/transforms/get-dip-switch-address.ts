import { DipSwitchConfiguration } from "../../../../ethernet-component/dip-switch-config";

export default function getDipSwitchConfig(value: string|Error) {
    if (value instanceof Error) {
        return {
            'networking.tcpip.dipswitch.address': value
        }
    }
    let config = <DipSwitchConfiguration> JSON.parse(value);
    return { 'networking.tcpip.dipswitch.address': config.address };
}