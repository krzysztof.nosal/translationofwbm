import { createBridgeLabel, sortWithPriority } from "../../../../ethernet-component/bridge-config";

interface IpData {
    source: string;
    ipaddr: string;
    netmask: string;
    bcast: string;
}

interface IpConfig {
    [bridge: string]: IpData;
}

export default function getIpConfig(value: string | Error) {
    const tcpIpBaseId = 'networking.tcpip.interfaces';
    if (value instanceof Error) {
        return {
            [`${tcpIpBaseId}.0.label`]: value,
            [`${tcpIpBaseId}.0.name`]: value,
            [`${tcpIpBaseId}.0.source`]: value,
            [`${tcpIpBaseId}.0.static.ipaddress`]: value,
            [`${tcpIpBaseId}.0.static.subnetmask`]: value
        }
    }

    const result = {} as any;
    const tcpipSettings = JSON.parse(value) as IpConfig;

    Object.keys(tcpipSettings).sort(sortWithPriority).forEach((bridgeName, index) => {
        var bridgeSettings = tcpipSettings[bridgeName];
        result[`${tcpIpBaseId}.${index}.label`] = createBridgeLabel(bridgeName);
        result[`${tcpIpBaseId}.${index}.name`] = bridgeName;
        result[`${tcpIpBaseId}.${index}.source`] = bridgeSettings.source;
        result[`${tcpIpBaseId}.${index}.static.ipaddress`] = bridgeSettings.ipaddr;
        result[`${tcpIpBaseId}.${index}.static.subnetmask`] = bridgeSettings.netmask;
    });
    return result;
};