/*!
 * wbm-networking@1.9.3
 * 
 *   Copyright © 2020 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     Marius Hellmeier <marius.hellmeier@wago.com>
 *   Johann Dück <johann.dueck@wago.com>
 *   Stefanie Meihöfer <stefanie.meihoefer@wago.com>
 * 
 *   Description:
 *     Network Configuration
 * 
 *   
 */
this["/set-ethernet-interfaces"]=function(e){var t={};function u(r){if(t[r])return t[r].exports;var l=t[r]={i:r,l:!1,exports:{}};return e[r].call(l.exports,l,l.exports,u),l.l=!0,l.exports}return u.m=e,u.c=t,u.d=function(e,t,r){u.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},u.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},u.t=function(e,t){if(1&t&&(e=u(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(u.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var l in e)u.d(r,l,function(t){return e[t]}.bind(null,l));return r},u.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return u.d(t,"a",t),t},u.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},u.p="",u(u.s=15)}({1:function(e,t,u){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),function(e){e.k10MbitHalf="10mbit-half-duplex",e.k10MbitFull="10mbit-full-duplex",e.k100MbitHalf="100mbit-half-duplex",e.k100MbitFull="100mbit-full-duplex"}(t.SpeedDuplexParameterValue||(t.SpeedDuplexParameterValue={})),function(e){e.getRawValues=function(t){switch(t){case e.k10MbitHalf:return{speed:10,duplex:"half"};case e.k10MbitFull:return{speed:10,duplex:"full"};case e.k100MbitHalf:return{speed:100,duplex:"half"};case e.k100MbitFull:default:return{speed:100,duplex:"full"}}},e.fromRawValues=function(e){var t=e.speed,u=e.duplex;return void 0===u&&(u="full"),void 0===t&&(t=100),(10===t?"10":"100")+"mbit-"+u+"-duplex"}}(t.SpeedDuplexParameterValue||(t.SpeedDuplexParameterValue={}))},15:function(e,t,u){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var r=u(1);t.default=function(e){var t={speed:100,duplex:"full"};return Object.keys(e).forEach((function(u){var l=u.split(".");switch(l[l.length-1]){case"state":t.state=e[u]?"up":"down";break;case"speedduplex":if("autonegotiation"===e[u]){t.autonegotiation="on";break}t.autonegotiation="off";var n=r.SpeedDuplexParameterValue.getRawValues(e[u]),a=n.speed,o=n.duplex;t.speed=a,t.duplex=o}})),t}}}).default;
//# sourceMappingURL=set-ethernet-interfaces.js.map