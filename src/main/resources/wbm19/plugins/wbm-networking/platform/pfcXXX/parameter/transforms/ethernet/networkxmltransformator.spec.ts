import { NetworkXMLTransformator } from './networkxmltransformator';

describe('service/parameter/mapping/custom/networkingxmltransformator', () => {

    describe('XMLTransormator', () => {
    
        describe('#convertXMLToETH_Object', () => {
            it('should convert XML to ETH_Object', () => {
                var xml = '<?xml version="1.0" encoding="UTF-8"?><interfaces>  <dsa_mode>0</dsa_mode>  <iface>    <device_name>eth0</device_name>    <state>enabled</state>    <no_dsa_disable>no</no_dsa_disable><promisc>off</promisc></iface><iface><device_name>ethX1</device_name><state>enabled</state><no_dsa_disable>no</no_dsa_disable><ethernet_settings><port_name>X1</port_name><autoneg>enabled</autoneg>      <speed>100M</speed>      <duplex>full</duplex></ethernet_settings>  </iface></interfaces>';
                var expected ={"_declaration":{"_attributes":{"version":"1.0","encoding":"UTF-8"}},"interfaces":{"dsa_mode":{"_text":"0"},"iface":[{"device_name":{"_text":"eth0"},"state":{"_text":"enabled"},"no_dsa_disable":{"_text":"no"},"promisc":{"_text":"off"}},{"device_name":{"_text":"ethX1"},"state":{"_text":"enabled"},"no_dsa_disable":{"_text":"no"},"ethernet_settings":{"port_name":{"_text":"X1"},"autoneg":{"_text":"enabled"},"speed":{"_text":"100M"},"duplex":{"_text":"full"}}}]}} as any;

                const result = NetworkXMLTransformator.convertXMLToETH_Object(xml);
                expect(result).toEqual(expected);
            });
        });

        describe('#filterForIPSettings', () =>{
            it('should filter for ipsettings elements', () => {
                var ethobject ={"_declaration":{"_attributes":{"version":"1.0","encoding":"UTF-8"}},"interfaces":{"dsa_mode":{"_text":"0"},"iface":[{"device_name":{"_text":"eth0"},"state":{"_text":"enabled"},"no_dsa_disable":{"_text":"no"},"promisc":{"_text":"off"}},{"device_name":{"_text":"ethX1"},"state":{"_text":"enabled"},"no_dsa_disable":{"_text":"no"},"ethernet_settings":{"port_name":{"_text":"X1"},"autoneg":{"_text":"enabled"},"speed":{"_text":"100M"},"duplex":{"_text":"full"},"mac":{}}},{"device_name":{"_text":"ethX2"},"state":{"_text":"enabled"},"no_dsa_disable":{"_text":"no"},"ethernet_settings":{"port_name":{"_text":"X2"},"autoneg":{"_text":"enabled"},"speed":{"_text":"100M"},"duplex":{"_text":"full"},"mac":{}}},{"device_name":{"_text":"br0"},"state":{"_text":"enabled"},"no_dsa_disable":{"_text":"no"},"bridge":{"dsa_slave":{"_text":"ethX1"},"no_dsa_slave":{"_text":"eth0"}},"ip_settings":{"show_in_wbm":{"_text":"1"},"port_name":{"_text":"X1"},"type":{"_text":"static"},"static_ipaddr":{"_text":"192.168.42.17"},"static_netmask":{"_text":"24"},"static_broadcast":{"_text":"192.168.42.255"}}},{"device_name":{"_text":"br1"},"state":{"_text":"enabled"},"no_dsa_disable":{"_text":"yes"},"bridge":{"dsa_slave":{"_text":"ethX2"},"no_dsa_slave":{}},"ip_settings":{"show_in_wbm":{"_text":"0"},"port_name":{"_text":"X2"},"type":{"_text":"static"},"static_ipaddr":{"_text":"192.168.2.17"},"static_netmask":{"_text":"24"},"static_broadcast":{"_text":"192.168.2.255"}}}]}} as any;
                const result = NetworkXMLTransformator.filterForIPSettings(ethobject);
                expect(result.length).toBe(2);    
            });
        });

        describe('#filterForEthSettings', () =>{
            it('should filter for ipsettings elements', () => {
                var ethobject ={"_declaration":{"_attributes":{"version":"1.0","encoding":"UTF-8"}},"interfaces":{"dsa_mode":{"_text":"0"},"iface":[{"device_name":{"_text":"eth0"},"state":{"_text":"enabled"},"no_dsa_disable":{"_text":"no"},"promisc":{"_text":"off"}},{"device_name":{"_text":"ethX1"},"state":{"_text":"enabled"},"no_dsa_disable":{"_text":"no"},"ethernet_settings":{"port_name":{"_text":"X1"},"autoneg":{"_text":"enabled"},"speed":{"_text":"100M"},"duplex":{"_text":"full"},"mac":{}}},{"device_name":{"_text":"ethX2"},"state":{"_text":"enabled"},"no_dsa_disable":{"_text":"no"},"ethernet_settings":{"port_name":{"_text":"X2"},"autoneg":{"_text":"enabled"},"speed":{"_text":"100M"},"duplex":{"_text":"full"},"mac":{}}},{"device_name":{"_text":"br0"},"state":{"_text":"enabled"},"no_dsa_disable":{"_text":"no"},"bridge":{"dsa_slave":{"_text":"ethX1"},"no_dsa_slave":{"_text":"eth0"}},"ip_settings":{"show_in_wbm":{"_text":"1"},"port_name":{"_text":"X1"},"type":{"_text":"static"},"static_ipaddr":{"_text":"192.168.42.17"},"static_netmask":{"_text":"24"},"static_broadcast":{"_text":"192.168.42.255"}}},{"device_name":{"_text":"br1"},"state":{"_text":"enabled"},"no_dsa_disable":{"_text":"yes"},"bridge":{"dsa_slave":{"_text":"ethX2"},"no_dsa_slave":{}},"ip_settings":{"show_in_wbm":{"_text":"0"},"port_name":{"_text":"X2"},"type":{"_text":"static"},"static_ipaddr":{"_text":"192.168.2.17"},"static_netmask":{"_text":"24"},"static_broadcast":{"_text":"192.168.2.255"}}}]}} as any;
                const result = NetworkXMLTransformator.filterForEthSettings(ethobject);
                expect(result.length).toBe(2);    
            });
        });

        describe('#loadTcpipSettings', () => {

            const testportname = "Test";
            const testIP = "192.168.42.17";
            const testcidr = "24";

            let ifaces: any = [{"ip_settings": {"show_in_wbm": {"_text": "1"},"port_name": {"_text": testportname },"type": {"_text": "static"},
                        "static_ipaddr": {"_text": testIP},
                        "static_netmask": {"_text": testcidr },
                        "static_broadcast": {"_text": "192.168.42.255"}}}];
                
            it('should load ip_settings with portname \"Test\"', () => {
                const result = NetworkXMLTransformator.loadTcpipSettings(ifaces);
                expect(result[0].portname).toEqual(testportname);
            });

            it('should load ip_settings with static_ipaddr \"192.168.42.17\"', () => {
                const result = NetworkXMLTransformator.loadTcpipSettings(ifaces);
                expect(result[0].static_ipaddr).toEqual(testIP);
            });

            it('should load ip_settings with static_netmask as cidr \"24\"', () => {
                const result = NetworkXMLTransformator.loadTcpipSettings(ifaces);
                expect(result[0].static_netmask_cidr).toEqual(testcidr);
            });

            it('should load two ip_settings items',() =>{
                ifaces.push({"ip_settings": {"show_in_wbm": {"_text": "1"},"port_name": {"_text": "X1"},"type": {"_text": "static"},"static_ipaddr": {"_text": "192.168.177.2"},"static_netmask": {"_text": "24"},"static_broadcast": {"_text": "192.168.42.255"}}});
                const result = NetworkXMLTransformator.loadTcpipSettings(ifaces);
                expect(result.length).toEqual(2);
            });
        });

        describe('#loadEthernetSettings', () => {

            const teststate = "enabled";
            const testportname = "X1";
            const testIP = "192.168.42.17";
            const testautoneg = "enabled";
            const testspeed = "10Mbit";
            const testduplex = "192.168.42.17";

            let ifaces = [{"state": {"_text": "enabled"}, "ethernet_settings": {"port_name":{"_text":testportname},"autoneg":{"_text":testautoneg},"speed":{"_text":testspeed},"duplex":{"_text":testduplex},"mac":{}}}];
             
            it('should load state', () => {
                const result = NetworkXMLTransformator.loadEthernetSettings(ifaces);
                expect(result[0].state).toEqual(teststate);
            });

            it('should load port name', () => {
                const result = NetworkXMLTransformator.loadEthernetSettings(ifaces);
                expect(result[0].portname).toEqual(testportname);
            });

            it('should load autonegotiation', () => {
                const result = NetworkXMLTransformator.loadEthernetSettings(ifaces);
                expect(result[0].autoneg).toEqual(testautoneg);
            });

            it('should load speed', () => {
                const result = NetworkXMLTransformator.loadEthernetSettings(ifaces);
                expect(result[0].speed).toEqual(testspeed);
            });

            it('should load duplex', () => {
                const result = NetworkXMLTransformator.loadEthernetSettings(ifaces);
                expect(result[0].duplex).toEqual(testduplex);
            });
        });
    });
});