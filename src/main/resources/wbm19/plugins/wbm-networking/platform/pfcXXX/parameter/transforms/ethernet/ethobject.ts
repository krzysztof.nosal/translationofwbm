export declare interface ETHObject {
    interfaces: {
        dsa_mode: {[key:string]:string},
        iface:[
            {
                [key:string]: {[key:string]:string | {[key:string]:string}}
            }
        ]
    }
}