/*!
 * wbm-networking@1.9.3
 * 
 *   Copyright © 2020 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     Marius Hellmeier <marius.hellmeier@wago.com>
 *   Johann Dück <johann.dueck@wago.com>
 *   Stefanie Meihöfer <stefanie.meihoefer@wago.com>
 * 
 *   Description:
 *     Network Configuration
 * 
 *   
 */
this["/get-ip-config-current"]=function(e){var r={};function t(n){if(r[n])return r[n].exports;var i=r[n]={i:n,l:!1,exports:{}};return e[n].call(i.exports,i,i.exports,t),i.l=!0,i.exports}return t.m=e,t.c=r,t.d=function(e,r,n){t.o(e,r)||Object.defineProperty(e,r,{enumerable:!0,get:n})},t.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},t.t=function(e,r){if(1&r&&(e=t(e)),8&r)return e;if(4&r&&"object"==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(t.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:e}),2&r&&"string"!=typeof e)for(var i in e)t.d(n,i,function(r){return e[r]}.bind(null,i));return n},t.n=function(e){var r=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(r,"a",r),r},t.o=function(e,r){return Object.prototype.hasOwnProperty.call(e,r)},t.p="",t(t.s=12)}({0:function(e,r,t){"use strict";Object.defineProperty(r,"__esModule",{value:!0}),r.DefaultBridgeName=/^(br|wwan)[\d]+$/,r.sortWithPriority=function(e,t,n){void 0===n&&(n=r.DefaultBridgeName);var i=n.test(e),u=n.test(t);return i&&!u?-1:!i&&u?1:e.localeCompare(t,void 0,{numeric:!0,sensitivity:"base"})},r.createBridgeLabel=function(e){var r,t=n.exec(e);if(t){var u=t[1],o=t[2];if(u in i){var a=parseInt(o);return i[u]+" "+(a+1)}}return"Bridge "+((r=e).charAt(0).toUpperCase()+r.slice(1))};var n=/^([a-zA-Z]+)([0-9]+)$/,i={br:"Bridge",wwan:"WWAN"}},12:function(e,r,t){"use strict";Object.defineProperty(r,"__esModule",{value:!0});var n=t(0);r.default=function(e){var r,t="networking.tcpip.interfaces";if(e instanceof Error)return(r={})[t+".0.current.ipaddress"]=e,r[t+".0.current.subnetmask"]=e,r;var i={},u=JSON.parse(e);return Object.keys(u).sort(n.sortWithPriority).forEach((function(e,r){var n=u[e];i[t+"."+r+".current.ipaddress"]=n.ipaddr,i[t+"."+r+".current.subnetmask"]=n.netmask})),i}}}).default;
//# sourceMappingURL=get-ip-config-current.js.map