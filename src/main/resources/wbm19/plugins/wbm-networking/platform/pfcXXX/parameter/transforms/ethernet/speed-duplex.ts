export enum SpeedDuplexParameterValue {
    k10MbitHalf = '10mbit-half-duplex',
    k10MbitFull = '10mbit-full-duplex',
    k100MbitHalf = '100mbit-half-duplex',
    k100MbitFull = '100mbit-full-duplex'
}

export interface SpeedDuplexRaw {
    speed: 10 | 100
    duplex: 'half' | 'full'
}

export namespace SpeedDuplexParameterValue {
    export function getRawValues(value: SpeedDuplexParameterValue): SpeedDuplexRaw {
        switch (value) {
            case SpeedDuplexParameterValue.k10MbitHalf: return {
                speed: 10,
                duplex: 'half'
            }
            case SpeedDuplexParameterValue.k10MbitFull: return {
                speed: 10,
                duplex: 'full'
            }
            case SpeedDuplexParameterValue.k100MbitHalf: return {
                speed: 100,
                duplex: 'half'
            }
            case SpeedDuplexParameterValue.k100MbitFull: return {
                speed: 100,
                duplex: 'full'
            }
            default: return {
                speed: 100,
                duplex: 'full'
            }
        }
    }

    export function fromRawValues({speed, duplex}: SpeedDuplexRaw): SpeedDuplexParameterValue {
        if (duplex === undefined) duplex = 'full';
        if (speed === undefined) speed = 100;
        return (<any>`${speed === 10 ? '10' : '100'}mbit-${duplex}-duplex`) as SpeedDuplexParameterValue;
    }
}