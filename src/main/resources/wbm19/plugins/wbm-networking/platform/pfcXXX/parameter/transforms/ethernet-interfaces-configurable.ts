export default function switchExtendedConfig(value: string|Error) {

    if (value instanceof Error) {    
        return {
            'networking.ethernet.interfaces.configurable': value
        }
    }
    else {
        const result = {} as any;

        result['networking.ethernet.interfaces.configurable'] = true;

        // device is PAC (with marvel switch) - extended configuration is not available
        if(value.match(/^768-/)) {
            result['networking.ethernet.interfaces.configurable'] = false;
        } 

        return result;
    }
};