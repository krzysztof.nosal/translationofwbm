import { Netmask } from 'netmask';


export class TcpipSettings {
    
    public readonly portname: string;
    public readonly type: string;
    public readonly static_ipaddr: string;
    public readonly static_netmask_cidr: string;
    public readonly static_broadcast: string;
    public readonly static_netmask: string;
    
    constructor(iface: any){
        this.portname = iface.ip_settings.port_name._text;
        this.type = iface.ip_settings.type._text;
        this.static_ipaddr = iface.ip_settings.static_ipaddr._text;
        this.static_netmask_cidr = iface.ip_settings.static_netmask._text;
        this.static_broadcast = iface.ip_settings.static_broadcast._text;
        this.static_netmask = this.generateNetmask(this.static_ipaddr, this.static_netmask_cidr);
    }

    private generateNetmask(ipaddr: string, cidr: string) : string{
        const netmaskblock = new Netmask(ipaddr+'/'+cidr);
        return netmaskblock.mask;
    }
}

export class EthernetSettings {

    public readonly portname: string;
    public readonly state: string;
    public readonly autoneg: string;
    public readonly speed: string;
    public readonly duplex: string;

    constructor(iface: any){
        this.state = iface.state._text
        this.portname = iface.ethernet_settings.port_name._text;
        this.autoneg = iface.ethernet_settings.autoneg._text;
        this.speed = iface.ethernet_settings.speed._text;
        this.duplex = iface.ethernet_settings.duplex._text;
    }
}

export interface NetworkSettings {
    tcpipSettings: TcpipSettings[]
    ethernetSettings: EthernetSettings[]
}