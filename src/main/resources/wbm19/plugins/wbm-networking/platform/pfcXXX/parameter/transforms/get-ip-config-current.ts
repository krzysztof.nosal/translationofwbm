import { sortWithPriority } from "../../../../ethernet-component/bridge-config";

interface IpData {
    source: string;
    ipaddr: string;
    netmask: string;
    bcast: string;
}

interface IpConfig {
    [bridge: string]: IpData;
}

export default function getIpConfig(value: string | Error) {
    const tcpIpBaseId = 'networking.tcpip.interfaces';
    if (value instanceof Error) {
        return {
            [`${tcpIpBaseId}.0.current.ipaddress`]: value,
            [`${tcpIpBaseId}.0.current.subnetmask`]: value,
        }
    }

    const result = {} as any;
    const tcpipSettings = JSON.parse(value) as IpConfig;

    Object.keys(tcpipSettings).sort(sortWithPriority).forEach((bridgeName, index) => {
        var bridgeSettings = tcpipSettings[bridgeName];
        result[`${tcpIpBaseId}.${index}.current.ipaddress`] = bridgeSettings.ipaddr;
        result[`${tcpIpBaseId}.${index}.current.subnetmask`] = bridgeSettings.netmask;
    });
    return result;
};
