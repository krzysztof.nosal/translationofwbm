type VariablesForTheTool = {
    'config': string
}

function endsWith(str: string, suffix: string) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
};

export default function (value: {[parameterId: string]: any}) {
    const toolVariables = {} as Partial<VariablesForTheTool>;

    let config = {}
    let parameters = Object.keys(value);
    let nameParameters = parameters.filter(parameter => endsWith(parameter, 'name'));

    nameParameters.forEach(parameter => {
        let bridgeName = value[parameter];
        let parts = parameter.split('.');
        let portsParameterName = parts.slice(0, parts.length - 1);
        portsParameterName.push('ports');

        let portsOfBridge = value[portsParameterName.join('.')];
        if (Array.isArray(portsOfBridge) && (portsOfBridge.length !== 0)) {
            config = {...config, [bridgeName]: portsOfBridge}
        }
    });

    toolVariables['config'] = JSON.stringify(config);
    return toolVariables as VariablesForTheTool;
}