import { IpConfig, IpData } from "./ip/ipobject"

type ParametersToBeCommited = {
    'networking.tcpip.interfaces.*.name': string,
    'networking.tcpip.interfaces.*.source': string,
    'networking.tcpip.interfaces.*.static.ipaddress': string,
    'networking.tcpip.interfaces.*.static.subnetmask': string
}

type VariablesForTheTool = {
    'json': string
}

export default function (value: {[parameterId: string]: any}) {
    const parameters = {} as Partial<ParametersToBeCommited>;
    const toolParameters = {} as Partial<VariablesForTheTool>;

    Object.keys(value).forEach(key => {
        let generic_key = key.replace(/\.[0-9]\.+/, '.*.');
        (parameters as any)[generic_key] = value[key];
    });

    const config = <IpConfig> {};
    config[parameters["networking.tcpip.interfaces.*.name"] as string] = <IpData> {
        source: parameters["networking.tcpip.interfaces.*.source"],
        ipaddr: parameters["networking.tcpip.interfaces.*.static.ipaddress"],
        netmask: parameters["networking.tcpip.interfaces.*.static.subnetmask"]
    }

    toolParameters.json = JSON.stringify(config);
    return toolParameters as VariablesForTheTool;
}