/*!
 * wbm-ports@1.7.6
 * 
 *   Copyright © 2020 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     Johann Dück <johann.dueck@wago.com>
 *   Marius Hellmeier <marius.hellmeier@wago.com>
 *   Stefanie Meihöfer <stefanie.meihoefer@wago.com>
 * 
 *   Description:
 *     Configuration of ports and services
 * 
 *   
 */
this["/get-dns-service-config"]=function(t){var n={};function e(r){if(n[r])return n[r].exports;var o=n[r]={i:r,l:!1,exports:{}};return t[r].call(o.exports,o,o.exports,e),o.l=!0,o.exports}return e.m=t,e.c=n,e.d=function(t,n,r){e.o(t,n)||Object.defineProperty(t,n,{enumerable:!0,get:r})},e.r=function(t){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0})},e.t=function(t,n){if(1&n&&(t=e(t)),8&n)return t;if(4&n&&"object"==typeof t&&t&&t.__esModule)return t;var r=Object.create(null);if(e.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:t}),2&n&&"string"!=typeof t)for(var o in t)e.d(r,o,function(n){return t[n]}.bind(null,o));return r},e.n=function(t){var n=t&&t.__esModule?function(){return t.default}:function(){return t};return e.d(n,"a",n),n},e.o=function(t,n){return Object.prototype.hasOwnProperty.call(t,n)},e.p="",e(e.s=2)}({2:function(t,n,e){"use strict";Object.defineProperty(n,"__esModule",{value:!0}),n.default=function(t){try{var n=JSON.parse(t)}catch(t){return{"networking.ports.dns.state":t,"networking.ports.dns.mode":t,"networking.ports.dns.allfixhosts":t,"networking.ports.dns.statichost.*.hostname":t,"networking.ports.dns.statichost.*.ipaddress":t,"networking.ports.dns.statichost.*.remainingafterdeletion":t}}var e={};return function(t,n){var e="networking.ports.dns",r="enabled"===n.dnsState,o=n.dnsMode;t[e+".state"]=r,t[e+".mode"]=o;var i=n.dnsFixHost.map((function(t){return t.ipAddr+":"+t.names.join("_")})).join(",");i&&(i+=","),t[e+".allfixhosts"]=i,function(t,n,e){for(var r=function(r){var o=n[r];t[e+"."+(r+0)+".hostname"]=o.names.join(" / "),t[e+"."+(r+0)+".ipaddress"]=o.ipAddr,t[e+"."+(r+0)+".remainingafterdeletion"]=n.filter((function(t){return t!==o})).map((function(t){return t.ipAddr+":"+t.names.join("_")})).join(",")},o=0;o<n.length;o++)r(o)}(t,n.dnsFixHost,e+".statichost")}(e,n),e}}}).default;
//# sourceMappingURL=get-dns-service-config.js.map