/*!
 * wbm-ports@1.7.6
 * 
 *   Copyright © 2020 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     Johann Dück <johann.dueck@wago.com>
 *   Marius Hellmeier <marius.hellmeier@wago.com>
 *   Stefanie Meihöfer <stefanie.meihoefer@wago.com>
 * 
 *   Description:
 *     Configuration of ports and services
 * 
 *   
 */
this["/get-dhcp-assigned-ntp-servers"]=function(e){var t={};function r(n){if(t[n])return t[n].exports;var i=t[n]={i:n,l:!1,exports:{}};return e[n].call(i.exports,i,i.exports,r),i.l=!0,i.exports}return r.m=e,r.c=t,r.d=function(e,t,n){r.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:n})},r.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},r.t=function(e,t){if(1&t&&(e=r(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(r.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var i in e)r.d(n,i,function(t){return e[t]}.bind(null,i));return n},r.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return r.d(t,"a",t),t},r.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},r.p="",r(r.s=0)}([function(e,t,r){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.default=function(e){if(e instanceof Error)return{"ntp.client.dhcp.timeserver.*.label":e,"ntp.client.dhcp.timeserver.*.address":e};var t=e.split("\n").filter((function(e){return!!e})),r=(t[t.length-1].split("=")[1]||"").replace(/"/g,""),n=r?r.split(" "):[],i={};return n.forEach((function(e,t){var r=""+(t+1);i["ntp.client.dhcp.timeserver."+t+".label"]=r,i["ntp.client.dhcp.timeserver."+t+".address"]=e})),i}}]).default;
//# sourceMappingURL=get-dhcp-assigned-ntp-servers.js.map