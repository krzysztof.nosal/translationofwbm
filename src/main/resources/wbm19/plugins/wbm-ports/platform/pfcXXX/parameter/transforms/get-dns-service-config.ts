
function createResult(result: any, responseObject: any) {

    const dnsBaseId = 'networking.ports.dns';
    
    const state = responseObject.dnsState === "enabled";
    const mode = responseObject.dnsMode;

    result[`${dnsBaseId}.state`] = state;
    result[`${dnsBaseId}.mode`] = mode;

    // create helper for all fix hosts used by 'add' method!
    let allfixedhosts = (responseObject.dnsFixHost as any[])
        .map(o => `${o.ipAddr}:${o.names.join('_')}`)
        .join(',');
    if (allfixedhosts) {
        allfixedhosts += ',';
    }
    result[`${dnsBaseId}.allfixhosts`] = allfixedhosts
    
    createStaticHosts(result, responseObject.dnsFixHost, `${dnsBaseId}.statichost`);
}

function createStaticHosts(result: any, hostList: any[], baseId: string) {
    
    let lastIndex: number = 0;
    
    for (let index = 0; index < hostList.length; index++) {
       const element = hostList[index] as any;

       result[`${baseId}.${index+lastIndex}.hostname`] = element.names.join(' / ');
       result[`${baseId}.${index+lastIndex}.ipaddress`] = element.ipAddr;

       // create helper parametert on contain all the others
       result[`${baseId}.${index+lastIndex}.remainingafterdeletion`] = hostList
            .filter(e => e !== element)
            .map(o => `${o.ipAddr}:${o.names.join('_')}`)
            .join(',');
    }
}

export default function getDHCPInformations(value: string) {
    try {
        var parsedJSONValue = JSON.parse(value);
    } catch (error) {
        return {
            'networking.ports.dns.state': error,
            'networking.ports.dns.mode': error,
            'networking.ports.dns.allfixhosts': error,
            'networking.ports.dns.statichost.*.hostname': error,
            'networking.ports.dns.statichost.*.ipaddress': error,
            'networking.ports.dns.statichost.*.remainingafterdeletion': error
        }
    }

    const result = {} as any;
    createResult(result, parsedJSONValue);
    return result;
};