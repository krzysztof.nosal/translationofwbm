/*!
 * wbm-ports@1.7.6
 * 
 *   Copyright © 2020 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     Johann Dück <johann.dueck@wago.com>
 *   Marius Hellmeier <marius.hellmeier@wago.com>
 *   Stefanie Meihöfer <stefanie.meihoefer@wago.com>
 * 
 *   Description:
 *     Configuration of ports and services
 * 
 *   
 */
this["/get-ntp-config"]=function(e){var t={};function r(n){if(t[n])return t[n].exports;var i=t[n]={i:n,l:!1,exports:{}};return e[n].call(i.exports,i,i.exports,r),i.l=!0,i.exports}return r.m=e,r.c=t,r.d=function(e,t,n){r.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:n})},r.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},r.t=function(e,t){if(1&t&&(e=r(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(r.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var i in e)r.d(n,i,function(t){return e[t]}.bind(null,i));return n},r.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return r.d(t,"a",t),t},r.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},r.p="",r(r.s=3)}({3:function(e,t,r){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.default=function(e){if(e instanceof Error)return{"ntp.client.state":e,"ntp.client.result":e,"ntp.client.update.interval":e,"ntp.client.timeserver.*.label":e,"ntp.client.timeserver.*.address":e};var t=JSON.parse(e),r={};r["ntp.client.state"]="enabled"===t.state,r["ntp.client.result"]="active"===t.activeState?"Time server available":"Time server not available until now",r["ntp.client.update.interval"]=t.updateTime;for(var n=0,i=[0,1,2,3];n<i.length;n++){var l=i[n],u=t.timeServerList[l]||"",a=""+(l+1);r["ntp.client.timeserver."+l+".label"]=a,r["ntp.client.timeserver."+l+".address"]=u}return r}}}).default;
//# sourceMappingURL=get-ntp-config.js.map