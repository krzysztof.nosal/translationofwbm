/*!
 * wbm-ports@1.7.6
 * 
 *   Copyright © 2020 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     Johann Dück <johann.dueck@wago.com>
 *   Marius Hellmeier <marius.hellmeier@wago.com>
 *   Stefanie Meihöfer <stefanie.meihoefer@wago.com>
 * 
 *   Description:
 *     Configuration of ports and services
 * 
 *   
 */
this["/get-dhcp-config"]=function(t){var e={};function r(n){if(e[n])return e[n].exports;var i=e[n]={i:n,l:!1,exports:{}};return t[n].call(i.exports,i,i.exports,r),i.l=!0,i.exports}return r.m=t,r.c=e,r.d=function(t,e,n){r.o(t,e)||Object.defineProperty(t,e,{enumerable:!0,get:n})},r.r=function(t){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0})},r.t=function(t,e){if(1&e&&(t=r(t)),8&e)return t;if(4&e&&"object"==typeof t&&t&&t.__esModule)return t;var n=Object.create(null);if(r.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:t}),2&e&&"string"!=typeof t)for(var i in t)r.d(n,i,function(e){return t[e]}.bind(null,i));return n},r.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return r.d(e,"a",e),e},r.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},r.p="",r(r.s=1)}([,function(t,e,r){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.default=function(t){try{var e=JSON.parse(t)}catch(t){return{"ports.dhcp.interface.*.state":t,"ports.dhcp.interface.*.iprange.first":t,"ports.dhcp.interface.*.iprange.last":t,"ports.dhcp.interface.*.leasetime":t,"ports.dhcp.interface.*.allfixhosts":t,"ports.dhcp.statichost.*.index":t,"ports.dhcp.statichost.*.name":t,"ports.dhcp.statichost.*.ip":t,"ports.dhcp.statichost.*.interface.index":t,"ports.dhcp.statichost.*.interface.label":t,"ports.dhcp.statichost.*.remainingafterdeletion":t}}var r={},n=0;return Object.keys(e).forEach((function(t,i){n+=function(t,e,r,n,i){var o="enabled"===e.dhcpdState,c=e.dhcpdRange.split("_"),a=c[0],p=c[1],s=e.dhcpdLeaseTime;t["ports.dhcp.interface."+n+".state"]=o,t["ports.dhcp.interface."+n+".iprange.first"]=a,t["ports.dhcp.interface."+n+".iprange.last"]=p,t["ports.dhcp.interface."+n+".leasetime"]=s;var d=e.dhcpdFixHost.map((function(t){return t.hostMac+"_"+t.ipAddr})).join(",");return d&&(d+=","),t["ports.dhcp.interface."+n+".allfixhosts"]=d,function(t,e,r,n,i,o){for(var c=function(c){var a=e[c];t[r+"."+(o+c)+".index"]=o+c,t[r+"."+(o+c)+".interface.index"]=n,t[r+"."+(o+c)+".interface.label"]=i,t[r+"."+(o+c)+".name"]=a.hostMac,t[r+"."+(o+c)+".ip"]=a.ipAddr,t[r+"."+(o+c)+".remainingafterdeletion"]=e.filter((function(t){return t!==a})).map((function(t){return t.hostMac+"_"+t.ipAddr})).join(",")},a=0;a<e.length;a++)c(a);return o+e.length}(t,e.dhcpdFixHost,"ports.dhcp.statichost",n,r,i)}(r,e[t],t,i,n)})),r}}]).default;
//# sourceMappingURL=get-dhcp-config.js.map