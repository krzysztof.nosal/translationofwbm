import { SuccessfulReadResult } from "wbm-base/type/parameter/read";
import { Base } from "wbm-base/base";

declare const base: Base;

// returns number of static hosts for the generated interface.
function createResult(result: any, interfaceObject: any, interfaceLabel: string, index: number, staticHostsCount: number): number {

    const dhcpBaseId = 'ports.dhcp';
    
    const state = interfaceObject.dhcpdState === "enabled";
    const [startRange, endRange] = interfaceObject.dhcpdRange.split('_');
    const leaseTime = interfaceObject.dhcpdLeaseTime;

    result[`${dhcpBaseId}.interface.${index}.state`] = state;
    result[`${dhcpBaseId}.interface.${index}.iprange.first`] = startRange;
    result[`${dhcpBaseId}.interface.${index}.iprange.last`] = endRange;
    result[`${dhcpBaseId}.interface.${index}.leasetime`] = leaseTime;

    // create helper for all fix hosts used by 'add' method!
    let allfixedhosts = (interfaceObject.dhcpdFixHost as any[])
        .map(o => `${o.hostMac}_${o.ipAddr}`)
        .join(',');
    if (allfixedhosts) {
        allfixedhosts += ',';
    }
    result[`${dhcpBaseId}.interface.${index}.allfixhosts`] = allfixedhosts
    

    return createStaticHosts(result, interfaceObject.dhcpdFixHost, `${dhcpBaseId}.statichost`, index, interfaceLabel, staticHostsCount);
}

// returns number of static hosts for the generated interface.
function createStaticHosts(result: any, hostList: any[], baseId: string, interfaceIndex: number, ifaceLabel: string, staticHostsCount: number): number {
    
    for (let index = 0; index < hostList.length; index++) {
       const element = hostList[index] as any;

       result[`${baseId}.${staticHostsCount + index}.index`] = staticHostsCount + index;
       result[`${baseId}.${staticHostsCount + index}.interface.index`] = interfaceIndex;
       result[`${baseId}.${staticHostsCount + index}.interface.label`] = ifaceLabel;
       result[`${baseId}.${staticHostsCount + index}.name`] = element.hostMac;
       result[`${baseId}.${staticHostsCount + index}.ip`] = element.ipAddr;

       // create helper parametert on contain all the others
       result[`${baseId}.${staticHostsCount + index}.remainingafterdeletion`] = hostList
            .filter(e => e !== element)
            .map(o => `${o.hostMac}_${o.ipAddr}`)
            .join(',');
    }

    return staticHostsCount + hostList.length;
}

export default function getDHCPInformations(value: string) {
    try {
        var parsedJSONValue = JSON.parse(value);
    } catch (error) {
        return {
            'ports.dhcp.interface.*.state': error,
            'ports.dhcp.interface.*.iprange.first': error,
            'ports.dhcp.interface.*.iprange.last': error,
            'ports.dhcp.interface.*.leasetime': error,
            'ports.dhcp.interface.*.allfixhosts': error,
            'ports.dhcp.statichost.*.index': error,
            'ports.dhcp.statichost.*.name': error,
            'ports.dhcp.statichost.*.ip': error,
            'ports.dhcp.statichost.*.interface.index': error,
            'ports.dhcp.statichost.*.interface.label': error,
            'ports.dhcp.statichost.*.remainingafterdeletion': error
        }
    }

    const result = {} as any;
    let staticHostsCount = 0;
    Object.keys(parsedJSONValue).forEach((interfaceLabel, index) => {
        staticHostsCount += createResult(result, parsedJSONValue[interfaceLabel], interfaceLabel, index, staticHostsCount);
    })

    return result;
};