export default function getNtpServersAssignedByDHCP(value: string|Error) {
    
    if (value instanceof Error) {    
        return {
            "ntp.client.dhcp.timeserver.*.label": value,
            "ntp.client.dhcp.timeserver.*.address": value
        }
    }
    else {
        const lines = value.split('\n').filter(l => !!l);
        const lastLine = lines[lines.length-1];
        const dhcpListValue = (lastLine.split('=')[1] || '').replace(/"/g, '');
        const dhcpAddressList = dhcpListValue ? dhcpListValue.split(' ') : [];
        const result = {} as any;

        // NTP Client Interfaces        
        const nptClientBaseId = 'ntp.client';
        
        dhcpAddressList.forEach((address, index) => {
            const label = `${index + 1}`;
            result[`${nptClientBaseId}.dhcp.timeserver.${index}.label`] = label;
            result[`${nptClientBaseId}.dhcp.timeserver.${index}.address`] = address;
        });

        return result;
    }
};