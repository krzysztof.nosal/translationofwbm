interface NTPConfigJSON{
    state: string;
    activeState: string;
    port: string;
    updateTime: string;
    timeServerList: string[];
 }

export default function getNtpConfig(value: string|Error) {
    
    if (value instanceof Error) {    
        return {
            "ntp.client.state": value,
            "ntp.client.result": value,
            "ntp.client.update.interval": value,
            "ntp.client.timeserver.*.label": value,
            "ntp.client.timeserver.*.address": value
        }
    }
    else {
        const jsonValue = JSON.parse(value) as NTPConfigJSON;

        const result = {} as any;

        // NTP Client Interfaces        
        const nptClientBaseId = 'ntp.client';
        result[`${nptClientBaseId}.state`] = jsonValue.state === 'enabled' ? true : false;
        result[`${nptClientBaseId}.result`] = jsonValue.activeState === 'active' ? "Time server available" : "Time server not available until now";
        result[`${nptClientBaseId}.update.interval`] = jsonValue.updateTime;

        for (const index of [0, 1, 2, 3]) {
            const address = jsonValue.timeServerList[index] || '';
            const label = `${index + 1}`;
            result[`${nptClientBaseId}.timeserver.${index}.label`] = label;
            result[`${nptClientBaseId}.timeserver.${index}.address`] = address;
        }

        return result;
    }
};