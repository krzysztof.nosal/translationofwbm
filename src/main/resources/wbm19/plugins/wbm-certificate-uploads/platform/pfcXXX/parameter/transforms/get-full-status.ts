interface RawJSONFormat {
    vpn : {
        certs: {
            [certName: string]: {
                [certAttribute: string]: any
            }
        },
        keys : string[]
    }
}

export default function(raw: string|Error) {

    if(raw instanceof Error) {
        return {
            'certificates.list.*.name': raw,
            'certificates.privatekeys.*.name': raw,
        };
    }

    const rawJSON = JSON.parse(raw) as RawJSONFormat;

    const keys = rawJSON.vpn.keys || [];
    const certs = Object.keys(rawJSON.vpn.certs || {});

    const results: {[paramId: string]: any} = {};
    
    keys.forEach((key, idx) => {
        results[`certificates.privatekeys.${idx}.name`] = key;
    });

    certs.forEach((cert, idx) => {
        results[`certificates.list.${idx}.name`] = cert;
    });

    return results;
}