/*!
 * wbm-certificate-uploads@1.0.2
 * 
 *   Copyright © 2020 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     
 * 
 *   Description:
 *     Plugin which adds certificate upload functionality as need e.g. to upload certificates used by OpenVPN
 * 
 *   
 */
this["/get-full-status"]=function(e){var t={};function r(n){if(t[n])return t[n].exports;var o=t[n]={i:n,l:!1,exports:{}};return e[n].call(o.exports,o,o.exports,r),o.l=!0,o.exports}return r.m=e,r.c=t,r.d=function(e,t,n){r.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:n})},r.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},r.t=function(e,t){if(1&t&&(e=r(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(r.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var o in e)r.d(n,o,function(t){return e[t]}.bind(null,o));return n},r.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return r.d(t,"a",t),t},r.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},r.p="",r(r.s=0)}([function(e,t,r){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.default=function(e){if(e instanceof Error)return{"certificates.list.*.name":e,"certificates.privatekeys.*.name":e};var t=JSON.parse(e),r=t.vpn.keys||[],n=Object.keys(t.vpn.certs||{}),o={};return r.forEach((function(e,t){o["certificates.privatekeys."+t+".name"]=e})),n.forEach((function(e,t){o["certificates.list."+t+".name"]=e})),o}}]).default;
//# sourceMappingURL=get-full-status.js.map