this['get-profibus-dp-slave-ssa-user-address'] = function (rawConfigtoolResultStringOrError) {

    // Check for error
    if (rawConfigtoolResultStringOrError instanceof Error)
    {
        return {
            'profibus.dp.slave.ssa.user.address' : rawConfigtoolResultStringOrError,
            'profibus.dp.slave.ssa.user.address.assignment': rawConfigtoolResultStringOrError
        }
    }

    // Check if some value is provided as address
    const assignment = rawConfigtoolResultStringOrError ? true : false

    return {
        'profibus.dp.slave.ssa.user.address' : rawConfigtoolResultStringOrError,
        'profibus.dp.slave.ssa.user.address.assignment': assignment
    }
}