export default function getProviderList(value: string|Error) {
    if (value instanceof Error) {    
        return {
            "modem.mobilenetwork.provider": value
        }
    }
    else {
        const result = {} as any;
        const json = JSON.parse(value);
        result['modem.mobilenetwork.provider'] = `${json['NetworkId']}|${json['NetworkType']}`;
        return result;
    }
};