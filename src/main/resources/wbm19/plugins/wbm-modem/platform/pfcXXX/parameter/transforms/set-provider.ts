
export default function getProviderList(value: {[key: string]: string}) {
    const mode = value['modem.mobilenetwork.selectionmode'];
    let providerId = '';
    let providerType = '';
    if (mode === 'MANUAL') {
        const [id, type]  = value['modem.mobilenetwork.provider'].split('|');
        providerId = `NetworkId=${id}`;
        providerType = `NetworkType=${type}`;
    }
    return { mode, providerId, providerType }
};