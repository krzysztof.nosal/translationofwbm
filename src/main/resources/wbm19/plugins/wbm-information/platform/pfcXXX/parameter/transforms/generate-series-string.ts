export default function generateSeriesString(value: string|Error) {
    if (value instanceof Error) {    
        return {
            'device.series': value
        }
    }
    else {
        const result = {} as any;

        // the default case is empty. 
        result['device.series'] = '';

        if(value.match(/^750-82/)) {
            result['device.series'] = "PFC";
        } else if(value.match(/^762-/)) {
            result['device.series'] = 'eDisplay';
        } else if(value.match(/^2850-3/)) {
            result['device.series'] = 'SRC';
        } else if(value.match(/^768-/)) {
            result['device.series'] = 'PAC';
        }

        result['device.ordernumber'] = value;

        return result;
    }
};