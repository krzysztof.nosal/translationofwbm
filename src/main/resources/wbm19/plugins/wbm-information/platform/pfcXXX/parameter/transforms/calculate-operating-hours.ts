export default function calculateOperatingHours(value: string|Error) {
    if (value instanceof Error) {    
        return {
            'device.operatinghours': value
        }
    }
    else {

        // operating hours should be in format years 

        let rawMinutes = parseInt(value, 10)!;
        let rawHours = Math.floor(rawMinutes / 60);
        let days = Math.floor(rawHours / 24);
        let hours = rawHours - days * 24;
        let minutes = rawMinutes - days * 24 * 60 - hours * 60;
        
        return {"device.operatinghours": `${days}d ${hours}h ${minutes}m` };
    }
}