export enum SpeedDuplexParameterValue {
    k10MbitHalf = '10mbit-half-duplex',
    k10MbitFull = '10mbit-full-duplex',
    k100MbitHalf = '100mbit-half-duplex',
    k100MbitFull = '100mbit-full-duplex'
}

export interface SpeedDuplexRaw {
    speed: '10M' | '100M' 
    duplex: 'half' | 'full'
}

export namespace SpeedDuplexParameterValue {
    export function getRawValues(value: SpeedDuplexParameterValue): SpeedDuplexRaw {
        switch (value) {
            case SpeedDuplexParameterValue.k10MbitHalf: return {
                speed: '10M',
                duplex: 'half'
            }
            case SpeedDuplexParameterValue.k10MbitFull: return {
                speed: '10M',
                duplex: 'full'
            }
            case SpeedDuplexParameterValue.k100MbitHalf: return {
                speed: '100M',
                duplex: 'half'
            }
            case SpeedDuplexParameterValue.k100MbitFull: return {
                speed: '100M',
                duplex: 'full'
            }
        }
    }

    export function fromRawValues({speed, duplex}: SpeedDuplexRaw): SpeedDuplexParameterValue {
        return (<any>`${speed === '10M' ? '10' : '100'}mbit-${duplex}-duplex`) as SpeedDuplexParameterValue;
    }
}