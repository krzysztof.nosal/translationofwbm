
type VariablesForTheTool = {[parameterId: string]: string};


/**
 * Set parameter text for configtool which writes the snmp general configuration values (besides state).
 * If parameter should be deleted (empty string), this must be signalized to configtool by writing \"\"
 * 
 * @param value input values from from
 */
export default function (value: {[parameterId: string]: string}) {

    const toolVariables = {} as Partial<VariablesForTheTool>;

    if("snmp.state" in value) {
        toolVariables["stateValue"] = value["snmp.state"];
    }

    if("snmp.devicename" in value) {
        toolVariables["deviceNameValue"] = (value["snmp.devicename"].length == 0) ? "\"\"" : value["snmp.devicename"];
    };

    if("snmp.devicedescription" in value) {
        toolVariables["descriptionValue"] = (value["snmp.devicedescription"].length == 0) ? "\"\"" : value["snmp.devicedescription"];
    };

    if("snmp.physicallocation" in value) {
        toolVariables["locationValue"] = (value["snmp.physicallocation"].length == 0) ? "\"\"" : value["snmp.physicallocation"];
    };

    if("snmp.contact" in value) {
        toolVariables["contactValue"] = (value["snmp.contact"].length == 0) ? "\"\"" : value["snmp.contact"];
    };

    if("snmp.objectid" in value) {
        toolVariables["objectIdValue"] = (value["snmp.objectid"].length == 0) ? "\"\"" : value["snmp.objectid"];
    };

    return toolVariables;
}
