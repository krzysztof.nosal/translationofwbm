/*!
 * wbm-snmp@1.0.0
 * 
 *   Copyright © 2019 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     
 * 
 *   Description:
 *     SNMP Configuration
 * 
 *   
 */
this["/general-config-values"]=function(e){var n={};function t(o){if(n[o])return n[o].exports;var r=n[o]={i:o,l:!1,exports:{}};return e[o].call(r.exports,r,r.exports,t),r.l=!0,r.exports}return t.m=e,t.c=n,t.d=function(e,n,o){t.o(e,n)||Object.defineProperty(e,n,{enumerable:!0,get:o})},t.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},t.t=function(e,n){if(1&n&&(e=t(e)),8&n)return e;if(4&n&&"object"==typeof e&&e&&e.__esModule)return e;var o=Object.create(null);if(t.r(o),Object.defineProperty(o,"default",{enumerable:!0,value:e}),2&n&&"string"!=typeof e)for(var r in e)t.d(o,r,function(n){return e[n]}.bind(null,r));return o},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,n){return Object.prototype.hasOwnProperty.call(e,n)},t.p="",t(t.s=0)}([function(e,n,t){"use strict";Object.defineProperty(n,"__esModule",{value:!0}),n.default=function(e){var n={};return"snmp.state"in e&&(n.stateValue=e["snmp.state"]),"snmp.devicename"in e&&(n.deviceNameValue=0==e["snmp.devicename"].length?'""':e["snmp.devicename"]),"snmp.devicedescription"in e&&(n.descriptionValue=0==e["snmp.devicedescription"].length?'""':e["snmp.devicedescription"]),"snmp.physicallocation"in e&&(n.locationValue=0==e["snmp.physicallocation"].length?'""':e["snmp.physicallocation"]),"snmp.contact"in e&&(n.contactValue=0==e["snmp.contact"].length?'""':e["snmp.contact"]),"snmp.objectid"in e&&(n.objectIdValue=0==e["snmp.objectid"].length?'""':e["snmp.objectid"]),n}}]).default;
//# sourceMappingURL=general-config-values.js.map