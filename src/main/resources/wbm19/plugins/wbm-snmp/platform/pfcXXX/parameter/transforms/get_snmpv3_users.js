/*!
 * wbm-snmp@1.0.0
 * 
 *   Copyright © 2019 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     
 * 
 *   Description:
 *     SNMP Configuration
 * 
 *   
 */
this["/get_snmpv3_users"]=function(e){var t={};function n(r){if(t[r])return t[r].exports;var u=t[r]={i:r,l:!1,exports:{}};return e[r].call(u.exports,u,u.exports,n),u.l=!0,u.exports}return n.m=e,n.c=t,n.d=function(e,t,r){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},n.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(n.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var u in e)n.d(r,u,function(t){return e[t]}.bind(null,u));return r},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,"a",t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p="",n(n.s=1)}([,function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.default=function(e){var t;return e instanceof Error?((t={})["snmp.v3.user"]=e,t):function(e,t){var n={};n[t+".count"]=e.length,e.length>0&&function(e,t,n){var r=0;e.forEach((function(e){t[n+"."+r+".index"]=r+1,t[n+"."+r+".authenticationname"]=e.authName,t[n+"."+r+".authenticationtype"]=e.authType,t[n+"."+r+".authenticationkey"]=e.authKey,t[n+"."+r+".privacy"]=e.privacy,t[n+"."+r+".privacykey"]=e.privacyKey,t[n+"."+r+".notificationreceiverip"]=e.notificationReceiver,r+=1}))}(e,n,t);return n}(JSON.parse(e),"snmp.v3.user")}}]).default;
//# sourceMappingURL=get_snmpv3_users.js.map