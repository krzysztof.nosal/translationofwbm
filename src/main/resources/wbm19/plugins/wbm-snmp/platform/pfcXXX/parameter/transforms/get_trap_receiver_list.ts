import { version } from "punycode";

interface trapReceiver{
    address: string,
    communityName: string,
    version: string
}

export default function (value:string|Error) {

    const baseId = 'snmp.v1v2c.trapreceiver';
    
    if(value instanceof Error){
        return{[baseId]: value};
    } else {
        let returnResult = {} as any;
        if(value !== "") {
            var jsonArray = JSON.parse(value) as Array<trapReceiver>;
            
            returnResult[`${baseId}.count`] = jsonArray.length;
            
            if(jsonArray.length > 0) {
                AddElementsToResult(jsonArray, returnResult, baseId);
            }
        } else {
            returnResult[`${baseId}.count`] = 0;
        }
        return returnResult;
    }
};

function AddElementsToResult(jsonItems: Array<trapReceiver>, returnResult: any, baseId: string) {
    let index=0;
    jsonItems.forEach(trapReceiverItem => {
        returnResult[`${baseId}.${index}.address`] = trapReceiverItem.address;
        returnResult[`${baseId}.${index}.communityname`] = trapReceiverItem.communityName;
        returnResult[`${baseId}.${index}.version`] = trapReceiverItem.version;
        index=index+1;
    });
}