/*!
 * wbm-snmp@1.0.0
 * 
 *   Copyright © 2019 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     
 * 
 *   Description:
 *     SNMP Configuration
 * 
 *   
 */
this["/get_trap_receiver_list"]=function(e){var r={};function t(n){if(r[n])return r[n].exports;var o=r[n]={i:n,l:!1,exports:{}};return e[n].call(o.exports,o,o.exports,t),o.l=!0,o.exports}return t.m=e,t.c=r,t.d=function(e,r,n){t.o(e,r)||Object.defineProperty(e,r,{enumerable:!0,get:n})},t.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},t.t=function(e,r){if(1&r&&(e=t(e)),8&r)return e;if(4&r&&"object"==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(t.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:e}),2&r&&"string"!=typeof e)for(var o in e)t.d(n,o,function(r){return e[r]}.bind(null,o));return n},t.n=function(e){var r=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(r,"a",r),r},t.o=function(e,r){return Object.prototype.hasOwnProperty.call(e,r)},t.p="",t(t.s=2)}({2:function(e,r,t){"use strict";Object.defineProperty(r,"__esModule",{value:!0}),r.default=function(e){var r,t="snmp.v1v2c.trapreceiver";if(e instanceof Error)return(r={})[t]=e,r;var n={};if(""!==e){var o=JSON.parse(e);n[t+".count"]=o.length,o.length>0&&function(e,r,t){var n=0;e.forEach((function(e){r[t+"."+n+".address"]=e.address,r[t+"."+n+".communityname"]=e.communityName,r[t+"."+n+".version"]=e.version,n+=1}))}(o,n,t)}else n[t+".count"]=0;return n}}}).default;
//# sourceMappingURL=get_trap_receiver_list.js.map