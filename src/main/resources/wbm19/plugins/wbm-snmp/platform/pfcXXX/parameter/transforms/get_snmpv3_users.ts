
interface v3Users {
    authName: string,
    authType: string,
    authKey: string,
    privacy: string,
    privacyKey: string,
    notificationReceiver: string   
};

export default function (value:string|Error){
    const baseId = 'snmp.v3.user';
    if(value instanceof Error){
        return {[baseId]: value};
    } else {
        const jsonArray = JSON.parse(value) as Array<v3Users>;
        return generateReturnResult(jsonArray, baseId);
    }
};

function generateReturnResult(value: Array<v3Users>, baseId: string){
    let returnResult = {} as any;
    returnResult[`${baseId}.count`] = value.length;
    
    if(value.length > 0) {
        AddElementsToResult(value, returnResult, baseId);
    }

    return returnResult
};

function AddElementsToResult(items: Array<v3Users>, returnResult: any, baseId: string) {
    let index=0;
    items.forEach(item => {
        returnResult[`${baseId}.${index}.index`] = index+1;
        returnResult[`${baseId}.${index}.authenticationname`] = item.authName;
        returnResult[`${baseId}.${index}.authenticationtype`] = item.authType;
        returnResult[`${baseId}.${index}.authenticationkey`] = item.authKey;
        returnResult[`${baseId}.${index}.privacy`] = item.privacy;
        returnResult[`${baseId}.${index}.privacykey`] = item.privacyKey;
        returnResult[`${baseId}.${index}.notificationreceiverip`] = item.notificationReceiver;
        index=index+1;
    });
};