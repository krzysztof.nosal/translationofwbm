/*!
 * wbm-create-image@1.1.2
 * 
 *   Copyright © 2019 WAGO Kontakttechnik GmbH & Co. KG
 * 
 *   License: 
 *     WAGO Software License Agreement
 * 
 *   Contributors:
 *     
 * 
 *   Description:
 *     Create bootable image
 * 
 *   
 */
this["/get-filesystem-data"]=function(e){var t={};function r(n){if(t[n])return t[n].exports;var i=t[n]={i:n,l:!1,exports:{}};return e[n].call(i.exports,i,i.exports,r),i.l=!0,i.exports}return r.m=e,r.c=t,r.d=function(e,t,n){r.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:n})},r.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},r.t=function(e,t){if(1&t&&(e=r(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(r.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var i in e)r.d(n,i,function(t){return e[t]}.bind(null,i));return n},r.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return r.d(t,"a",t),t},r.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},r.p="",r(r.s=0)}([function(e,t,r){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.default=function(e){if(e instanceof Error)return{"filesystem.devices.*.name":e,"filesystem.devices.*.medium":e,"filesystem.devices.*.description":e};try{var t=JSON.parse(e),r={},n="filesystem.devices";return t.forEach((function(e,t){var i,u;r[n+"."+t+".name"]=(i=e.deviceName.split("/"))[i.length-1],r[n+"."+t+".medium"]=e.deviceMedium,r[n+"."+t+".description"]="sd-card"===(u=e.deviceMedium)?"Memory Card":/^internal-flash.*$/.test(u)?"Internal Flash":u})),r}catch(e){return{"filesystem.devices.*.name":e,"filesystem.devices.*.medium":e,"filesystem.devices.*.description":e}}}}]).default;
//# sourceMappingURL=get-filesystem-data.js.map