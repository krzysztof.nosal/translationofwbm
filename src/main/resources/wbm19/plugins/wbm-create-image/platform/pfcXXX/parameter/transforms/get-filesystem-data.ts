interface DeviceData {
    deviceName: string, // /dev/bdfkjbsdf
    deviceMedium: string, // 'sd-card', 'internal-flash-emmc', etc
    deviceLabel: string // Name of SD-Card or internal device
}

function getMediumDescriptionForDeviceMedium(mediumName: string) {
    
    if (mediumName === 'sd-card') return 'Memory Card';
    
    if (/^internal-flash.*$/.test(mediumName)) return 'Internal Flash';
    
    // medium name should be the fallback
    return mediumName;
}

export default function (value: string|Error) {
    if (value instanceof Error) {
        return {
            'filesystem.devices.*.name': value,
            'filesystem.devices.*.medium': value,
            'filesystem.devices.*.description': value
        }
    } else {
        try {
            const deviceDataList = JSON.parse(value) as DeviceData[];
            let result = {} as any;
        
            const baseId = 'filesystem.devices';
            deviceDataList.forEach((deviceData, index) => {
                result[`${baseId}.${index}.name`] = ((path) => path[path.length-1])(deviceData.deviceName.split('/'));
                result[`${baseId}.${index}.medium`] = deviceData.deviceMedium;
                result[`${baseId}.${index}.description`] = getMediumDescriptionForDeviceMedium(deviceData.deviceMedium);
            })
            
            return result;
        } catch(e) {
            return {
                'filesystem.devices.*.name': e,
                'filesystem.devices.*.medium': e,
                'filesystem.devices.*.description': e
            }
        }
    }
};