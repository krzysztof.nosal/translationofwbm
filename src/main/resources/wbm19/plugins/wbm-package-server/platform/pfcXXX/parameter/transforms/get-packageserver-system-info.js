this['get-packageserver-system-info'] = function (rawConfigtoolResultStringOrError) {

    // Check for error
    if (rawConfigtoolResultStringOrError instanceof Error)
    {
        return {
            'packageserver.system.nonactive':   false,
            'packageserver.system1.active':     rawConfigtoolResultStringOrError,
            'packageserver.system1.configured': rawConfigtoolResultStringOrError,
            'packageserver.system1.state':      rawConfigtoolResultStringOrError,
            'packageserver.system2.active':     rawConfigtoolResultStringOrError,
            'packageserver.system2.configured': rawConfigtoolResultStringOrError,
            'packageserver.system2.state':      rawConfigtoolResultStringOrError
        }
    }

    var systemInfoArray = JSON.parse(rawConfigtoolResultStringOrError);
    var system1Index = 0;
    var system2Index = 0;
    for (let i = 0; i < systemInfoArray.length; ++i) {
        if (systemInfoArray[i]["system-number"] === 1) system1Index = i;
        if (systemInfoArray[i]["system-number"] === 2) system2Index = i;
    }

    return {
        'packageserver.system.nonactive':   false,
        'packageserver.system1.active':     systemInfoArray[system1Index].active,
        'packageserver.system1.configured': systemInfoArray[system1Index].configured,
        'packageserver.system1.state':      systemInfoArray[system1Index].state,
        'packageserver.system2.active':     systemInfoArray[system2Index].active,
        'packageserver.system2.configured': systemInfoArray[system2Index].configured,
        'packageserver.system2.state':      systemInfoArray[system2Index].state
    }
}