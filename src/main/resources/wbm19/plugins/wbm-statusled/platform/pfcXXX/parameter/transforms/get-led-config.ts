export default function getLedConfig(value: string|Error) {

    if (value instanceof Error) {
        return {
            'hardware.leds.*.name': value,
            'hardware.leds.*.color': value,
            'hardware.leds.*.message': value,
            'hardware.leds.*.date': value
        };
    }

    const result: {[key: string]: string} = {}
    const lines = value.split('\n');
    let count = 0;
    for (let line of lines) {
        if(line) {
            let name: string;
            let message: string;
            [name, line, message] = line.split(': ', 3);

            let color: string;
            let date: string;
            [color, date] = line.split(/ [0-9]+ [0-9]+ /, 2);

            result[`hardware.leds.${count}.name`]    = name;
            result[`hardware.leds.${count}.color`]   = color;
            result[`hardware.leds.${count}.message`] = `${date}\n${message}`;

            count++;
        }
    }
    return result;
};