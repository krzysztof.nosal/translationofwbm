<?php

/**
 * Readme 
 * 
 * As this file contains no critical data in the sense of security 
 * we don't take any steps to ensure the user is authenticated.
 * 
 * The contents of the zoneinfos file will therefore be accessible 
 * for any client requesting this PHP resource.
 */

 // get the lines of the file
$timezones = file_get_contents('/usr/share/zoneinfo/zoneinfos');
$timezones = explode("\n", $timezones);

// build a nice JSON formatted response object
foreach ($timezones as $index => $timezone) {

    if ($timezone) {
        $tzAndName = explode(' ', $timezone, 3);

        $timezones[$index] = [
            'tzstring' => $tzAndName[1],
            'timezone' => $tzAndName[2],
        ];
    } else {
        // remove 
        array_splice($timezones, $index, 1);
    }

}
echo json_encode($timezones);