<?php

include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';
include_once __DIR__.'/mapping.inc.php';
include_once __DIR__.'/infos.inc.php';
include_once __DIR__.'/access_rights.inc.php';
include_once __DIR__.'/exec_rights.inc.php';



class Authorization
{
    private $errorLogger;
    private $parameterMappings;
    private $parameterInfos;
    private $accessRights;
    private $execRights;
    private $authList       = false;
    private $userRoles      = ["guest", "user", "admin"];

    // example for complete auth list entry
    // {
    //    "configtool" => "some_very_important_tool",
    //    "parameter" => ["param_1", "param_2", "param_3"],
    //    "accessRights" => ["user" => "read", "admin" = "readwrite"]
    // }


    /**
     * Constructor
     * 
     */
    public function __construct($errorLogger = false, $parameterMappings = false, $parameterInfos = false, $accessRights = false, $execRights = false)
    {
        $this->errorLogger = $errorLogger ? $errorLogger : new ErrorLogger();
        $this->parameterMappings = $parameterMappings ? $parameterMappings : new ParameterMapping();
        $this->parameterInfos = $parameterInfos ? $parameterInfos : new ParameterInfos();
        $this->accessRights = $accessRights ? $accessRights : new AccessRights();
        $this->execRights = $execRights ? $execRights : new ExecRights();
        $this->authList = array();
    }


    /**
     * Initialize authorization data, either via data of session, or create it ourself by parameter mappings and infos.
     * 
     * TODO: a extra object for session and session lifetime handling must be created. After that, we can use this object to
     * save the data in session ourself. Actually, the old "session lifetime" functionality is used, so we deliver session
     * handling to calling superior script.
     * 
     * @param array object authorization list, got from 
     * @return error if an error occured
     *         boolena true in case of no error
     */
    public function Init($authList = false)
    {
        // if authList is given, just take it
        if($authList)
        {
            $this->authList = $authList;
        }
        else
        {
            $parameterMappingData = $this->parameterMappings->convertParamMappingContents();
            $parameterInfoData = $this->parameterInfos->convertParamInfoContents();

            if(($parameterMappingData instanceof WBMError) || ($parameterInfoData instanceof WBMError))
            {
                $error = new WBMError(ERROR_GROUP_AUTHORIZATION, ERROR_CODE_AUTH_DATA_INIT, "Can't read parameter data"); 
                $this->errorLogger->logError($result);
            }
            else
            {
                $this->addMappingsToAuthList($parameterMappingData);
                $this->addParameterInfosToAuthList($parameterInfoData);
            }
        }

        //var_dump($this->authList);
        return isset($error) ? $error : true;
    }

    /**
     * Find out, if access to configtool is allowed for specified user roles.
     * 
     * @param string name of configtool
     * @param array string user roles
     * @return boolean true: access is allowed, false: access is not allowed
     *         object error
     */
    public function configtoolAccessAllowed($configtoolName, $userRoles)
    {
        $result = false;

        // configtool is unknown
        $configtoolEntry = $this->findConfigtoolInAuthList($configtoolName);
        if(!$configtoolEntry)
        {
            $result = new WBMError(ERROR_GROUP_AUTHORIZATION, ERROR_CODE_AUTH_CONFIGTOOL_MISSING, "Missing authorization data (configtool unknown): ".$configtoolName);
            $this->errorLogger->logError($result);
        }
        // configtool is known, but authorization data was not specified yet
        else if(!isset($configtoolEntry->accessRights) && !isset($configtoolEntry->executionRights))
        {
            $result = new WBMError(ERROR_GROUP_AUTHORIZATION, ERROR_CODE_AUTH_DATA_MISSING, "Missing authorization data for configtool: ".$configtoolName);
            $this->errorLogger->logError($result);
        }
        else
        {
            // loop through all requested user roles, if it is specified in auth list and has an access right (not only "")
            foreach($userRoles as $userRole)
            {
                // once we find any access write for one of the specified user roles, we know, that access is allowed
                if(isset($configtoolEntry->accessRights[$userRole]) && strlen($configtoolEntry->accessRights[$userRole]))
                {
                    $result = true;
                    break;
                }

                // once we find any access write for one of the specified user roles, we know, that access is allowed
                else if(in_array($userRole, $configtoolEntry->executionRights))
                {
                    $result = true;
                    break;
                }
            }
        }

        return $result;
    }


    /**
     * Set whole authorization list. Intended to reload data from session
     * 
     * @param array object authorization list
     * 
     */
    public function setAuthData($authList)
    {
        $this->authList = $authList;
    }


    /**
     * Get whole authorization list. Intended to sava data in session
     * 
     * @return array object authorization list
     * 
     */
    public function getAuthData()
    {
        return $this->authList;
    }


    /**
     * Add information from mapping entries to authorization list.
     * 
     * @param array object mapping entries
     * 
     */
    public function addMappingsToAuthList($mappings)
    {
        foreach($mappings as $mapping)
        {
            $configtoolName = explode(" ", $mapping->command)[0];
            // check if configtool is already existing in authorization list, else create new entry
            $authListEntry = $this->findConfigtoolInAuthList($configtoolName);
            if(!$authListEntry)
            {
                // name of configtool is the fist word in command string
                $authListEntry = (object) array(
                    "configtool" => $configtoolName,
                    "parameter" => array(),
                    "methods" => array()
                );
                array_push($this->authList, $authListEntry);
            }

            // add all parameters for read and write access by this configtool to authorization list
            if (isset($mapping->reads)) {
                foreach($mapping->reads as $parameter)
                {
                    if(!in_array($parameter, $authListEntry->parameter))
                    {
                        array_push($authListEntry->parameter, $parameter);
                    }
                }
            }
            if (isset($mapping->writes)) {
                foreach($mapping->writes as $parameter)
                {
                    if(!in_array($parameter, $authListEntry->parameter))
                    {
                        array_push($authListEntry->parameter, $parameter);
                    }
                }
            }
            if (isset($mapping->executes)) {
                foreach($mapping->executes as $method)
                {
                    if(!in_array($method, $authListEntry->methods))
                    {
                        array_push($authListEntry->methods, $method);
                    }
                }
            }
        }
        //var_dump($this->authList);
    }


    /**
     * Add information from parameter info entries to authorization list.
     * paramter infos include the information of parameter ids and access rights.
     * This must be connected with the configtool informations already given by mapping infos.
     * 
     * @param array object parameter info entries
     * 
     */
    public function addParameterInfosToAuthList($parameterInfos)
    {
        // handle all new parameter infos
        foreach($parameterInfos as $parameterInfo)
        {
            // handle all authorization entries
            foreach($this->authList as $authEntry)
            {
                // found new parameter in authorization list?
                if(isset($authEntry->parameter) && in_array($parameterInfo->id, $authEntry->parameter))
                {
                    // add authorization info to authorization entry
                    $authEntry->accessRights = isset($authEntry->accessRights) ? $authEntry->accessRights : array();
                    //var_dump($parameterInfo->accessRights);
                    $authEntry->accessRights = $this->accessRights->mergeAccessRights($authEntry->accessRights, $parameterInfo->accessRights, "higherst");
                } 
                
                // found new method in authorization list?
                else if(isset($authEntry->methods) && in_array($parameterInfo->id, $authEntry->methods))
                {
                    // add authorization info to authorization entry
                    $authEntry->executionRights = isset($authEntry->executionRights) ? $authEntry->executionRights : array();
                    $authEntry->executionRights = $this->execRights->mergeExecRights($authEntry->executionRights, $parameterInfo->executionRights);
                }
            }
        }
        //var_dump($this->authList);
    }
    

    /**
     * Search after entry of specified configtool in authorization list.
     * 
     * @param string configtool name
     * @return object configtool entry
     *         boolean false, if configtool was not found
     */
    private function findConfigtoolInAuthList($configtoolName)
    {
        $configtoolEntry = false;

        foreach($this->authList as $authEntry)
        {
            if(isset($authEntry->configtool) && ($authEntry->configtool === $configtoolName))
            {
                $configtoolEntry = $authEntry;
                break;
            }
        }

        return $configtoolEntry;
    }

};