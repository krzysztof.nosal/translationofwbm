<?php

include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';
include_once __DIR__.'/../authentication/wbm_session.inc.php';
include_once __DIR__.'/../authentication/user.inc.php';
include_once __DIR__.'/authorization.inc.php';
include_once __DIR__.'/configtools.inc.php';

include_once __DIR__.'/../authentication/session_lifetime.inc.php';


define(SUCCESS, 0);
define(ERROR, -1);


/**
 * Check if access to configtool is allowed for specified user.
 * If authorization data not existing, access is also not allowed.
 * After authorization data is extracted the first time, it is stored in session, to reuse it at next call.
 * Read and store to session takes place in this function, because authorization object has no access to session.
 * 
 * TODO create generall session object and give it to authorization object (and others) to store its data on its own
 * 
 * @param string configtool name
 * @param array userroles
 * @return boolean true access is allowed
 *                 false access is not allowed
 *         WBMError
 */
function AccessAllowedForUser($configtool, $userroles = [])
{
  $result = false;
  $authorization = new Authorization();

  $initResult = $authorization->init($_SESSION["parameterAuthData"]);
  if(!$initResult instanceof WBMError)
  {
    if(!isset($_SESSION["parameterAuthData"]))
    {
      $_SESSION["parameterAuthData"] = $authorization->getAuthData();
    }

    $result = $authorization->configtoolAccessAllowed($configtool, $userroles);
  }
  return $result;
}


function CallConfigtool($configtoolObj, &$resultObj, $userroles)
{
  $status       = SUCCESS;
  $resultString = "";
  $errorText    = "";
  $callString   = "";
  $dataId       = isset($configtoolObj["dataId"]) ? $configtoolObj["dataId"] : "";

  if(!isset($configtoolObj))
  {
    $status     = ERROR;
    $errorText  = "Missing input";
    $error = new WBMError(ERROR_GROUP_MISC, ERROR_CODE_INVALID_INPUT, "Missing input"); 
  }

  else if(!Configtools::configtoolNameIsValid($configtoolObj["name"]))
  {
    $status     = ERROR;
    $errorText  = "Invalid configtool name (".$configtoolObj["name"].")";
    $error = new WBMError(ERROR_GROUP_MISC, ERROR_CODE_INVALID_INPUT, "Invalid configtool name (".$configtoolObj["name"].")"); 
  }

  else if(!file_exists(sprintf("/etc/config-tools/%s", $configtoolObj["name"])))
  {
    $status     = ERROR;
    $errorText  = "Unknown configtool (".$configtoolObj["name"].")";
    $error = new WBMError(ERROR_GROUP_MISC, ERROR_CODE_INVALID_INPUT, "Unknown configtool (".$configtoolObj["name"].")"); 
  }

  else 
  {
    // check if access is allowed for actual user
    $accessAllowedResult = AccessAllowedForUser($configtoolObj["name"], $userroles);
    if($accessAllowedResult instanceof WBMError)
    {
      $status = ACCESS_NOT_ALLOWED;
      $errorText = "Access not allowed";
      $error = $accessAllowedResult;      
    }
    else if(!$accessAllowedResult)
    {
      $status     = ACCESS_NOT_ALLOWED;
      $errorText  = "Access not allowed.";
      $error      = new WBMError(ERROR_GROUP_CONFIGTOOL, ERROR_CODE_CONFIGTOOL_ACCESS_NOT_ALLOWED, "Access not allowed");
    }
    else
    {
      // by now, generally call all configtools with sudo
      $callString = "sudo ";

      // create string to call configtool by linux shell - first directory and configtool name
      $callString = $callString."/etc/config-tools/".$configtoolObj["name"];

      // now all configtool parameters, one after the other
      $paramNo = 0;
      while(isset($configtoolObj["parameter"][$paramNo]))
      {
        $paramString = Configtools::decodeRequestStringFromBrowser($configtoolObj["parameter"][$paramNo]);

        // decode '%' and '+', because they have special meanings in the encoded characters
        $paramString = str_replace("%", "%25", $paramString);
        $paramString = str_replace("+", "%2b", $paramString);

        // decode characters in url format, which otherwise confuse the bash
        $paramString = str_replace(" ", "%20", $paramString);
        $paramString = str_replace("'", "%27", $paramString);
        $paramString = str_replace('"', "%22", $paramString);
        $paramString = str_replace('`', "%60", $paramString);
        $paramString = str_replace('*', "%2a", $paramString);
        $paramString = str_replace('$', "%24", $paramString);

        // escape parameter string for bash
        // (add (single!) quotation marks around parameter string to let the bash don't interprete "(", ")", and so on
        // and mask quotation mark itself inside string)
        // "escapeshellarg" does not work, because it strippes UTF8 characters from string and setlocale does not help
        
        $paramString = "'".str_replace("'", "'\\''", $paramString)."'";

        // add parameter to call string
        $callString = $callString." ".$paramString;

        ++$paramNo;
      }

      $dummyOutput;

      // multiline output expected - use special shell call, which transfers more than one output line
      if(isset($configtoolObj["multiline"]) && $configtoolObj["multiline"])
      {
        $resultString = shell_exec($callString);
      }

      // normal call of configtool
      else 
      {
        $resultString = exec($callString, $dummyOutput, $status);
      }

      if($resultString == NULL) 
      {
        $resultString = "";
      }

      // Is this really needed?! 
      // Makes things quite complicated on the client side...
      // I've commented that out until we've discussed that
      // $resultString = Configtools::encodeResultStringForBrowser($resultString);

      if(0 != $status)
      {
        $lastErrorFilename = "/tmp/last_error.txt";
        if(file_exists($lastErrorFilename) && !is_link($lastErrorFilename))
        {
          $errorText = file_get_contents($lastErrorFilename);
          //unlink($lastErrorFilename);
          exec(sprintf("sudo /bin/rm %s", $lastErrorFilename));
        }
      }
    }
  }

  // to return call string (only!) for debugging, comment out next line meanwhile
  $callString = "";

  $resultObj = array('status'       => $status,
                     'resultString' => $resultString,
                     'errorText'    => $errorText,
                     'dataId'       => $dataId,
                     'callString'   => $callString);
  if(isset($error))
  {
    $resultObj["error"] = $error;
  }
}


$aDeviceResponse  = [];
$configtoolCallResponse = [];
$configtoolResponse = (object) [];

// get request json string by stdin
$requestString    = file_get_contents("php://input");
//echo "requestString:".$requestString;

if($requestString)
{
  $request = json_decode($requestString, false);
}

if(($requestString === false) || ($request === NULL))
{
  $error = new WBMError(ERROR_GROUP_MISC, ERROR_CODE_INVALID_INPUT, "Wrong request data format"); 
  $configtoolResponse->error = $error;
}
else
{
  // initialize objects
  $errorLogger      = new ErrorLogger();
  $filesystemUtils  = new FilesystemUtils();
  $user             = new User();
  $wbmSession       = new WbmSession($errorLogger, $filesystemUtils);

  // handle wmb session
  $checkWbmSessionResult = $wbmSession->handleWbmSession($request, $configtoolResponse);

  if(!$checkWbmSessionResult instanceof WBMError)
  {
    $sessionUsername = $wbmSession->getSessionUsername();
    //$sessionUsername = "admin"; // for tests
    $userroles = $user->getUserrolesForUser($sessionUsername);

    // loop over all requested configtool calls
    //foreach($request["aDeviceParams"] as $configtoolCallParams)
    foreach($request->aDeviceParams as $configtoolCallParams)
    {
      CallConfigtool((array) $configtoolCallParams, $configtoolCallResponse, $userroles);
      $aDeviceResponse[] = $configtoolCallResponse;
    }
  }
  $configtoolResponse->aDeviceResponse = $aDeviceResponse;
}

// encode responses to json string and send it to stdout
echo json_encode($configtoolResponse);

?>
