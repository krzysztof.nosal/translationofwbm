<?php

include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';

/**
 * Helper class for handling access rights
 */
class AccessRights
{
    private $errorLogger;

     /**
     * Constructor
     * 
     */
    public function __construct($errorLogger = false)
    {
        $this->errorLogger = $errorLogger ? $errorLogger : new ErrorLogger();
    }

    /**
     * Return most significant ("higherst") access right from several single rights.
     * 
     * @param string access right 1
     * @param string access right 2
     * @return string most significant access right
     * 
     * TODO refactoring - remove function in parameter info object an use this
     * 
     */
    public function getMostSignificantAccessRight($accessRight1, $accessRight2)
    {
        $resultAccessRight = '';

        if(($accessRight1 === 'readwrite') || ($accessRight2 === 'readwrite'))
        {
            $resultAccessRight = 'readwrite';
        }
        else if((($accessRight1 === 'read') || ($accessRight2 === 'read')))
        {
            $resultAccessRight = 'read';
        }
        return $resultAccessRight;
    }


    /**
     * Return less significant ("lowerst") access right from several single rights.
     * 
     * @param string access right 1
     * @param string access right 2
     * @return string less significant access right
     * 
     */
    public function getLessSignificantAccessRight($accessRight1, $accessRight2)
    {
        $resultAccessRight = 'readwrite';

        if(($accessRight1 === '') || ($accessRight2 === ''))
        {
            $resultAccessRight = '';
        }
        else if(($accessRight1 === 'read') || ($accessRight2 === 'read'))
        {
            $resultAccessRight = 'read';
        }

        return $resultAccessRight;
    }


    /**
     * Merge two arrays with access rights for several users togeter.
     * if one user is existing in both arrays, decide by means of order
     * 
     * @param array access rights 1
     * @param array access rights 2
     * @param string order - "lowerst" or "higherst". if not given, "lowerst" is default
     */
    public function mergeAccessRights($accessRights1, $accessRights2, $order = "lowerst")
    {
        // take initial first access rights array
        $resultAccessRights = $accessRights1;

        // loop through access rights of second array
        foreach($accessRights2 as $userRole => $accessRight)
        {
            // user role not existing yet, take access right over
            if(!isset($resultAccessRights[$userRole]))
            {
                //array_push($resultAccessRights, $accessRight);
                $resultAccessRights[$userRole] = $accessRight;
            }

            // access rights for user role is already existing, but different
            else if($resultAccessRights[$userRole] !== $accessRight)
            {
                if($order === "lowerst")
                {
                    $resultAccessRights[$userRole] = $this->getLessSignificantAccessRight($accessRights1[$userRole], $accessRight);
                }
                else
                {
                    $resultAccessRights[$userRole] = $this->getMostSignificantAccessRight($accessRights1[$userRole], $accessRight);
                }

            }
        }
        return $resultAccessRights;
    }

}
