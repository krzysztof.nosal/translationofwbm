<?php

include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/../plugins/pluginloader.inc.php';
include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';
include_once __DIR__.'/access_rights.inc.php';


class ParameterInfos
{
    private $baseParamInfoPath;
    private $filesystemUtils;
    private $pluginloader;
    private $errorLogger;
    private $accessRights;


    /**
     * Constructor
     * 
     * @param directory where parameter info files are placed, 
     *        if not given (= false), use default 'parameter/infos'
     * @param object filesystem utils
     *        if not given (false), instance an own one
     * @param object pluginloader
     *        if not given (false), instance an own one
     */
    public function __construct($baseParamInfoPath = false, $filesystemUtils = false, $pluginloader = false, $errorLogger = false, $accessRights = false)
    {
        $this->baseParamInfoPath = $baseParamInfoPath ? $baseParamInfoPath : 'parameter/infos';
        $this->filesystemUtils = $filesystemUtils ? $filesystemUtils : new FilesystemUtils();
        $this->pluginloader = $pluginloader ? $pluginloader : new Pluginloader();
        $this->errorLogger = $errorLogger ? $errorLogger : new ErrorLogger();
        $this->accessRights = $accessRights ? $accessRights : new AccessRights();
    }


    /**
     * Change private data which was rather created in constructor.
     * Intended for unit tests with mock environment via phpunit.
     */
    public function attach($baseParamInfoPath = false, $filesystemUtils = false, $pluginloader = false)
    {
        $this->__construct($baseParamInfoPath, $filesystemUtils, $pluginloader);
    }


    /**
     * Get (relative) directory where parameter info files are placed
     * 
     * @return string
     */
    public function getBaseParamInfoPath()
    {
        return $this->baseParamInfoPath;
    }


    /**
     * Get absolute path of (basic) parameter info files in filesystem.
     * 
     * @return string
     */
    public function getBaseParamInfoPathAbsolute()
    {
        $path = $this->filesystemUtils->getWbmPathAbsolute().'/'.$this->baseParamInfoPath;
        //echo "\npath: ".$path;
        return $path;
    }

    /**
     * Get directorys (with absolute path) including parameter info files
     * Direct access to filesytem
     * 
     * @return array directorys
     */
    public function getParamInfoDirectorys()
    {
        // get basic param info directory
        $paramInfoDirectorys = array($this->getBaseParamInfoPathAbsolute());

        // get (paths of) all plugins
        $pluginPlatforms = $this->pluginloader->getPluginsPlatformPathArray();
        foreach($pluginPlatforms as $pluginPlatformPath)
        {
            // add specified name of parameter info directory to plugin path
            array_push($paramInfoDirectorys, $pluginPlatformPath.'/'.$this->getBaseParamInfoPath());
        }
        //echo "\nparamInfoDirectorys: "; var_dump($paramInfoDirectorys);
        return $paramInfoDirectorys;

    }

    /**
     * Get all parameter info files (basic and plugins)
     * Direct access to filesytem
     * 
     * @return array parameter info files (with path)
     */
    public function getParamInfoFilepaths()
    {
        $paramInfoFilepaths = array();

        foreach($this->getParamInfoDirectorys() as $paramInfoDirectory)
        {
            $paramInfoFilepaths = array_merge($paramInfoFilepaths, $this->filesystemUtils->getFilepathsOfDirectory($paramInfoDirectory));
        }
        //echo "\nparamInfoFilepaths: "; var_dump($paramInfoFilepaths);
        return $paramInfoFilepaths;
    }


    /**
     * Convert content of parameter info files to object, handle format errors.
     * 
     * @param array information/content about all parameter info files (e.g. for unit test)
     *              or false, if content should be read from filesystem first
     * @return array object converted parameter infos
     */
    public function convertParamInfoContents($paramInfoContents = false)
    {
        // if parameter mappings are not given (in unit test), get them from filesystem
        if(!$paramInfoContents)
        {
            $paramInfoContents = $this->filesystemUtils->getFileContents($this->getParamInfoFilepaths());
        }

        $paramInfoArray = [];
        foreach ($paramInfoContents as $paramInfoContent)
        {
            if(isset($paramInfoContent->error))
            {
                // log detail error information (with filepath) and return less details
                $this->errorLogger->logError($paramInfoContent->error);
                $error = new WBMError(ERROR_GROUP_PARAM_INFOS, ERROR_CODE_READ_PARAM_INFO, "Can't read parameter info");
                // transfer error object to the same strukture as parameter infos for equal treatment later
                $paramInfoArray = array_merge($paramInfoArray, array(0 => (object) array("error" => $error)));
            }
            else if(isset($paramInfoContent->content))
            {
                $paramInfo = json_decode($paramInfoContent->content);
                if(!$paramInfo)
                {
                    $error = new WBMError(ERROR_GROUP_PARAM_INFOS, ERROR_CODE_PARAM_INFO_FORMAT, "Parameter info format error ".$paramInfoContent->filepath);
                    $this->errorLogger->logError($error);
                    $error->text = "Parameter info format error";
                    $paramInfoArray = array_merge($paramInfoArray, array(0 => (object) array("error" => $error)));
                }
                else
                {
                    $paramInfoArray = array_merge($paramInfoArray, $paramInfo);
                }
            }
        }
        //var_dump($paramInfoArray);
        return $paramInfoArray;
    }


    /**
     * Check if parameter info data is complete.
     * Required data: ID, access rights
     * 
     * @param object parameter info
     * @return boolean true, if info is complete
     */
    private function paramInfoDataComplete($paramInfo)
    {
        $dataComplete = false;
        if(isset($paramInfo->id) && (isset($paramInfo->accessRights) || isset($paramInfo->executionRights)))
        {
            $dataComplete = true;
        }
        return $dataComplete;
    }


    /**
     * Seach after id in parameter info array
     * 
     * @param object parameter info array
     * @param string id
     * @return object parameter info, if id is existing
     *         boolean false if not
     */
    private function getParamInfoViaId($paramInfoArray, $id)
    {
        $resultParamInfo = false;
        foreach($paramInfoArray as $paramInfo)
        {
            if(isset($paramInfo->id) && $paramInfo->id === $id)
            {
                $resultParamInfo = $paramInfo;
                break;
            }
        }
        return $resultParamInfo;
    }


    /**
     * Check if given id is already existing in parameter info array
     * 
     * @param object parameter info array
     * @param string id
     * @return boolean true, if id is already existing
     */
    private function paramInfoIdExisting($paramInfoArray, $id)
    {
        $idExisting = false;
        if($this->getParamInfoViaId($paramInfoArray, $id)) 
        {
            $idExisting = true;
        }
        return $idExisting;
    }



    /**
     * Check parameter infos relating to
     * - missing data (ID)
     * - double IDs
     * - invalid IDs
     * 
     * @param array parameter infos
     * @return array checked parameter infos including only correct ones
     */
    public function check($paramInfos)
    {
        $resultParamInfos = [];

        foreach($paramInfos as $paramInfo)
        {
            // wave errors through
            if(isset($paramInfo->error))
            {
                array_push($resultParamInfos, $paramInfo);
                continue;
            }
            // incomplete parameter data
            if(!$this->paramInfoDataComplete($paramInfo))
            {
                $error = new WBMError(ERROR_GROUP_PARAM_INFOS, ERROR_CODE_PARAM_INFO_INCOMPLETE, "Parameter info incomplete");
                $this->errorLogger->logError($error);
                array_push($resultParamInfos, (object) array("error" => $error));
                continue;
            }

            // invalid parameter id
            $validIdPattern = '/^[a-z][a-z0-9]+(\.[a-z][a-z0-9]+)*(\.\*)?(\.[a-z][a-z0-9]+)*$/';
            if(0 === preg_match($validIdPattern, $paramInfo->id))
            {
                $error = new WBMError(ERROR_GROUP_PARAM_INFOS, ERROR_CODE_PARAM_INFO_ID_INVALID, "Parameter info id invalid error ".$paramInfo->id);
                $this->errorLogger->logError($error);
                $error->text = "Parameter info id invalid error";
                array_push($resultParamInfos, (object) array("error" => $error));
                continue;
            }

            array_push($resultParamInfos, $paramInfo);
        }
        return $resultParamInfos;
    }


    /**
     * Merge multible parameter info entries (same parameter id) to one.
     * Respected members are access rights (lowerst right ist winner) and writable flag (true in case of differences).
     * For all other members the values of the first parameter info are taken.
     * 
     * @param array parameter infos
     * @return array parameter infos without multible parameter ids
     */
    public function merge($paramInfos)
    {
        $resultParamInfos = [];

        foreach($paramInfos as $paramInfo)
        {
            // wave errors through
            if(isset($paramInfo->error))
            {
                array_push($resultParamInfos, $paramInfo);
                continue;
            }
            // ignore incomplete parameter data (check should have been already done in check function)
            if(!$this->paramInfoDataComplete($paramInfo))
            {
                continue;
            }
            // double parameter id
            $existingParamInfo = $this->getParamInfoViaId($resultParamInfos, $paramInfo->id);
            if($existingParamInfo)
            {
                // log information about merge, but don't return error
                $error = new WBMError(ERROR_GROUP_PARAM_INFOS, ERROR_CODE_PARAM_INFO_ID_DOUBLE, "Parameter info merge: ".$paramInfo->id);
                $this->errorLogger->logError($error);

                // merge access rights
                $newAccessRights = $this->accessRights->mergeAccessRights((array) $existingParamInfo->accessRights, (array) $paramInfo->accessRights, "higherst");
                $existingParamInfo->accessRights = (object) $newAccessRights;

                // merge writable flag
                $newWritableFlag = $this->mergeWritableFlag($existingParamInfo->writable, $paramInfo->writable);
                $existingParamInfo->writable = $newWritableFlag;
                continue;
            }
            // parameter info not buggy or double - take it over to output array
            array_push($resultParamInfos, $paramInfo);
        }
        return $resultParamInfos;

    }


    /**
     * Check if (parameter) access is allowed depending on access mode.
     * 
     * @param string parameter access right
     * @param string access mode
     * @return boolean true, if access is allowed
     * 
     */
    private function accessAllowedForMode($paramAccessRight, $accessMode)
    {
        $accessAllowed = false;
        if($accessMode === 'read')
        {
            if(($paramAccessRight === 'read') || ($paramAccessRight === 'readwrite'))
            {
               $accessAllowed = true;      
            }
        }
        else if($accessMode === 'write')
        {
            if($paramAccessRight === 'readwrite')
            {
                $accessAllowed = true;
            }
        }
        return $accessAllowed;
    } 


    /**
     * merge writable flag of two (double) parameter infos
     * 
     * @param boolean writable flag 1
     * @param boolean writable flag 2
     * @return boolean merged flag - true, if one of the input flags is true
     */
    private function mergeWritableFlag($writableFlag1, $writableFlag2)
    {
        $resultWritableFlag = false;
        if($writableFlag1 || $writableFlag2)
        {
            $resultWritableFlag = true;
        }
        return $resultWritableFlag;
    }


    /**
     * Check if access to parameter is allowed for given user roles.
     * 
     * TODO move to user management object
     * 
     * @param array access rights of parameter
     * @param array user roles
     * @param string access (default is read)
     * @return boolean true, if access is allowed
     */
    private function accessAllowed($paramAccessRights, $userRoles, $access = 'read')
    {
        $accessAllowed = false;

        foreach($userRoles as $userRole)
        {
            foreach($paramAccessRights as $paramUserRole => $paramAccessRight)
            {
                if($paramUserRole === $userRole)
                {
                    if($accessAllowed = $this->accessAllowedForMode($paramAccessRight, $access))
                    {
                       break; 
                    }
                }
            }
        }
        return $accessAllowed;
    }

    
    /**
     * Check if execution of method is allowed for given user roles.
     * 
     * @param array execution rights of methds, wich is an array of user roles actually
     * @param array user roles, that the current user ownes
     * @return boolean true, if access is allowed
     */
    private function executionAllowed($paramExecRights, $userRoles)
    {
        return count(array_intersect($paramExecRights, $userRoles)) > 0;
    }

    /**
     * Filter parameter infos for access rights.
     * 
     * @param array parameter infos
     * @param array user roles
     * @return array filtered parameter infos (only permitted entries)
     */
    public function filterForAccessRights($paramInfos, $userRoles)
    {
        $resultParamInfos = [];

        foreach($paramInfos as $paramInfo)
        {
            if(isset($paramInfo->error))
            {
                array_push($resultParamInfos, $paramInfo);
            }
            else if(isset($paramInfo->accessRights) && $this->accessAllowed($paramInfo->accessRights, $userRoles))
            {
                $paramInfo->accessRights = $this->convertAccessRights($paramInfo->accessRights, $userRoles);
                array_push($resultParamInfos, $paramInfo);
            }
        }
        return $resultParamInfos;
    }

    /**
     * Filter parameter infos for access rights.
     * 
     * @param array parameter infos
     * @param array user roles
     * @return array filtered parameter infos (only permitted entries)
     */
    public function filterForExecutionRights($paramInfos, $userRoles)
    {
        $resultParamInfos = [];

        foreach($paramInfos as $paramInfo)
        {
            if(isset($paramInfo->error))
            {
                array_push($resultParamInfos, $paramInfo);
            }
            else if(isset($paramInfo->executionRights) && $this->executionAllowed($paramInfo->executionRights, $userRoles))
            {
                unset($paramInfo->executionRights);
                array_push($resultParamInfos, $paramInfo);
            }
        }
        return $resultParamInfos;
    }

    /**
     * Return most significant ("higherst") access right from several single rights.
     * 
     * @param string access right 1
     * @param string access right 2
     * @return string most significant access right
     */
    private function getMostSignificantAccessRight($accessRight1, $accessRight2)
    {
        $resultAccessRight = '';

        if(($accessRight1 === 'readwrite') || ($accessRight2 === 'readwrite'))
        {
            $resultAccessRight = 'readwrite';
        }
        else if((($accessRight1 === 'read') || ($accessRight2 === 'read')))
        {
            $resultAccessRight = 'read';
        }
        return $resultAccessRight;
    }

    /**
     * Return only access rights for specified user roles. Only return the most significant ("higherst") access right.
     * 
     * @param object parameter info
     * 
     */
    public function convertAccessRights($paramAccessRights, $userRoles)
    {
        $userAccessRight = '';

        foreach($userRoles as $userRole)
        {
            if(isset($paramAccessRights->{$userRole}))
            {
                $userAccessRight = $this->accessRights->getMostSignificantAccessRight($userAccessRight, $paramAccessRights->{$userRole});
            }
        }
        return $userAccessRight;
    }

    /**
     * Filter parameter infos by parameter id. 
     * ID is fitting, if it is exactly the same, or it begins with the given string + '.'
     * 
     * @param string parameter id
     * @return array parameter info object fitting to parameter id
     */
    public function filterParameterInfosById($parameterInfos, $parameterId)
    {
        $resultInfos = [];
        foreach($parameterInfos as $parameterInfo)
        {
            if(($parameterInfo->id === $parameterId) || (strpos($parameterInfo->id, $parameterId.'.') === 0))
            {
                array_push($resultInfos, $parameterInfo);
            }
        }
        return $resultInfos;
    }

    
}