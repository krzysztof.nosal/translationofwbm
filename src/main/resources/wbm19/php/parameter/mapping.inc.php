<?php

include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/../plugins/pluginloader.inc.php';
include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';


class ParameterMapping
{
    private $platformString         = 'pfcXXX';
    private $baseParamMappingPath;
    private $filesystemUtils;
    private $pluginloader;
    private $errorLogger;


    /**
     * Constructor
     * 
     * @param directory where parameter mapping files are placed, 
     *        if not given (= false), use default 'parameter/mapping'
     * @param object filesystem utils
     *        if not given (false), instance an own one
     * @param object pluginloader
     *        if not given (false), instance an own one
     */
    public function __construct($baseParamMappingPath = false, $filesystemUtils = false, $pluginloader = false, $errorLogger = false)
    {
        $this->baseParamMappingPath = $baseParamMappingPath ? $baseParamMappingPath : 'parameter/mappings';
        $this->filesystemUtils = $filesystemUtils ? $filesystemUtils : new FilesystemUtils();
        $this->pluginloader = $pluginloader ? $pluginloader : new Pluginloader();
        $this->errorLogger = $errorLogger ? $errorLogger : new ErrorLogger();
    }


    /**
     * Change private data which was rather created in constructor.
     * Intended for unit tests with mock environment via phpunit.
     */
    public function attach($baseParamMappingPath = false, $filesystemUtils = false, $pluginloader = false)
    {
        $this->__construct($baseParamMappingPath, $filesystemUtils, $pluginloader);
    }


    /**
     * Get (relative) directory where parameter mapping files are placed
     * 
     * @return string
     */
    public function getBaseParamMappingPath()
    {
        return $this->baseParamMappingPath;
    }


    /**
     * Get absolute path of (basic) parameter info files in filesystem.
     * 
     * @return string
     */
    public function getBaseParamMappingPathAbsolute()
    {
        $path = $this->filesystemUtils->getWbmPathAbsolute().'/'.$this->baseParamMappingPath;
        //echo "\npath: ".$path;
        return $path;
    }

    /**
     * Get directorys (with absolute path) including parameter mapping files
     * Direct access to filesytem
     * 
     * @return array directorys
     */
    private function getParamMappingDirectorys()
    {
        // get basic param mapping directory
        $paramMappingDirectorys = array($this->getBaseParamMappingPathAbsolute());

        // get (paths of) all plugins
        $pluginPlatforms = $this->pluginloader->getPluginsPlatformPathArray();
        foreach($pluginPlatforms as $pluginPlatformPath)
        {
            // add specified name of parameter info directory to plugin path
            array_push($paramMappingDirectorys, $pluginPlatformPath.'/'.$this->getBaseParamMappingPath());
        }
        //echo "\nparamMappingDirectorys: "; var_dump($paramMappingDirectorys);
        return $paramMappingDirectorys;
    }


    /**
     * Get all parameter mapping files (basic and plugins)
     * Direct access to filesytem
     * 
     * @return array parameter mapping files (with path)
     */
    private function getParamMappingFilepaths()
    {
        $paramMappingFilepaths = array();

        foreach($this->getParamMappingDirectorys() as $paramMappingDirectory)
        {
            $paramMappingFilepaths = array_merge($paramMappingFilepaths, $this->filesystemUtils->getFilepathsOfDirectory($paramMappingDirectory));
        }
        //echo "\nparamMappingFilepaths: "; var_dump($paramMappingFilepaths);
        return $paramMappingFilepaths;
    }


    /**
     * Convert content of parameter mapping files to object, handle format errors.
     * 
     * @param array information/content about all parameter mapping files
     *              or false, if content should be read from filesystem first
     * @return array object converted parameter mapping
     */
    public function convertParamMappingContents($paramMappingContents = false)
    {
        // if parameter mappings are not given (in unit test), get them from filesystem
        if(!$paramMappingContents)
        {
            //$paramMappingContents = $this->getParamMappingFileContents();
            $filepaths = $this->getParamMappingFilepaths();
            $paramMappingContents = $this->filesystemUtils->getFileContents($filepaths);
        }


        $paramMappingArray = [];
        foreach($paramMappingContents as $paramMappingContent)
        {
            if(isset($paramMappingContent->error))
            {
                $error = new WBMError(ERROR_GROUP_PARAM_MAPPING, ERROR_CODE_READ_PARAM_MAPPING, "Can't read parameter mapping");
                // transfer error object to the same strukture as parameter infos for equal treatment later
                $paramMappingArray = array_merge($paramMappingArray, array(0 => (object) array("error" => $error)));
            }
            else if(isset($paramMappingContent->content))
            {
                $paramMapping = json_decode($paramMappingContent->content);
                if(!$paramMapping)
                {
                    $error = new WBMError(ERROR_GROUP_PARAM_MAPPING, ERROR_CODE_PARAM_MAPPING_FORMAT, "Parameter mapping format error");
                    $this->errorLogger->logError($error);
                    $paramMappingArray = array_merge($paramMappingArray, array(0 => (object) array("error" => $error)));
                }
                else
                {
                    foreach ($paramMapping as $singleMapping) {
                        $singleMappingObject = $this->convertMappingToResponseObject($singleMapping, $paramMappingContent->filepath);
                        array_push($paramMappingArray, $singleMappingObject);
                    }
                }
            }
        }
        //echo "\nparamMappingArray: "; var_dump($paramMappingArray);
        return $paramMappingArray;
    }


    /**
     * Convert original mapping data to format for frontend:
     * - add filepath (without document root) to all filenames in custom fields
     * 
     * @param object original mapping data
     * @param string path of original mapping file
     * 
     * @return object converted mapping data
     */
    private function convertMappingToResponseObject($originalMapping, $mappingPath)
    {
        $mapping = clone($originalMapping);

        // get path to manifest for url - without absolute path string to our wbm document root and without plugin file name
        $customTransformRequestPath = str_replace($_SERVER["DOCUMENT_ROOT"], '', dirname($mappingPath));
        $customTransformRequestPath = str_replace('/'.basename($customTransformRequestPath), '', $customTransformRequestPath);

        if(isset($mapping->custom))
        {
            $pluginName = $this->getPluginNameViaMappingPath($mappingPath);
            $pluginData = $this->pluginloader->getPluginDataViaPluginName($pluginName);
            $mapping->custom = $customTransformRequestPath.'/transforms/'.$mapping->custom.'.js?v='.$pluginData->manifest->version;
        }
        //echo "\nmapping: "; var_dump($mapping);
        return $mapping;
    }

    /**
     * Filter name of plugin from mapping path
     * 
     * @param string mapping path e.g. "/var/www/wbm/plugins/wbm-aide/platform/pfcXXX/parameter/mappings/aide-mappings.json"
     * 
     * @return string name of plugin or empty string, if mapping belongs to base wbm (e.g. "wbm-aide")
     */
     public function getPluginNameViaMappingPath($mappingPath) 
     {
        // check if mapping file belongs to plugin or to wbm base and then return wbm base name
        if(!strpos($mappingPath, "plugins"))
        {
            return $this->pluginloader->getBasicWbmManifest()->name;
        }

        // remove name of transform file whith regex and other path elements
        $pluginName = $mappingPath;
        $pluginName = preg_replace("#\/[^\/]*\.json$#", "", $pluginName);
        $pluginName = str_replace("/platform/".$this->platformString."/parameter/mappings", "", $pluginName);
        $pluginName = str_replace($this->filesystemUtils->getWbmPathAbsolute()."/plugins/", "", $pluginName);

        return $pluginName;
    }
}

