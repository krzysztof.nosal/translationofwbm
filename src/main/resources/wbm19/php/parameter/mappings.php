<?php

include_once __DIR__.'/mapping.inc.php';
include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';
include_once __DIR__.'/../plugins/pluginloader.inc.php';
include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/../authentication/wbm_session.inc.php';



$paramMappingPath  = false;
$pluginPath = false;
$request = (object) [];
$paramMappingsResponse = (object) [];

// get request json string by stdin
$requestString  = file_get_contents("php://input");
//echo "\nrequestString:"; var_dump($requestString);

if($requestString)
{
  $request = json_decode($requestString, false);
}

if(($requestString === false) || ($request === NULL))
{
  $error = new WBMError(ERROR_GROUP_MISC, ERROR_CODE_INVALID_INPUT, "Wrong request data format"); 
  $paramMappingsResponse->error = $error;
}
else
{
  // get (optional) input parameter from request or use default values
  $paramMappingPath = isset($request->paramInfoPath) ? $request->paramInfoPath : $paramInfoPath;
  $pluginPath = isset($request->pluginPath) ? $request->pluginPath : $pluginPath;

  // initialize objects
  $errorLogger      = new ErrorLogger();
  $filesystemUtils  = new FilesystemUtils();
  $wbmSession       = new WbmSession($errorLogger, $filesystemUtils);
  $pluginloader     = new Pluginloader($pluginPath, $filesystemUtils, $errorLogger);
  $paramMapping     = new ParameterMapping($paramMappingPath, $filesystemUtils, $pluginloader, $errorLogger);

  // handle wmb session
  $checkWbmSessionResult = $wbmSession->handleWbmSession($request, $paramMappingsResponse);

  if(!$checkWbmSessionResult instanceof WBMError)
  {
    $paramMappingArray = $paramMapping->convertParamMappingContents();
    //echo "\nparamMappingArray: "; var_dump($paramMappingArray);

    $paramMappingsResponse->parameterMappings = $paramMappingArray;
  }
}

// convert response object to output format
$paramMappingsResponseString = json_encode($paramMappingsResponse);
echo $paramMappingsResponseString;



