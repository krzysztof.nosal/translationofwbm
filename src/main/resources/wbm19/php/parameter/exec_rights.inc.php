<?php

include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';

/**
 * Helper class for handling execution rights for methods
 */
class ExecRights
{
    private $errorLogger;

     /**
     * Constructor
     * 
     */
    public function __construct($errorLogger = false)
    {
        $this->errorLogger = $errorLogger ? $errorLogger : new ErrorLogger();
    }

    /**
     * Merge two arrays with execution rights for several users togeter.
     * 
     * @param array exec rights 1
     * @param array exec rights 2
     */
    public function mergeExecRights($execRights1, $execRights2)
    {
        return array_unique(array_merge($execRights1, $execRights2));
    }

}
