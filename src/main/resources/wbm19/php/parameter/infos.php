<?php

include_once __DIR__.'/infos.inc.php';
include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';
include_once __DIR__.'/../plugins/pluginloader.inc.php';
include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/../authentication/wbm_session.inc.php';
include_once __DIR__.'/../authentication/user.inc.php';



$paramInfoPath  = false;
$pluginPath = false;
$request = (object) [];
$paramInfoResponse = (object) [];

// get request json string by stdin
$requestString = file_get_contents("php://input");
//echo "\nrequestString:"; var_dump($requestString);

if($requestString)
{
  $request = json_decode($requestString, false);
}

if(($requestString === false) || ($request === NULL))
{
  $error = new WBMError(ERROR_GROUP_MISC, ERROR_CODE_INVALID_INPUT, "Wrong request data format"); 
  $paramInfoResponse->error = $error;
}
else
{
  // get (optional) input parameter from request or use default values
  $paramInfoPath  = isset($request->paramInfoPath) ? $request->paramInfoPath : $paramInfoPath;
  $pluginPath     = isset($request->pluginPath) ? $request->pluginPath : $pluginPath;

  // initialize objects
  $errorLogger      = new ErrorLogger();
  $filesystemUtils  = new FilesystemUtils();
  $user             = new User();
  $wbmSession       = new WbmSession($errorLogger, $filesystemUtils);
  $pluginloader     = new Pluginloader($pluginPath, $filesystemUtils, $errorLogger);
  $paramInfos       = new ParameterInfos($paramInfoPath, $filesystemUtils, $pluginloader, $errorLogger);

  // handle wmb session
  $checkWbmSessionResult = $wbmSession->handleWbmSession($request, $paramInfoResponse);

  if(!$checkWbmSessionResult instanceof WBMError)
  {
    $paramInfoArray = $paramInfos->convertParamInfoContents();
    //echo "\nparamInfoArray: "; var_dump($paramInfoArray);

    $sessionUsername = $wbmSession->getSessionUsername();
    //$sessionUsername = "admin";  // for tests
    $userroles = $user->getUserrolesForUser($sessionUsername);

    $paramInfoArray = $paramInfos->check($paramInfoArray);
    $paramInfoArray = $paramInfos->merge($paramInfoArray);

    $paramInfoResponse->parameterInfos = $paramInfos->filterForAccessRights($paramInfoArray, $userroles);
    $paramInfoResponse->methodInfos = $paramInfos->filterForExecutionRights($paramInfoArray, $userroles);
  }
}

// convert response object to output format
$paramInfosResponseString = json_encode($paramInfoResponse);
echo $paramInfosResponseString

?>