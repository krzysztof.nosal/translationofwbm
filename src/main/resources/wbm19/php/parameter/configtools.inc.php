<?php

include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';
include_once __DIR__.'/../utils/filesystem.inc.php';



class Configtools
{
    private $errorLogger;
    private $filesystemUtils;

    /**
     * Constructor
     * 
     * @param object filesystem utils
     *        if not given (false), instance an own one
     * @param object error logger
     *        if not given (false), instance an own one
     */
    public function __construct($errorLogger = false, $filesystemUtils = false)
    {
        $this->errorLogger = $errorLogger ? $errorLogger : new ErrorLogger();
        $this->filesystemUtils = $filesystemUtils ? $filesystemUtils : new FilesystemUtils();
    }


    /**
     * Check if name of configtool is valid: '/' is not allowed
     * 
     * @param string configtool name
     * @return boolean 
     */
    public function configtoolNameIsValid($configtoolName)
    {
      $nameIsValid = true;
    
      if(strpos($configtoolName, "/"))
      {
        $nameIsValid = false;
      }
    
      return $nameIsValid;
    }
    

    /**
     * Convert possible (html) encoded string from browser for use in configtools or parameter server.
     * Function is complement to encodeResultStringForBrowser.
     * 
     * @param string string from browser
     * @param string decoded string for configtool
     */
    public function decodeRequestStringFromBrowser($browserString)
    {
        return htmlspecialchars_decode($browserString, ENT_QUOTES);
    }


    /**
     * Convert configtool result string to right format for response to client.
     * 
     * In all there comes data in four different formats:
     *   - Text ("lorem ipsum")
     *   - XML ("<?xml ...> ... </ ...>")
     *   - JSON ("{ ... }")
     *   - JSON Array ("[ ... ]")
     *
     * In case of Text we replace all special characters with the
     * corresponding HTML entities, in case of XML we do nothing
     * as we expect to be valid and for JSON/JSON Array we do not
     * escape quotes.
     * Function is complement to decodeRequestStringFromBrowser.
     * 
     * @param string result string of configtool
     * @return string converted result string for response
     */
    public function encodeResultStringForBrowser($resultString)
    {
        $responseResultString = $resultString;
        if(Configtools::isJSON($resultString) || Configtools::isJSONArray($resultString))
        {
            // JSON [Array]
            $responseRsultString = htmlspecialchars($resultString, ENT_NOQUOTES);
        }
        else if(!Configtools::isXML($resultString))
        {
            // text
            $responseResultString = htmlspecialchars($resultString, ENT_QUOTES);
        }
        return $responseResultString;
    }


    /**
     * Evaluate if strings starts with a specified substring.
     * 
     * @param string entire string
     * @param string substring
     * @return boolean
     */
    private function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }
    
    /**
     * Evaluate if strings ends with a specified substring.
     * 
     * @param string entire string
     * @param string substring
     * @return boolean
     */
    private function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return $length === 0 ||
        (substr($haystack, -$length) === $needle);
    }
    
     
    /**
     * detect if text is in JSON format
     */
    private function isJSON($text)
    {
      // without parsing we cannot be sure it is JSON,
      // but it should be sufficient if text starts and
      // ends with curly brackets
      return Configtools::startsWith(trim($text), '{') && Configtools::endsWith(trim($text), '}');
    }
    
     
    /**
     * detect if text is a JSON array
     */
    private function isJSONArray($text)
    {
      // some Config-Tools return their data in JSON array
      // notation, then we also may not escape quotes. We
      // assume if text starts and ends with square brackets
      // it is a JSON array.
      return Configtools::startsWith(trim($text), '[') && Configtools::endsWith(trim($text), ']');
    }
    

    /**
     * detect if text is in XML format
     */
    private function isXML($text)
    {
      // if the text starts with '<?xml' and ends with '>'
      // we assume this is some data in XML format
      return Configtools::startsWith(trim($text), '<?xml') && Configtools::endsWith(trim($text), '>');
    }
    
    
};