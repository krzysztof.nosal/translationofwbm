<?php

class WBMError
{
    public $group;
    public $code;
    public $text;

    /**
     * Constructor
     * 
     * @param int error group
     * @param int error code
     * @param string error text 
     */
    public function __construct($group, $code, $text = '')
    {
        $this->group = $group;
        $this->code = $code;
        $this->text = $text;
    }
}

//---------------------------------------------------------------------------------
// error groups
//---------------------------------------------------------------------------------
define("ERROR_GROUP_MISC",              "1");
define("ERROR_GROUP_FILESYSTEM_UTILS",  "2");
define("ERROR_GROUP_ERROR_LOGGING",     "3");
define("ERROR_GROUP_PARAM_INFOS",       "4");
define("ERROR_GROUP_PARAM_MAPPING",     "5");
define("ERROR_GROUP_PLUGINS",           "6");
define("ERROR_GROUP_AUTHORIZATION",     "7");
define("ERROR_GROUP_CONFIGTOOL",        "8");
define("ERROR_GROUP_AUTHENTICATION",    "9");
define("ERROR_GROUP_FILE_TRANSFER",     "10");


//---------------------------------------------------------------------------------
// error codes
//---------------------------------------------------------------------------------

// miscancellous or often used errors, dont't redefine them in error groups
define("ERROR_CODE_MISC",                           "1");
define("ERROR_CODE_INVALID_INPUT",                  "2");

// ERROR_GROUP_FILESYSTEM_UTILS
define("ERROR_CODE_CREATE_DIRECTORY",               "100");
define("ERROR_CODE_READ_FILE",                      "102");
define("ERROR_CODE_WRITE_LOG",                      "103");
define("ERROR_CODE_DELETE_FILE",                    "104");
define("ERROR_CODE_DELETE_DIRECTORY",               "105");

// ERROR_GROUP_ERROR_LOGGING
define("ERROR_CODE_WRITE_ERROR_LOG",                "101");

// ERROR_GROUP_PARAM_INFOS
define("ERROR_CODE_READ_PARAM_INFO_FILE",           "101");
define("ERROR_CODE_READ_PARAM_INFO",                "102");
define("ERROR_CODE_PARAM_INFO_FORMAT",              "103");
define("ERROR_CODE_PARAM_INFO_INCOMPLETE",          "104");
define("ERROR_CODE_PARAM_INFO_ID_DOUBLE",           "105");
define("ERROR_CODE_PARAM_INFO_ID_INVALID",          "106");

// ERROR_GROUP_PARAM_MAPPING
define("ERROR_CODE_READ_PARAM_MAPPING_FILE",        "101");
define("ERROR_CODE_READ_PARAM_MAPPING",             "102");
define("ERROR_CODE_PARAM_MAPPING_FORMAT",           "103");

// ERROR_GROUP_PLUGINS
define("ERROR_CODE_READ_PLUGIN_MANIFEST_FILE",      "101");
define("ERROR_CODE_PLUGIN_MANIFEST_FORMAT",         "102");

// ERROR_GROUP_AUTHORISATION
define("ERROR_CODE_AUTH_CONFIGTOOL_MISSING",        "101");
define("ERROR_CODE_AUTH_PARAM_MISSING",             "102");
define("ERROR_CODE_AUTH_DATA_MISSING",              "103");
define("ERROR_CODE_AUTH_DATA_INIT",                 "104");

// ERROR_GROUP_CONFIGTOOL
define("ERROR_CODE_CONFIGTOOL_AUTH_DATA_MISSING",   "101");
define("ERROR_CODE_CONFIGTOOL_ACCESS_NOT_ALLOWED",  "102");
define("ERROR_CODE_CONFIGTOOL_REQUEST_FORMAT",      "103");

// ERROR_GROUP_AUTHENTICATION
define("ERROR_CODE_CSRF_TOKEN_INVALID",             "101");
define("ERROR_CODE_USER_AGENT_INVALID",             "102");
define("ERROR_CODE_USER_TIMEOUT_MISSING",           "103");
define("ERROR_CODE_WBM_SESSION_EXPIRED",            "104");
define("ERROR_CODE_WBM_SESSION_RENEW",              "105");

// ERROR_GROUP_FILE_TRANSFER
define("ERROR_CODE_NOT_ENOUGH_MEMORY",              "101");
define("ERROR_CODE_TRANSFER_NOT_ALLOWED",           "102");
define("ERROR_CODE_TRANFER_TOKEN_MISSING",          "103");
define("ERROR_CODE_TRANSFER_NOT_EXISTING",          "104");
define("ERROR_CODE_UPLOAD_NOT_EXISTING",            "105");
define("ERROR_CODE_UPLOAD_NAME_MISSING",            "106");
define("ERROR_CODE_UPLOAD_SIZE_MISSING",            "107");
define("ERROR_CODE_UPLOAD_INIT_FAILED",             "108");
define("ERROR_CODE_UPLOAD_COMPLETED",               "109");
define("ERROR_CODE_UPLOAD_FAILED",                  "110");
define("ERROR_CODE_DOWNLOAD_PATH_MISSING",          "111");
define("ERROR_CODE_DOWNLOAD_FILE_DOES_NOT_EXIST",   "112");
define("ERROR_CODE_DOWNLOAD_FILE_NOT_READABLE",     "113");