<?php

include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/error_definitions.inc.php';

class ErrorLogger
{
    private $logfileDir;
    private $logfilePath;
    private $oldLogfilePath;
    private $logfileMaxSizeByte;
    private $separator = '|';

    /**
     * Constructor
     * 
     * @param string directory of error log. If not given (= false), use default
     * @param string name of logfile. If not given (= false), use default
     * @param int maximum size of logfile in kbyte. If not given (= false), use default
     * @param object filesystem utilities, if not given (= false), create self
     */
    public function __construct($logfileDir = false, $logfileName = false, $logfileMaxSizeKByte = false, $filesystemUtils = false)
    {
        // take given filepath and name, or create it itself by default
        $this->logfileDir = $logfileDir ? $logfileDir : '/tmp/wbm';
        $this->logfilePath = $logfileName ? $this->logfileDir.'/'.$logfileName : $this->logfileDir.'/errorlog.txt';
        $this->oldLogfilePath = $this->logfilePath.'.old';

        // take filesystemUtil object (e.g. for test), or instance it directly
        $this->filesystemUtils = $filesystemUtils ? $filesystemUtils : new FilesystemUtils();

        // for max size of logfile take default - or configured value if it is valid
        $this->logfileMaxSizeByte = 20000;
        if($logfileMaxSizeKByte && is_int($logfileMaxSizeKByte) && ($logfileMaxSizeKByte > 0))
        {
            $this->logfileMaxSizeByte = $logfileMaxSizeKByte * 1000;
        }

        // set default timezone to avoid warning messages at date() call
        // we only use UTC, because the timezone strings we can get from system have not the format requested format for php
        // workaround (for later, if it is important): use clock configtool to get system time and date
        date_default_timezone_set('UTC');
    }


    /**
     * Get path and filename of error log
     * 
     * @return string
     */
    public function getLogFilePath()
    {
        return $this->logfilePath;
    }

    /**
     * Get max size of logfile in KByte
     * 
     * @return string
     */
    public function getLogFileMaxSizeKByte()
    {
        return floor($this->logfileMaxSizeByte / 1000);
    }


    /**
     * Create error message
     * 
     * @param object error object (group, code, text)
     * @param string timestring - only given while unit test!!!
     * 
     * @return error error object which should be logged
     */
    private function createErrorMessage($error, $timeString = false)
    {
        $errorMessage = '';
        if($error && ($error instanceof WBMError))
        {
            $timeString = $timeString ? $timeString : date('Y-m-d H:i:s');
            $errorMessage = sprintf("%s%s%4d%s%4d%s%s\n", $timeString, $this->separator, $error->group, $this->separator, $error->code, $this->separator, $error->text);
        }
        return $errorMessage;
    }
    
    
    /**
     * Log error
     * 
     * @param error error object (group, code, text)
     * @param string timestring - only given while unit test!!!
     */
    public function logError($error, $timeString = false)
    {
        $resultError = new WBMError(ERROR_GROUP_ERROR_LOGGING, ERROR_CODE_WRITE_ERROR_LOG, "Can't write wbm error logging file");

        if($error)
        {
            $filesystemError = $this->filesystemUtils->ensureDirectoryExistence($this->logfileDir);
            if(!$filesystemError)
            {
                $logMessage = $this->createErrorMessage($error, $timeString);
                $this->filesystemUtils->addLogfileEntry($logMessage, $this->logfilePath, $this->oldLogfilePath, $this->logfileMaxSizeByte);
                $error = false;
            }
        }
        return $error;
    }

    
}
