<?php

include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';
include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/../utils/http.inc.php';

// Handle cookie secure flag for session cookie based
// upon used request protocol. We only set this option
// in case we are requested via HTTPS, if HTTP is used,
// some older clients might still use, we allow cookies
// to be send also via HTTP not to break backward
// compatibility.
if ( !empty( $_SERVER['REQUEST_SCHEME'] ) && $_SERVER['REQUEST_SCHEME'] === 'https' )
{
  ini_set('session.name', 'NG_WBM_SESSION_SEC');
  ini_set('session.cookie_secure', '1');
}

class WbmSession
{
    private $errorLogger;
    private $filesystemUtils;
    private $sessionLifetimeInSec = 900;

    /**
     * Constructor
     * 
     */
    public function __construct($errorLogger = false, $filesystemUtils = false)
    {
        $this->errorLogger = $errorLogger ? $errorLogger : new ErrorLogger();
        $this->filesystemUtils = $filesystemUtils ? $filesystemUtils : new FilesystemUtils();
    }


    /**
     * Start session
     */
    public function init()
    {
        @session_start();
    }


    /**
     * Get (authenticated) username from actual session
     */
    public function getSessionUsername()
    {
        return $_SESSION['username'];
    }


    /**
     * Get actual CSRF token
     */
    public function getCsrfToken()
    {
        return $_SESSION["csrf_token"];
    }


    /**
     * Filter authentication data from several destinations - request, header or query string.
     * TODO add evaluation of query string
     * 
     * @param object request
     * @return object authentication data
     * 
     */
    private function getAuthenticationData($request)
    {
        $authData = (object) [
            'csrfToken' => false,
            'renewSession' => false
        ];

        // try to get authentication parameter from request header
        $authData->csrfToken = HttpUtils::getRequestHeaderParam('Com-Wago-Session-Token');
        if($authData->csrfToken !== null)
        {
            $authData->renewSession = HttpUtils::getRequestHeaderParam('Com-Wago-Renew-Session');
        }

        // not found - try to get them from request body
        else if(isset($request))
        {
            $authData->csrfToken = isset($request->csrfToken) ? htmlspecialchars($request->csrfToken, ENT_QUOTES) : false;
            $authData->renewSession = isset($request->renewSession) ? $request->renewSession : false;
        }

        // check and return parameter
        $authData->renewSession = is_bool($authData->renewSession) ? $authData->renewSession : false;
        return $authData;
    }


    /**
     * Hanling of WBM session for every request from frontend.
     * Read relevant data from request data, check it and add relevant data to response.
     * No change of other values in response object.
     * 
     * NOTE renew of session not implemented yet, not needed for parameter infos and mappings
     * 
     * @param object request object to get the session relevant data from:
     *               crfsToken: string token delivered from frontend, must be the same as in session
     *               renewSession: boolean, true, if session should be renewed
     *               false, if no request object is given and data must be read from request header
     * @param object response object to write the session relevant data for frontend 
     * @return boolean true
     *         object error, if an error occured
     */
    public function handleWbmSession($request, $response)
    {
        $result = true;

        $this->init();

        $authParams = $this->getAuthenticationData($request);

        $checkWbmSessionResult = $this->checkWbmSession($authParams->csrfToken);
        if($checkWbmSessionResult instanceof WBMError)
        {
           $response->error = $checkWbmSessionResult;
           $result = $checkWbmSessionResult;
        }
        else
        {
            // reset timeout in session, if requested
            if($authParams->renewSession)
            {
                $this->renewSession();
            }

            // write actual csrf token to response object
            $response->csrfToken = $this->getCsrfToken();
        }

        return $result;
    }


    /**
     * Check session data via given csrf token (delivered in frontend request).
     * If no csrf token is given, access is ok, but only for user "guest".
     * Otherwise check csrf token, user agent and session timeout.
     * Reset session username, if session is not ok.
     * 
     * @param string CSRF token from frontend
     * @return boolean true if session is ok
     *         error if session is not ok
     */
    public function checkWbmSession($csrfToken = false)
    {
        $result = true;
        
        // if no csrf token is given, only guest access is allowed
        if(!$csrfToken)
        {
            $_SESSION['username'] = "guest";
            $_SESSION['csrf_token'] = false;
            $result = true;
        }
        // if csrf token is given, session data must be correct
        else if(!isset($_SESSION["csrf_token"]) || ($_SESSION["csrf_token"] != $csrfToken))
        {
            $result = new WBMError(ERROR_GROUP_AUTHENTICATION, ERROR_CODE_CSRF_TOKEN_INVALID, "CSRF token invalid");
        }
        else if($_SESSION['userAgent'] != $_SERVER['HTTP_USER_AGENT'])
        {
            $result = new WBMError(ERROR_GROUP_AUTHENTICATION, ERROR_CODE_USER_AGENT_INVALID, "User agent invalid");
        }
        else if(!isset($_SESSION['timeout']))
        {
            $result = new WBMError(ERROR_GROUP_AUTHENTICATION, ERROR_CODE_USER_TIMEOUT_MISSING, "Timeout missing");
        }
        else if($this->sessionExpired())
        {
            $result = new WBMError(ERROR_GROUP_AUTHENTICATION, ERROR_CODE_WBM_SESSION_EXPIRED, "Session expired");
        }

        if($result instanceof WBMError)
        {
            $_SESSION['username'] = "";
        }

        return $result;
    }


    /**
     * Renew session - reset timeout value.
     * 
     * @return boolean true, if session is new 
     *         error, if an error occured
     */
    public function renewSession()
    {
        $result = true;
        $uptimeOutputArray = $this->filesystemUtils->getFileContents(["/proc/uptime"]);
        if($uptimeOutputArray[0] instanceof WBMError)
        {
            $result = new WBMError(ERROR_GROUP_AUTHENTICATION, ERROR_CODE_WBM_SESSION_RENEW, "Can't get uptime value");
            $errorLogger->logError($result);
        }
        else 
        {
            $uptimeOutput = $uptimeOutputArray[0]->content;
            $uptime = explode(' ', $uptimeOutput)[0];
            $_SESSION['timeout'] = $uptime + $this->sessionLifetimeInSec;
        }
        return $result;
    }


    /**
     * Figure out, if session expired since last frontend access.
     * 
     * @return boolean true if session expired or an error occured
     *                 false if session is still in time
     */
    private function sessionExpired()
    {
        $sessionExpired = false;

        $uptimeOutputArray = $this->filesystemUtils->getFileContents(["/proc/uptime"]);
         
        if($uptimeOutputArray[0] instanceof WBMError)
        {
            $sessionExpired = true;
        }
        else 
        {
            $uptimeOutput = $uptimeOutputArray[0]->content;
            if(explode(' ', $uptimeOutput)[0] > $_SESSION['timeout'])
            {
                $sessionExpired = true;
            }
        }

        return $sessionExpired;
    }

    /**
     * Destroy the current session
     */
    public function destroySession() {
        session_unset();
        if (ini_get("session.use_cookies")) // delete cookie
        {
            $params = session_get_cookie_params();
            setcookie(  session_name(), '', time() - 42000,
                $params["path"],
                $params["domain"]
                //$params["secure"],
                //$params["httponly"]
                );
        }
        session_destroy();
    }
}