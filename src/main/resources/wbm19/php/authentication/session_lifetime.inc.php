<?php

//------------------------------------------------------------------------------
// constants and global variables
//------------------------------------------------------------------------------
define("SUCCESS", 0);
define("ERROR", -1);
define("ACCESS_NOT_ALLOWED", -2);
define("SESSION_EXPIRED", -3);
define("CSRF_TOKEN_INVALID", -4);
define("SESSION_LIFETIME", 900); //in seconds
define("USER_USER", "user");
define("USER_ADMIN", "admin");
const CSRF_TOKEN_LENGTH = 48;

// Handle cookie secure flag for session cookie based
// upon used request protocol. We only set this option
// in case we are requested via HTTPS, if HTTP is used,
// some older clients might still use, we allow cookies
// to be send also via HTTP not to break backward
// compatibility.
if ( !empty( $_SERVER['REQUEST_SCHEME'] ) && $_SERVER['REQUEST_SCHEME'] === 'https' )
{
  ini_set('session.name', 'NG_WBM_SESSION_SEC');
  ini_set('session.cookie_secure', '1');
}

/**
 * Handle Session Lifetime
 * 
 * DEPRECATED use wbm_session.inc.php
 */
function Session_HandleSessionLifeTime($csrfToken, $shouldResetSessionTimeout)
{
  $status     = SUCCESS;
  @session_start();

  // if session is existing and user agent of the actual request is the same as stored in session at the time of login
  if(Session_CheckClientInformation())
  {
    // if given csrf token is the same as stored in session    
    if(Session_CheckCSRFToken($csrfToken))
    {
      // check if session has expired and if not, renew session
      $status = Session_CheckSessionLifeTime($shouldResetSessionTimeout);
    }
    else // invalid csrfToken
    {
      $status = CSRF_TOKEN_INVALID;
    }
  }
  else // no active session for client
  {
    $status = ACCESS_NOT_ALLOWED;
  }

  return $status;
}

/**
 * Start new session, store username, store user agend, calculate timeout point, generate csrf token
 * 
 */
function StartLoginSession($username)
{
  session_start();
  session_unset();
  session_regenerate_id(true);
  $_SESSION['username']  = $username;

  // /proc/uptime - time since last system restart
  $uptimeOutput = exec('cat /proc/uptime');
  $uptime = explode(' ', $uptimeOutput)[0];

  // calculate timeout point in time
  $_SESSION['timeout'] = $uptime + SESSION_LIFETIME;
  $_SESSION["csrf_token"] = Session_GenerateCSRFToken();
  $_SESSION['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
}

function Session_DestroySession()
{
  session_unset();
  if (ini_get("session.use_cookies")) // delete cookie
  {
    $params = session_get_cookie_params();
    setcookie(  session_name(), '', time() - 42000,
          $params["path"],
          $params["domain"]
          //$params["secure"],
          //$params["httponly"]
         );
  }
  session_destroy();
  unset($_SESSION['username']);
  unset($_SESSION['userlevel']);
  unset($_SESSION['timeout']);
  unset($_SESSION['csrf_token']);
  unset($_SESSION['userAgent']);
}


function Session_GenerateCSRFToken()
{
    $csrfTokenChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    $maxRand = strlen($csrfTokenChars) - 1;
    $csrfToken='';

    for ($i = 0; $i < CSRF_TOKEN_LENGTH; $i++)
    {
        $csrfToken .= $csrfTokenChars{intval(mt_rand(0.0, $maxRand))};
    }

    return $csrfToken;
}


/**
 * Check timeout value in session is existing and user agent of the actual request is the same as stored in session at the time of login.
 * 
 * DEPRECATED use wbm_session.inc.php
 */
function Session_CheckClientInformation()
{
  $isAccessAllowed = false;
  if(isset($_SESSION['timeout']) && Session_CheckUserAgent())
    $isAccessAllowed = true;

  return $isAccessAllowed;
}


/**
 * Generate error text by error status.
 * 
 * DEPRECATED use wbm_session.inc.php
 */
function Session_GetErrorTxt($cmdStatus)
{
  $errorText = "";
  switch($cmdStatus)
  {
    case ACCESS_NOT_ALLOWED:  // no session
      $errorTxt = "Access not allowed.";
      break;

    case SESSION_EXPIRED:    // session active and valid csrf token but session expired
      $errorTxt = "Session Expired";
      break;

    case CSRF_TOKEN_INVALID:  // session active but invalid csrf token
      $errorTxt = "CSRF token is invalid";
      break;

    default:
      $errorTxt = "";
      break;
  }
  return $errorTxt;
}

/**
 * Check if specified user is still using its default password.
 */
function isDefaultSessionUserPW($username)
{
  $isDefaultPW = "";
  if(!empty(session_id()))
  {
    if( "" != $_SESSION['username'])
    {
      if($_SESSION['username'] == $username)
      {
        $execString = "sudo /etc/config-tools/get_user_info --is-default-pwd ".$username;
        $isDefaultPW = exec($execString);
      }
    }
  }
  return $isDefaultPW;
}

