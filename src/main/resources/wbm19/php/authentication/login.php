<?php

include("session_lifetime.inc.php");
define("MISSING_PARAMETER", 1);
define("AUTH_FAILURE", 20);
define("MAX_PASSWORD_LENGTH", 100);

define("PASSWORD_FILENAME", "/etc/lighttpd/lighttpd-htpasswd.user");
#define("PASSWORD_FILENAME", "/etc/shadow");

function PasswordCorrect($passwordFilename, $username = '', $password = '')
{
  $pwCorrect  = false;
  //var_dump($username); var_dump($password);

  // used hash function will need some time to calculate the hash for
  // huge passwords which can be abused to DoS WBM and other PHP
  // applications. It is very unlikely that a user has a password
  // longer than 100 character.
  if(strlen($password) > MAX_PASSWORD_LENGTH)
  {
    return $pwCorrect;
  }

  // get password file and iterate over every line
  $pwFileArray = file($passwordFilename);

  foreach($pwFileArray as $lineNo => $pwFileLine)
  {
    //var_dump($pwFileLine);
    // extract username and user password
    $passwordFileData = explode(':', trim($pwFileLine));

    // if username was found in line, verify given password with user password
    if(isset($passwordFileData[0]) && ($passwordFileData[0] === $username))
    {
      $pwCorrect = password_verify($password, $passwordFileData[1]);
      break;
    }
  }

  return $pwCorrect;
}


function Login()
{
  $status     = ERROR;
  $errorText  = "";
  $csrfId   = "";
  $username = "";
  $isDefaultPW = "";
  // get request json string by stdin
  $requestString  = file_get_contents("php://input");
  //echo "requestString:".$requestString;

  if($requestString)
  {
    // decode string to php array/object
    $request  = json_decode($requestString, true);
    //var_dump($request);

    if(isset($request["username"]))
      $username = htmlspecialchars($request["username"], ENT_QUOTES);
 
    if(!isset($username) || !isset($request["password"]))
    {
      $status     = MISSING_PARAMETER;
      $errorText  = "Invalid input";
    }

    else if(!PasswordCorrect(PASSWORD_FILENAME, $username, $request["password"]))
    {
      $status    = AUTH_FAILURE;
      //$errorText = "Wrong username or password";
    }
    else
    {
      StartLoginSession($username);
      $isDefaultPW = isDefaultSessionUserPW($username);
      $csrfId = $_SESSION["csrf_token"];
      $status                = SUCCESS;
    }
  }
  
  // encode responses to json string and send it to stdout
  $resultObj = array('status'    	 => $status, 
                     'csrfToken' 	 => $csrfId, 
                     'username'  	 => $username,
                     'isDefaultPW' => $isDefaultPW,
                     'errorText' 	 => $errorText);
  
  echo json_encode($resultObj);
}

Login();
  
?>
