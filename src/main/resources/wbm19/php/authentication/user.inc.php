<?php

class User
{
    /**
     * Get all roles of one user.
     * Actually userrules are weighted, "admin" has all rules, "user" has user and guest.
     */
    public function getUserrolesForUser($username)
    {
        $userroles = array();

        switch($username)
        {
            case "admin":   array_push($userroles, "admin");
            case "user":    array_push($userroles, "user");
            case "guest":   
            default:        array_push($userroles, "guest");
        }
        return $userroles;
    }
};
