<?php

include("wbm_session.inc.php");

$requestString  = file_get_contents("php://input");
if($requestString)
{
  // decode string to php array/object
  $request = json_decode($requestString, true);

  $session = new WBMSession();
  $session->init();
  
  $result = (object) [];
  $sessionStatus = $session->handleWbmSession($request, $result);
  if( SUCCESS == $sessionStatus || SESSION_EXPIRED == $sessionStatus)
  {
    $session->destroySession();
  }
  echo json_encode($result);
} else {
  http_response_code(400);
}

?>