<?php

include_once __DIR__.'../../error_handling/error_definitions.inc.php';

class HttpUtils
{
    /**
     * Get parameter value from http request header. Convert name from frontend notation to header notation.
     * 
     * @param string name in frontend order (eg. 'Com-Wago-Session-Token')
     * @return string value of parameter
     *         null, if parameter is not given in header
     */
    public function getRequestHeaderParam($name)
    {
        $value = null;
        $name = strtoupper($name);
        $name = str_replace('-', '_', $name);
        $name = "HTTP_$name";
        $value = isset($_SERVER[$name]) ? urldecode($_SERVER[$name]) : null;
        if($value === 'true')
        {
            $value = true;
        }
        if($value === 'false')
        {
            $value = false;
        }
        return $value;
    }

}

