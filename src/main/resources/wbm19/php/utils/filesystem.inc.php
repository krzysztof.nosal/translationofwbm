<?php

include_once __DIR__.'../../error_handling/error_definitions.inc.php';

class FilesystemUtils
{
    public function getWbmBaseDir()
    {
        // find 'phpsource' on testsystem...
        if(false === ($position = strpos($_SERVER["SCRIPT_NAME"], 'phpsource')))
        {
            // ... or first '/php/' on target
            $position = strpos($_SERVER["SCRIPT_NAME"], '/php/');
        }

        $baseDir = substr($_SERVER["SCRIPT_NAME"], 0, $position);
        //echo "\nbasedir: ".$baseDir.".";
        return $baseDir;
    }

    public function getWbmPathAbsolute()
    {
        $absolutePath = $_SERVER["DOCUMENT_ROOT"].FilesystemUtils::getWbmBaseDir();
        //echo "\nabsolutePath: ".$absolutePath;
        return $absolutePath;
    }

    /**
     * Get all filenames contained in a specified directory
     * Direct access to file system
     * 
     * @param string absolute path of directory
     * @return array filenames of contained files
     *               empty array, if directory is not existing
     * 
     */
    public function getDirectoryContentArray($pathAbsolute)
    {
        $contentArray = array();
        if(file_exists($pathAbsolute))
        {
            // get all directories but "." and ".."
            $contentArray = array_diff(scandir($pathAbsolute), array('..', '.'));
            // let array index start at 0 (would start at 2 because of .. and .)
            $contentArray = array_values($contentArray);
        }
        return $contentArray;
    }   
   

    /**
     * Get all filenames with path contained in a specified directory
     * Direct access to file system
     * 
     * @param string absolute path of directory
     * @return array filenames of contained files wiht path
     * 
     * TODO Test
     */
    public function getFilepathsOfDirectory($dirPathAbsolute)
    {
        $filepaths = array();

        $contentArray = $this->getDirectoryContentArray($dirPathAbsolute);
        foreach($contentArray as $filename)
        {
            array_push($filepaths, $dirPathAbsolute.'/'.$filename);
        }
        return $filepaths;
    }   
 

    /**
     * Ensure existence of directory - create it, if it is not existing.
     * 
     * Unit test only executable with file system access.
     * 
     * @param string directory path
     * @return error object if error occured, otherwise false
     */
    public function ensureDirectoryExistence($directory)
    {
        $error = false;
        if(!file_exists($directory))
        {
            if(!mkdir($directory, 0777, true))
            {
                $error = new WBMError(ERROR_GROUP_FILESYSTEM_UTILS, ERROR_CODE_CREATE_DIRECTORY, "");
            }
        }
        return $error;
    }

    
    /**
     * Create directory.
     * 
     * No unit test.
     * 
     * @param string directory path
     * @return error object if error occured, otherwise false
     */
    public function createDirectory($directory)
    {
        $error = false;
        if(!mkdir($directory, 0777, true))
        {
            $error = new WBMError(ERROR_GROUP_FILESYSTEM_UTILS, ERROR_CODE_CREATE_DIRECTORY, "");
        }
        return $error;
    }


    /**
     * Recursive remove directory and its content, or single file. No error return, if specified file does not exist.
     * 
     * @param string path of file or directory
     * @return error object if error occured, otherwise false
     */
    public function removePath($path)
    {
        $error = false;
        if(!file_exists($path))
        {
            return $error;
        }

        if(is_file($path))
        {
            if(!unlink($path))
            {
                $error = new WBMError(ERROR_GROUP_FILESYSTEM_UTILS, ERROR_CODE_DELETE_FILE, "");
            }
        }
        else
        {
            // recursively remove subdirectories and files
            $objects = scandir($path);
            foreach($objects as $object) 
            { 
                if(($object != ".") && ($object != ".."))
                {
                    FilesystemUtils::removePath("$path/$object");
                }
            }
            reset($objects);
    
            // lastly try to remove the given path itself
            if(!rmdir($path))
            {
                $error = new WBMError(ERROR_GROUP_FILESYSTEM_UTILS, ERROR_CODE_DELETE_PATH, "");
            }
        } 
        return $error;
    }
    

    /**
     * Add an entry to logfile
     * If logfile is bigger than limit, copy old entries to backup file and create new, empty file
     * Directory must exist (use function ensureDirectoryExistence)
     * No detail check for errors, because we could not log them at all...
     * 
     * Unit test only executable with file system access.
     * 
     * @param string log message
     * @param string path of logfile
     * @param string path of old logfile (copy if logfile gets to big)
     * @param int maximum size of logfile in byte (minimum 100 byte)
     * @return error object if error occured, otherwise false
     * 
     */
    public function addLogfileEntry($logMessage, $logfilePath, $oldLogfilePath, $logfileMaxSizeByte)
    {
        // check input paramter
        if(!strlen($logMessage) || !strlen($logfilePath) || !strlen($oldLogfilePath) || ($logfilePath === $oldLogfilePath) || ($logfileMaxSizeByte < 100)) 
        {
            return new WBMError(ERROR_GROUP_FILESYSTEM_UTILS, ERROR_CODE_INVALID_INPUT);
        }

        $error = new WBMError(ERROR_GROUP_FILESYSTEM_UTILS, ERROR_CODE_WRITE_LOG, "Can't write wbm error logging file");

        // open and lock logfile
        $fileHandle = fopen($logfilePath, 'a+');
        if($fileHandle)
        {
            if(flock($fileHandle, LOCK_EX))
            {
                // check if file is too big
                $filesize = filesize($logfilePath) + strlen($logMessage);
                if($filesize > $logfileMaxSizeByte)
                {
                    // copy existing log entries to extra file and remove all content in original file
                    copy($logfilePath, $oldLogfilePath);
                    ftruncate($fileHandle, 0);
                }
                fwrite($fileHandle, $logMessage);
                fflush($fileHandle);
                flock($fileHandle, LOCK_UN);
                $error = false;
            }
            fclose($fileHandle);
            clearstatcache();
        }
        return $error;
    }

    /**
     * Provide contents of files.
     * Direct access to filesytem
     * 
     * @param array string path of requested files
     * @return array string contents of files or false, if file could not be read
     */
    public function getFileContents($filepaths)
    {
        $fileContentArray = array();
        foreach($filepaths as $filepath)
        {
            //echo "\nfilepath: ".$filepath;
            $fileContent = file_get_contents($filepath);
            if($fileContent)
            {
                array_push($fileContentArray, (object) [ "filepath" => $filepath, "content" => $fileContent ]);
            }
            else
            {
                $error = new WBMError(ERROR_GROUP_FILESYSTEM_UTILS, ERROR_CODE_READ_FILE, "Can't read file ".$filepath);
                array_push($fileContentArray, (object) [ "filepath" => $filepath, "error" => $error ]);
            }
        }
        //echo "\nfileContentArray: "; var_dump($fileContentArray);
        return $fileContentArray;
    }

    
    /**
     * Get size of all files in a specified directory recursivly including sub directories.
     * If path refers to a single file - return size of file.
     * 
     * @param string absolute path of directory
     * @return int size of whole path (or single file) in byte
     */
    public function sizeOfPath($path) 
    {
        $totalSize = 0;

        // if path is not existing - return 0
        if(!file_exists($path))
        {
            return $totalSize;
        }

        // if path is file - return size of file
        if(is_file($path)) 
        {
            $totalSize = filesize($path);
        }
        // path is directory - get size of whole directory recursivly
        else
        {
            $objects = scandir($path);
            foreach($objects as $object) 
            { 
                if ($object != "." && $object != "..") 
                {
                    $totalSize += FilesystemUtils::sizeOfPath("$path/$object");
                }
            }
            reset($objects);
        } 
        return $totalSize;
    }

    
}