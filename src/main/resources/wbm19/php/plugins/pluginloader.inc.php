<?php

include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';

class Pluginloader
{
    private $pluginPath;
    private $platformString = 'pfcXXX';
    private $filesystemUtils;
    private $errorLogger;
    private $pluginDataArray;
    private $basicWbmManifest;

    public function __construct($pluginPath = false, $filesystemUtils = false, $errorLogger = false)
    {
        $this->pluginPath = $pluginPath ? $pluginPath : 'plugins';
        $this->filesystemUtils = $filesystemUtils ? $filesystemUtils : new FilesystemUtils();
        $this->errorLogger = $errorLogger ? $errorLogger : new ErrorLogger();
    }

    public function setPath($newPluginPath)
    {
        $this->pluginPath = $newPluginPath;
    }

    public function getPath()
    {
        return $this->pluginPath;
    }


    /**
     * get absolute path of plugin directory
     * 
     * @return string path
     */
    public function getPluginPathAbsolute()
    {
        $pluginPath = $this->filesystemUtils->GetWbmPathAbsolute().'/'.$this->pluginPath;
        //echo "\npluginPath: ".$pluginPath;
        return $pluginPath;
    }


    /**
     * Get all manifest filenames in plugin area which fit to our platform.
     * Note: actually we don't arrange the manifests in platform directories, so this function is absolete, but might be reactivated in future
     * 
     * @return array string absolute paths of plugin manifests
     */
    public function getPluginsPlatformPathArray()
    {
        $pluginDirectoryArray = $this->filesystemUtils->getDirectoryContentArray($this->getPluginPathAbsolute());
        $pluginsPlatformPathArray = [];
        foreach($pluginDirectoryArray as $pluginDirectory) 
        {
            //echo "\npluginDirectory: ".$pluginDirectory;    
            $pluginPath = $this->getPluginPathAbsolute()."/".$pluginDirectory;
            $pluginPath = $pluginPath.'/platform/'.$this->platformString;
            //echo "\npluginPath: ".$pluginPath;
            array_push($pluginsPlatformPathArray, $pluginPath);
        }
        //var_dump($pluginsPlatformPathArray);
        return $pluginsPlatformPathArray;
    }

    /**
     * Get all manifest filenames in plugin areaa
     * 
     * TODO TEST
     * @return array string absolute paths of plugin manifests
     */
    public function getPluginsManifestPathArray()
    {
        $pluginDirectoryArray = $this->filesystemUtils->getDirectoryContentArray($this->getPluginPathAbsolute());
        //var_dump($pluginDirectoryArray);
        $pluginsPathArray = [];
        foreach($pluginDirectoryArray as $pluginDirectory) 
        {
            $pluginPath = $this->getPluginPathAbsolute()."/".$pluginDirectory.'/manifest.json';
            array_push($pluginsPathArray, $pluginPath);
        }
        //var_dump($pluginsPathArray);
        return $pluginsPathArray;
    }

    /**
     * Get basic wbm manifest.
     * 
     * @return array object converted manifest data of basic wbm.
     * 
     */
    public function getBasicWbmManifest()
    {
        // already read? Just return
        if($this->basicWbmManifest)
        {
            return $this->getBasicWbmManifest;
        }

        $basicWbmManifestPath = $this->filesystemUtils->GetWbmPathAbsolute().'/manifest.json';
        $basicWbmManifestContent = $this->filesystemUtils->getFileContents([ $basicWbmManifestPath ])[0];
        //var_dump($basicWbmManifestContent);

        if(isset($basicWbmManifestContent->error))
        {
            // log detail error information (with filepath) and return less details
            $this->errorLogger->logError($basicWbmManifestContent->error);
            $error = new WBMError(ERROR_GROUP_PLUGINS, ERROR_CODE_READ_PLUGIN_MANIFEST_FILE, "Can't read basic wbm manifest");
            return $error;
        }
        else if(isset($basicWbmManifestContent->content))
        {
            $this->basicWbmManifest = json_decode($basicWbmManifestContent->content);
            if(!$this->basicWbmManifest)
            {
                // log error with detail file information and return without
                $error = new WBMError(ERROR_GROUP_PLUGINS, ERROR_CODE_PLUGIN_MANIFEST_FORMAT, "Base wbm manifest format error ".$basicWbmManifestContent->filepath);
                $this->errorLogger->logError($error);
                $error->text = "Basic wbm manifest format error";
                return $error;
            }
        }
        //var_dump($this->basicWbmManifest);
        return $this->basicWbmManifest;
    }

    
    /**
     * Convert original manifest file content to format for frontend:
     * - convert to json, make checks and add pathes to filenames.
     * 
     * @param array object original manifest data (filenames and filecontents)
     *                     only given in case of unit test!!!
     * 
     * @return array object converted manifest data
     */
    public function convertManifestContentsToResponseObject($manifestContents = false)
    {
        // if manifest contents are not given (in unit test), get them from filesystem
        if(!$manifestContents)
        {
            $manifestContents = $this->filesystemUtils->getFileContents($this->getPluginsManifestPathArray());        
        }

        //var_dump($manifestContents);
        $manifestObjectsArray = array();
        foreach($manifestContents as $manifestContentData)
        {
            //echo "\nmanifestContentData: "; var_dump($manifestContentData);
            if(isset($manifestContentData->error))
            {
                // log detail error information (with filepath) and return less details
                $this->errorLogger->logError($manifestContentData->error);
                $error = new WBMError(ERROR_GROUP_PLUGINS, ERROR_CODE_READ_PLUGIN_MANIFEST_FILE, "Can't read plugin manifest");
                // transfer error object to the same strukture as parameter infos for equal treatment later
                $manifestObjectsArray = array_merge($manifestObjectsArray, array(0 => (object) array("error" => $error)));
            }
            else if(isset($manifestContentData->content))
            {
                $manifest = json_decode($manifestContentData->content);
                if(!$manifest)
                {
                    // log error with detail file information and return without
                    $error = new WBMError(ERROR_GROUP_PLUGINS, ERROR_CODE_PLUGIN_MANIFEST_FORMAT, "Plugin manifest format error ".$manifestContentData->filepath);
                    $this->errorLogger->logError($error);
                    $error->text = "Plugin manifest format error";
                    $manifestObjectsArray = array_merge($manifestObjectsArray, array(0 => (object) array("error" => $error)));
                }
                else
                {
                    $manifestObject = $this->convertManifestToResponseObject($manifest, $manifestContentData->filepath);
                    array_push($manifestObjectsArray, $manifestObject);
                }
            }
        }
        //var_dump($manifestObjectsArray);
        return $manifestObjectsArray;
    }


    /**
     * Convert original manifest data to write format for frontend:
     * - add filepath (without document root) to all filenames
     * 
     * If other checks of manifest content should be executed in future, this is the right place
     * 
     * @param object original manifest data
     * @param string path of original manifest file
     * 
     * @return object converted manifest data
     */
    private function convertManifestToResponseObject($originalManifest, $manifestPath)
    {
        $manifest = clone($originalManifest);
        //var_dump($manifest);

        // get path to manifest for url - without absolute path string to our wbm document root and without plugin file name
        $pluginRequestPath = str_replace($_SERVER["DOCUMENT_ROOT"], '', $manifestPath);
        $pluginRequestPath = str_replace('/'.basename($pluginRequestPath), '', $pluginRequestPath);
        //echo "\npluginRequestPath: ".$pluginRequestPath;

        if(isset($manifest->files)) foreach($manifest->files as $key => $file)
        {
            //var_dump($file);
            $manifest->files[$key] = $pluginRequestPath."/".$file;
        }
        //echo "\nmanifest: "; var_dump($manifest);
        return $manifest;
    }

    /**
     * Get several plugin data.
     * 
     * @param array object original manifest data (filenames and filecontents)
     *                     only given in case of unit test!!!
     * 
     * @return array object plugin data
     */
    public function getPluginData($manifestContents = false)
    {
        // if manifest contents are not given (in unit test), get them from filesystem
        if(!$manifestContents)
        {
            // get path of basic wbm manifest and of all plugin manifests (together in one array) and read all of them
            $manifestPathArray = $this->getPluginsManifestPathArray();
            $basicWbmManifestPath = $this->filesystemUtils->GetWbmPathAbsolute().'/manifest.json';
            array_push($manifestPathArray, $basicWbmManifestPath);

            $manifestContents = $this->filesystemUtils->getFileContents($manifestPathArray);        
        }

        $pluginDataArray = array();
        foreach($manifestContents as $manifestContentData)
        {
            if(isset($manifestContentData->error))
            {
                // log detail error information (with filepath) and return less details
                $this->errorLogger->logError($manifestContentData->error);
                $error = new WBMError(ERROR_GROUP_PLUGINS, ERROR_CODE_READ_PLUGIN_MANIFEST_FILE, "Can't read plugin manifest");
                // transfer error object to the same strukture as parameter infos for equal treatment later
                $pluginDataArray = array_merge($pluginDataArray, array(0 => (object) array("error" => $error)));
            }
            else if(isset($manifestContentData->content))
            {
                $manifest = json_decode($manifestContentData->content);
                if(!$manifest)
                {
                    // log error with detail file information and return without
                    $error = new WBMError(ERROR_GROUP_PLUGINS, ERROR_CODE_PLUGIN_MANIFEST_FORMAT, "Plugin manifest format error ".$manifestContentData->filepath);
                    $this->errorLogger->logError($error);
                    $error->text = "Plugin manifest format error";
                    $pluginDataArray = array_merge($pluginDataArray, array(0 => (object) array("error" => $error)));
                }
                else
                {
                    // evaluate path to transform functions via path to manifest file
                    $transformsDirectory = str_replace("manifest.json", "platform/".$this->platformString."/transforms", $manifestContentData->filepath);
                    
                    $manifestObject = [
                        (object) [
                            "manifest" => $manifest,
                            "manifestFilepath" => $manifestContentData->filepath,
                            "transformsDirectory" => $transformsDirectory
                        ]
                    ];
                    array_push($pluginDataArray, $manifestObject);
                }
            }
        }
        //var_dump($pluginDataArray);
        return $pluginDataArray;
    }


    public function getPluginDataViaPluginName($pluginName)
    {
        $resultPluginData = false;

        // get plugin data array initially, if it was not requested by now (once is enough, plugin data does not change)
        if(!$this->pluginDataArray)
        {
            $this->pluginDataArray = $this->getPluginData();
        }

        //var_dump($pluginDataArray);
        //var_dump($this->pluginDataArray[30]);//->manifest->name);
        foreach($this->pluginDataArray as $singlePluginData)
        {
            //var_dump($singlePluginData[0]->manifest->name);
            if(strcmp($singlePluginData[0]->manifest->name, $pluginName) == 0)
            {
                $resultPluginData = $singlePluginData[0];
                break;
            }
        }
        //var_dump($resultPluginData);
        return $resultPluginData;
    }

}

