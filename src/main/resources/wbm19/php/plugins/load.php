<?php

include_once __DIR__.'/pluginloader.inc.php';
include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';
include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/../authentication/wbm_session.inc.php';

$request = (object) [];
$pluginloaderResponse = (object) [];

// get request json string by stdin
$requestString  = file_get_contents("php://input");

if($requestString)
{
  $request = json_decode($requestString, false);
}

if(($requestString === false) || ($request === NULL))
{
  $error = new WBMError(ERROR_GROUP_MISC, ERROR_CODE_INVALID_INPUT, "Wrong request data format"); 
  $pluginloaderResponse->error = $error;
}
else
{
  // get (optional) input parameter from request or use default values
  $pluginPath = isset($request->pluginPath) ? $request->pluginPath : false;

  // initialize objects
  $errorLogger      = new ErrorLogger();
  $filesystemUtils  = new FilesystemUtils();
  $wbmSession       = new WbmSession($errorLogger, $filesystemUtils);
  $pluginloader     = new Pluginloader($pluginPath, $filesystemUtils, $errorLogger);

  // handle wmb session
  $checkWbmSessionResult = $wbmSession->handleWbmSession($request, $pluginloaderResponse);

  if(!$checkWbmSessionResult instanceof WBMError)
  {
      $manifestObjects = $pluginloader->convertManifestContentsToResponseObject($manifestContents);
      //echo "\nmanifestObjects:"; var_dump($manifestObjects);

      $pluginloaderResponse->plugins = $manifestObjects;
  }
}

// convert response object to output format
$pluginloaderResponseString = json_encode($pluginloaderResponse);
echo $pluginloaderResponseString;


