<?php namespace response;

function die_with_response($code, $object) {
    \http_response_code(200);
    echo json_encode($object, JSON_UNESCAPED_SLASHES);
    die;
}