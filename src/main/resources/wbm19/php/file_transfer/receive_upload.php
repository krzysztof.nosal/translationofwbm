<?php 

include_once __DIR__.'/response/response.inc.php';
include_once __DIR__.'/file_transfer.inc.php';
include_once __DIR__.'/file_upload.inc.php';

include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';
include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/../authentication/wbm_session.inc.php';

use function response\die_with_response;
use transfer\FileTransfer;
use transfer\FileUpload;

// initialize objects
$errorLogger      = new ErrorLogger();
$filesystemUtils  = new FilesystemUtils();
$wbmSession       = new WbmSession($errorLogger, $filesystemUtils);


$dummyResponse = (object) [];

// handle wmb session
$checkWbmSessionResult = $wbmSession->handleWbmSession(false, $dummyResponse);
if($checkWbmSessionResult instanceof WBMError)
{
    $errorLogger->logError($checkWbmSessionResult);
    die_with_response(200, [
        'error' => $checkWbmSessionResult
    ]);
}


if ('admin' != $_SESSION['username']) {
    $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_TRANSFER_NOT_ALLOWED, "Access not allowed");
    $errorLogger->logError($error);
    die_with_response(200, [
        'error' => $error
    ]);
}

$transfer;
$transferToken = HttpUtils::getRequestHeaderParam('Com-Wago-Transfer-Token');
if ($transferToken) {
    $transfer = FileTransfer::reopen($transferToken);
    if (!$transfer) {
        $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_TRANSFER_NOT_EXISTING, "No Transfer for provided token");
        $errorLogger->logError($error);
        die_with_response(200, [
            'error' => $error
        ]);
    }
} else {
    $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_TRANFER_TOKEN_MISSING, "No transfer token provided");
    $errorLogger->logError($error);
    die_with_response(200, [
        'error' => $error
    ]);
}


$upload;
$uploadToken = HttpUtils::getRequestHeaderParam('Com-Wago-Upload-Token');
if ($uploadToken) {
    $upload = FileUpload::resume($uploadToken);
    if (!$upload) {
        $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_UPLOAD_NOT_EXISTING, "No upload for provided token");
        $errorLogger->logError($error);
        die_with_response(200, [
            'error' => $error
        ]);
    }
} else {
    $name = HttpUtils::getRequestHeaderParam('Com-Wago-Upload-Name');
    if (!$name) {
        $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_UPLOAD_NAME_MISSING, "No upload name provided");
        $errorLogger->logError($error);
        die_with_response(200, [
            'error' => $error
        ]);
    }
    $size = HttpUtils::getRequestHeaderParam('Com-Wago-Upload-Size');
    if (!$size) {
        $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_UPLOAD_SIZE_MISSING, "No upload size provided");
        $errorLogger->logError($error);
        die_with_response(200, [
            'error' => $error
        ]);
    }

    $destination = HttpUtils::getRequestHeaderParam('Com-Wago-Upload-Destination'); // optional
    
    $upload = FileUpload::create($transfer, $name, $size, $destination);
    if (!$upload) {
        $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_UPLOAD_INIT_FAILED, "Upload could not be created (too large?)");
        $errorLogger->logError($error);
        die_with_response(200, [
            'error' => $error
        ]);
    }
}



$bytes = file_get_contents('php://input');
if (false == $upload->process($bytes)) {
    if ($upload->finalize() != false) {
        $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_UPLOAD_COMPLETED, "Upload already completed");
        $errorLogger->logError($error);
        die_with_response(200, [
            'error' => $error
        ]);
    }
    $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_UPLOAD_FAILED, "Upload cannot be processed");
    $errorLogger->logError($error);
    die_with_response(200, [
        'error' => $error
    ]);
}


if ($path = $upload->finalize()) {
    die_with_response(200, [
        'status' => 0,
        'uploadPath' => $path
    ]);
} else {
    die_with_response(200, [
        'status' => 0,
        'uploadToken' => $upload->token()
    ]);
}