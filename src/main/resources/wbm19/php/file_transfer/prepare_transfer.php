<?php

include_once __DIR__.'/response/response.inc.php';
include_once __DIR__.'/file_transfer.inc.php';


include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';
include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/../authentication/wbm_session.inc.php';


use function response\die_with_response;
use transfer\FileTransfer;


// initialize objects
$errorLogger      = new ErrorLogger();
$filesystemUtils  = new FilesystemUtils();
$wbmSession       = new WbmSession($errorLogger, $filesystemUtils);

$dummyResponse = (object) [];

// handle wmb session
$checkWbmSessionResult = $wbmSession->handleWbmSession(false, $dummyResponse);
if($checkWbmSessionResult instanceof WBMError)
{
    $errorLogger->logError($checkWbmSessionResult);
    die_with_response(200, [
        'error' => $checkWbmSessionResult
    ]);
}


if ('admin' != $_SESSION['username']) {
    $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_TRANSFER_NOT_ALLOWED, "Access not allowed");
    $errorLogger->logError($error);
    die_with_response(200, [
        'error' => $error
    ]);
}


$size = HttpUtils::getRequestHeaderParam('Com-Wago-Transfer-Size');
if (!$size) {
    $size = 0;
} 
$transfer = FileTransfer::create($size);
    
if (!$transfer) {
    $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_NOT_ENOUGH_MEMORY, "Not enough memory available"); 
    $errorLogger->logError($error);
    die_with_response(200, [
        'error' => $error
    ]);
}

die_with_response(200, [
    'uploadChunkSize' => $transfer->maxPostSizeInByte(),
    'transferToken' => $transfer->token(),
    'transferPath' => $transfer->path()
]);