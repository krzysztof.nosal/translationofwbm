<?php

include_once __DIR__.'/response/response.inc.php';
include_once __DIR__.'/file_transfer.inc.php';

include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../error_handling/logger.inc.php';
include_once __DIR__.'/../utils/filesystem.inc.php';
include_once __DIR__.'/../authentication/wbm_session.inc.php';


use function response\die_with_response;
use transfer\FileTransfer;

// initialize objects
$errorLogger      = new ErrorLogger();
$filesystemUtils  = new FilesystemUtils();
$wbmSession       = new WbmSession($errorLogger, $filesystemUtils);

$dummyResponse = (object) [];

// handle wmb session
$checkWbmSessionResult = $wbmSession->handleWbmSession(false, $dummyResponse);
if($checkWbmSessionResult instanceof WBMError)
{
    $errorLogger->logError($checkWbmSessionResult);
    die_with_response(200, [
        'error' => $checkWbmSessionResult
    ]);
}


if ('admin' != $_SESSION['username']) {
    $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_TRANSFER_NOT_ALLOWED, "Access not allowed");
    $errorLogger->logError($error);
    die_with_response(200, [
        'error' => $error
    ]);
}

    
$token = HttpUtils::getRequestHeaderParam('Com-Wago-Transfer-Token');
if (!$token) {
    $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_TRANFER_TOKEN_MISSING, "No transfer token provided");
    $errorLogger->logError($error);
    die_with_response(200, [
        'error' => $error
    ]);
}
$transfer = FileTransfer::reopen($token);
if (!$transfer) {
    $error = new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_TRANSFER_NOT_EXISTING, "No Transfer for provided token");
    $errorLogger->logError($error);
    die_with_response(200, [
        'error' => $error
    ]);
}

$transfer->cleanup();

die_with_response(200, [
    'status' => 0
]);