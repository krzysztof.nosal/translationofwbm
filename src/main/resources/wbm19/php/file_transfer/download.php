<?php

include_once __DIR__.'/../error_handling/error_definitions.inc.php';
include_once __DIR__.'/../authentication/wbm_session.inc.php';
include_once __DIR__.'/../authentication/user.inc.php';
include_once __DIR__.'/response/response.inc.php';
include_once __DIR__.'/file_transfer.inc.php';


use function response\die_with_response;
use transfer\FileTransfer;


// initialize objects
$errorLogger      = new ErrorLogger();
$filesystemUtils  = new FilesystemUtils();
$wbmSession       = new WbmSession($errorLogger, $filesystemUtils);

$authenticationRequest = (object) [];
$authenticationRequest->csrfToken = $_GET['csrf'];
$authenticationRequest->renewSession = true;
$response = (object) [];

// handle wmb session
$checkWbmSessionResult = $wbmSession->handleWbmSession($authenticationRequest, $response);

if ('admin' != $_SESSION['username']) {
    die_with_response(200, [
        'error' => new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_TRANSFER_NOT_ALLOWED, 'Not allowed')
    ]);
}

// check authentication
if ($response->error instanceof WBMError) {
    die_with_response(200, $response);
}

$downloadPath = '';

if (isset($_GET['download'])) {
    $downloadPath = $_GET['download'];

    // check file exists
    if(!file_exists($downloadPath))
    {
        die_with_response(200, [
            'error' => new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_DOWNLOAD_FILE_DOES_NOT_EXIST, 'File does not exist')
        ]);
    }

    // check access rights
    if(!is_readable($downloadPath))
    {
        die_with_response(200, [
            'error' => new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_DOWNLOAD_FILE_NOT_READABLE, 'File is not readable')
        ]);
    }
}
else if(isset($_GET['transfer'])) {
    $transferToken = $_GET['transfer'];
    $transfer = FileTransfer::reopen($transferToken);
    $downloadPath = $transfer->filepath();
}
else {
    die_with_response(200, [
        'error' => new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_DOWNLOAD_PATH_MISSING, 'Download path missing')
    ]);
}


$file = fopen($downloadPath, 'r');
if(!$file) {
    die_with_response(200, [
        'error' => new WBMError(ERROR_GROUP_FILE_TRANSFER, ERROR_CODE_DOWNLOAD_FILE_NOT_READABLE, 'File is not readable')
    ]);
}

// put fitting filetype to header
header("Content-Type: application/octet-stream");

// send header with proposal of fitting filename
header(sprintf("Content-Disposition: attachment; filename=\"%s\"",  basename($downloadPath)));

// send file size
header('Content-Length: '.filesize($downloadPath));

// before sending the file content, add a cookie to tell the frontend that the download will be started
setcookie("download($downloadPath)", 'started', 0, '/');

flush();

// read and send file chunkwise
while (!feof($file)) {
    print fread($file, 1024*1024); // read 1MiB...
    flush();                       // ... and send to browser
}
fclose($file);

// remove file as soon as the download is completed, if a deletion was requested
if(isset($_GET['transfer'])) {
    $transferToken = $_GET['transfer'];
    $transfer = FileTransfer::reopen($transferToken);
    $transfer->cleanup();
}