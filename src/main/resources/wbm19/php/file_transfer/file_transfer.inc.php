<?php namespace transfer;

include_once __DIR__.'/../utils/filesystem.inc.php';

use \FilesystemUtils;

class FileTransfer {


    /**
     * The indentifier used to identify a transfer. 
     */
    private $token = null;
    
    /**
     * The directory where the file transfer stores its files
     */
    private $path = null;

    /**
     * The directory where the file transfers $path is located in
     */
    private $basepath = null;

    /**
     * The size left for this transfer. if anything uses this transfer it should call the 
     * take($bytes) method to tell the transfer that some of its size is used. -1: no limit.
     */
    private $size = -1;

    /**
     * true, if this transfer is broken, which may occur, if a cleanup 
     * has failed.
     */
    private $broken = false;

    
    /**
     * true, if the transfer required to mount the extra partition
     */
    private $mounted = false;

    private $filesystemUtils;


    /** 
     * Create a file transfer object either by reference, when a token 
     * is given, or a new one, if none is given. 
     * 
     * @param object filesystem utils
     *        if not given (false), instance an own one
     */
    private function __construct($filesystemUtils = false) 
    { 
        $this->filesystemUtils = $filesystemUtils ? $filesystemUtils : new FilesystemUtils();
    }


    /**
     * Get maximum post size in byte. Take value from php.ini and convert it to byte count.
     * 
     * @param string maximum post size string from php ini (only for tests)
     * @return int maximum post size in byte 
     *             0, if post max size string is invalid
     *               
     */
    public function maxPostSizeInByte($iniPostMaxSize = false)
    {
        $maxPostSizeInByte = 0;
        if(!$iniPostMaxSize)
        {
            $iniPostMaxSize = ini_get('post_max_size');
        }
        if (preg_match('/K$/', $iniPostMaxSize)) 
        {
            $maxPostSizeInByte = intval($iniPostMaxSize) * 1024;
        } 
        else if (preg_match('/M$/', $iniPostMaxSize)) 
        {
            $maxPostSizeInByte = intval($iniPostMaxSize) * 1024 * 1024;
        } 
        else if (preg_match('/G$/', $iniPostMaxSize)) 
        {
            $maxPostSizeInByte = intval($iniPostMaxSize) * 1024 * 1024 * 1024;
        }
        return $maxPostSizeInByte;
    }
    

    /**
    * Creates a new file transfer by loading information about an existing 
    * transfer from an internal cache using the given token.
    * 
    * @param string token The token to be used to identify the to-be-loaded 
    *               transfer information
    *
    * @return FileTransfer|null
    */
    public static function reopen($token) {

        // no cached transfer with this token
        if (!isset($_SESSION['file_transfer']) || !isset($_SESSION['file_transfer'][$token])) 
            return null;

        $transfer = new FileTransfer();
        $transfer->token = $token;
        $transfer->loadFromCache();
        
        return $transfer;
    }



    /**
    * Creates a new file transfer. In dependency of the size 
    * of the transfer a different setup may apply. 
    *
    * @param int size the overall size of the transfer
    *
    * @return FileTransfer|null
    */
    public static function create($size) {

        $transfer = new FileTransfer();

        if ($size == 0) {
            $size = -1; // unlimited
        }

        $transfer->size = $size;

        // setup cache object if not present
        if (!isset($_SESSION['file_transfer']))
            $_SESSION['file_transfer'] = [];

        // generate some unique token using the current timstamp
        do {
            $token = \str_pad((string)microtime(true), 16, '0');
        } while (isset($_SESSION['file_transfer'][$token]));
        $transfer->token = $token;
        
        $basepath;
        if ($size == -1 || $size > $transfer->maxPostSizeInByte()) {
            if ($basepath = $transfer->mount()) {
                $basepath = $basepath . '/transfer';
                $transfer->mounted = true;
            } else {
                // ERROR mount failed (not enough memory) 
                return null;
            }
        } else {
            $basepath = '/tmp/transfer';
        }
        $transfer->basepath = $basepath;
        $transfer->path = "$basepath/$token";
        

        // create the directory. If that fails, the transfer is not created
        if($transfer->filesystemUtils->createDirectory($transfer->path) instanceof WBMError)
        {
            return null;
        }

        $transfer->saveToCache();
        return $transfer;
    }



    
    private function mount() {
        $path = null;

        // check if partition is mounted by ommiting the -p (--prepare) argument               
        $result = exec('sudo /etc/config-tools/file_transfer -q -d -j -a', $_, $status);       
        $result = json_decode($result, true);                                                  
                                                                                               
        if ($status != 0 || $result['FileTransferPath'] == 'none' ) {                          
            // try to mount, if failed or not mounted yet                                      
            $result = exec('sudo /etc/config-tools/file_transfer -q -p -d -j -a', $_, $status);
            $result = json_decode($result, true);                                              
        }                                                                                      
                                                                                               
        if ($status == 0) {                                                       
                                                                                  
            if ($this->size != -1 && $result['AvailableCapacity'] < $this->size) {
                $this->unmount();                                                 
                $path = null;                                                     
            } else {
                $path = $result['FileTransferPath'];
                
                // increment mount counter
                $file = fopen("$path/TRANSFER", 'c+');
                flock($file, LOCK_EX);
                $count = intval(fread($file, filesize("$path/TRANSFER")));
                $count += 1;
                ftruncate($file, 0);
                rewind($file);
                fwrite($file, "$count");
                flock($file, LOCK_UN);
                fclose($file);
            }
        }
        return $path;
    }

    private function unmount() {
        $path = dirname($this->basepath);

        // open counter
        $file = fopen("$path/TRANSFER", 'c+');
        flock($file, LOCK_EX);
        $count = intval(fread($file, filesize("$path/TRANSFER")));
        $count -= 1;

        if ($count > 0) {
            // don't unmount, but decrement a counter
            ftruncate($file, 0);
            rewind($file);
            fwrite($file, "$count");
            flock($file, LOCK_UN);
            fclose($file);
            return true;

        } else {
            flock($file, LOCK_UN);
            fclose($file);

            // unmount
            $result = exec('sudo /etc/config-tools/file_transfer -q -c', $_, $status);
            if ($status == 0) {
                return true;
            } else {
                return false; // ERROR: should have been unmounted, but failed
            }
        }
    }

    private function loadFromCache() {
        $cache = $_SESSION['file_transfer'][$this->token];
        $this->path = $cache['path'];
        $this->basepath = $cache['basepath'];
        $this->broken = $cache['broken'];
        $this->size = $cache['size'];
        $this->mounted = $cache['mounted'];
    }

    private function saveToCache() {
        $_SESSION['file_transfer'][$this->token] = [
            'path' => $this->path,
            'basepath' => $this->basepath,
            'size' => $this->size,
            'broken' => $this->broken,
            'mounted' => $this->mounted
        ];
    }


    public function takeBytes($count) {
        if ($this->size == -1) {
            // take from infinite size
            return true;

        } else if ($this->availableBytes() < $bytes) { 
            return false;
        } else {
            $this->size -= $count;
            $this->saveToCache();
            return true;
        }
    }


    /**
     * Get the amount of bytes available to be stored in this transfers
     * context. If the value is negative, more bytes are stored, than reserved. 
     * However, this might not be a problem, as no prevention is applied to 
     * exceed the transfer size. 
     *
     * @return int number of bytes available 
     */
    public function availableBytes() {
        
        if ($this->size == -1) { return INF; }

        $dirSize = $this->filesystemUtils->sizeOfPath($this->path);
        return $this->size - $dirSize;
    }

    /**
     * Get the transfer path
     */
    public function path() {
        return $this->path;
    }

    /**
     * Generate a file path pointing to a file with a given name inside 
     * the base path of this transfer.
     * 
     * If no filename is provided, 
     */
    public function filepath($filename = false) {
        if(!$filename) {
            $existingFiles = $this->filesystemUtils->getFilepathsOfDirectory($this->path);
            if($existingFiles && $existingFiles[0]) {
                return $existingFiles[0];
            } else {
                return false;
            }
        }
        return "$this->path/$filename";
    }


    /**
     * Get the transfer token
     */
     public function token() {
        return $this->token;
    }


    /**
    * Perform cleanup steps if a transfer is not beeing in use any 
    * more. This always includes a removal of any files and directories
    * associated with the transfer. Also, if the cleanup succeeds,
    * the transfer will be removed from an internal cache, which will 
    * cause it to not beeing beeing re-openable any more. If the cleanup 
    * fails, the transfer is marked as beeing corrupted, but is still 
    * reopenable from cache.
    *
    * @return boolean true, if cleanup was successful, false otherwise
    */
    public function cleanup() {

        // clear whole path
        $this->filesystemUtils->removePath($this->path);

        if ($this->mounted) {
            $this->unmount();
        }

        // remove self from internal cache.
        unset($_SESSION['file_transfer'][$this->token]);
    }
}

