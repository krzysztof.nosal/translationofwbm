<?php

// DEPRECATED

// don't show any errors in output stream
error_reporting(0);

//------------------------------------------------------------------------------
// constants and global variables
//------------------------------------------------------------------------------

define("SUCCESS", "0");
define("ERROR", "-1");
define("ACCESS_NOT_ALLOWED", -2);

define("CF_CARD", "cf-card");
define("INTERNAL_FLASH", "internal-flash");
define("INTERNAL_FLASH_NAND", "internal-flash-nand");
define("INTERNAL_FLASH_EMMC", "internal-flash-emmc");
define("SD_CARD", "sd-card");
define("USB1", "usb1");
define("USB2", "usb2");
define("USB3", "usb3");
define("USB4", "usb4");
define("USB5", "usb5");
define("USB6", "usb6");
define("USB7", "usb7");
define("USB8", "usb8");
define("NETWORK", "network");
define("UNKNOWN", "unknown");


$status               = SUCCESS;
$errorText            = "";

