package translator;

import dictionary.WbmDictionary;
import files.DirectoryCrawler;
import regex_tools.RegexHandler;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileTranslator {
    private final WbmDictionary dictionary;
    private final RegexHandler regexHandler;
    private static final Logger LOGGER = Logger.getLogger(FileTranslator.class.getName());

    public FileTranslator(WbmDictionary dictionary, RegexHandler regexHandler) {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT] [%4$-7s] %5$s %n");
        LOGGER.setLevel(Level.INFO);
        this.regexHandler = regexHandler;
        this.dictionary = dictionary;
    }

    private void getNewKeys(String path) {
        String content = DirectoryCrawler.getFileContent(path);
        if (content == null) return;
        for (var i = 0; i < regexHandler.size(); i++) {
            var matcher = regexHandler.getMatcher(i, content);
            while (matcher.find()) {
                if (!dictionary.getDict().containsKey(matcher.group())) {
                    dictionary.putNewKey(matcher.group());
                    var foundPhrase = matcher.group();
                    var loggerMessage = "new key: " + foundPhrase;
                    LOGGER.info(loggerMessage);
                }
            }
        }
    }

    private void translate(String path) {
        String content = DirectoryCrawler.getFileContent(path);
        if (content == null) return;

        for (var i = 0; i < regexHandler.size(); i++) {
            var matcher = regexHandler.getMatcher(i, content);
            while (matcher.find()) {
                if (dictionary.getDict().containsKey(matcher.group())) {
                    var stringBuilder = new StringBuilder();
                    stringBuilder.append(content.substring(0, matcher.start()));
                    stringBuilder.append(dictionary.getTranslation(matcher.group()));
                    stringBuilder.append(content.substring(matcher.end()));
                    if (!matcher.group().equals(dictionary.getTranslation(matcher.group()))) {
                        content = stringBuilder.toString();
                        var translatedPhrase = matcher.group();
                        var loggerMessage = "translated: " + translatedPhrase;
                        LOGGER.finest(loggerMessage);
                        matcher = regexHandler.getMatcher(i, content);
                    }
                }
            }
        }
        try {
            Files.delete(Paths.get(path));
        } catch (Exception e) {
            LOGGER.severe("delete error");
        }
        try {
            Files.writeString(Paths.get(path), content, StandardOpenOption.CREATE_NEW);
        } catch (Exception e) {
            LOGGER.severe("save error");
        }
    }

    public void translateDirectory(String path) {
        List<String> directories;
        directories = DirectoryCrawler.getDirectories(path);
        if (directories != null) {
            for (String entry : directories) {
                translate(entry);
            }
        }
    }

    public void getNewKeysFromDirectory(String path) {
        List<String> directories;
        directories = DirectoryCrawler.getDirectories(path);
        if (directories != null) {
            for (String entry : directories) {
                getNewKeys(entry);
            }
        }
    }
}
