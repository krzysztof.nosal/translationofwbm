package main;

import dictionary.WbmDictionary;
import files.DirectoryCrawler;
import files.FileDatabase;
import translator.FileTranslator;

import java.io.File;
import java.io.IOException;


public class Main {



    public static void main(String[] args) {

        var currentPathString="";
        try {
            currentPathString = new File(".").getCanonicalPath();
        }catch (Exception e){
            e.printStackTrace();
        }

         final String DICTIONARY_PATH=currentPathString+"\\src\\main\\resources\\dict\\slownik.txt";
         final String REGEX_PATH=currentPathString+"\\src\\main\\resources\\regex.txt";
         final String SOURCE_WBM_PATH=currentPathString+"\\src\\main\\resources\\wbm19";
         final String DESTINATION_WBM_PATH=currentPathString+"\\src\\main\\resources\\wbm";

        var regexHandler = FileDatabase.getRegexHandlerFromFile(REGEX_PATH);
        WbmDictionary dictionary= FileDatabase.getDictionaryFromFile(DICTIONARY_PATH);
        var directoryCrawler=new DirectoryCrawler(SOURCE_WBM_PATH,DESTINATION_WBM_PATH);

        try {
            directoryCrawler.createCopyOfOriginalSite();
        }catch (IOException e){
            e.printStackTrace();
        }
        var fileTranslator = new FileTranslator(dictionary,regexHandler);
        fileTranslator.getNewKeysFromDirectory(DESTINATION_WBM_PATH);
        FileDatabase.saveDictionaryToFile(DICTIONARY_PATH,dictionary);
        fileTranslator.translateDirectory(DESTINATION_WBM_PATH);

    }
}
