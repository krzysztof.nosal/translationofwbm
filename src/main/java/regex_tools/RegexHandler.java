package regex_tools;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexHandler {
    private List<Pattern> regexList;

    public int size() {
        return regexList.size();
    }

    public RegexHandler(List<Pattern> regexList) {
        this.regexList = regexList;
    }

    public RegexHandler() {
        this.regexList = new LinkedList<>();
    }

    public void addRegex(String regexString) {
        regexList.add(Pattern.compile(regexString));
    }

    public Matcher getMatcher(int number, String content) {
        if (number < 0 || number > regexList.size()) {
            return null;
        } else {
            return regexList.get(number).matcher(content);
        }
    }

}
