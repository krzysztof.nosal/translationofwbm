package dictionary;

import java.util.LinkedHashMap;
import java.util.Map;

public class WbmDictionary {
    private Map<String, String> dict;

    public WbmDictionary() {
        this.dict =new LinkedHashMap<>();
    }
    public WbmDictionary(Map<String, String> dictionary) {
        this.dict =dictionary;
    }

    public Map<String, String> getDict() {
        return dict;
    }

    public void setDict(Map<String, String> dict) {
        this.dict = dict;
    }

    public String getTranslation(String key){
        return dict.get(key);
    }

    public boolean putNewKey(String key){
        if(dict.containsKey(key)){
            return false;
        }
        dict.put(key, key);
        return  true;
    }

    public boolean putNewKey(String key, String value){
        if(dict.containsKey(key)){
            return false;
        }
        dict.put(key, value);
        return  true;
    }

    public boolean setNewTranslation(String key, String value){
        if(!dict.containsKey(key)){
            return false;
        }
        dict.replace(key, dict.get(key),value);
        return  true;
    }

    public String printDictionary(){
        var stringBuilder = new StringBuilder();
        Object[] keys = dict.keySet().toArray();
        for (var i = 0; i < dict.size(); i++) {
            stringBuilder.append(keys[i].toString());
            stringBuilder.append(">>>>>");
            stringBuilder.append(dict.get(keys[i].toString()));
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
