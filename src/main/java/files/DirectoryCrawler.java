package files;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import org.apache.commons.io.FileUtils;

public class DirectoryCrawler {
    private final String originalDirectory;
    private final String masterDirectory;

    public DirectoryCrawler(String originalDirectory, String masterDirectory) {
        this.originalDirectory = originalDirectory;
        this.masterDirectory = masterDirectory;
    }

    public static List<String> getDirectories(String masterDirectory) {
        var masterDirectoryPath = Paths.get(masterDirectory);
        if(!Files.exists(masterDirectoryPath))return null;
        List<String> listOfPaths=null;
        try (Stream<Path> stream = Files.walk(masterDirectoryPath)) {

            Object[] paths = stream.toArray();
            listOfPaths = new LinkedList<>();
            for (Object object : paths) {
                listOfPaths.add(object.toString());
            }
            return listOfPaths;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfPaths;
    }

    public List<String> getDirectories()  {
        var masterDirectoryPath = Paths.get(this.masterDirectory);
        if(!Files.exists(masterDirectoryPath))return null;
        List<String> listOfPaths=null;
        try (Stream<Path> stream = Files.walk(masterDirectoryPath)) {

            Object[] paths = stream.toArray();
            listOfPaths = new LinkedList<>();
            for (Object object : paths) {
                listOfPaths.add(object.toString());
            }
            return listOfPaths;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfPaths;
    }

    public static String getFileExtension(String path) {
        var file = new File(path);
        if (file.isDirectory()) {
            return null;
        }
        var pattern = Pattern.compile("(?<=[.]).+$");
        var matcher = pattern.matcher(file.getName());
        if (matcher.find()) {
            return matcher.group();
        } else {
            return null;
        }
    }

    public static String getFileContent(String path) {
        var file = new File(path);
        if (!file.exists()) return null;
        if (file.isDirectory()) {
            return null;
        }
        try {
            return Files.readString(Paths.get(path), StandardCharsets.UTF_8);
        } catch (Exception e) {
            return null;
        }
    }

    public void createCopyOfOriginalSite() throws IOException {

        var src = new File(originalDirectory);
        var dest = new File(masterDirectory);

        FileUtils.deleteDirectory(dest);
        FileUtils.copyDirectory(src,dest);


    }
}
