package files;

import dictionary.WbmDictionary;
import regex_tools.RegexHandler;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;


public class FileDatabase {
    private FileDatabase(){

    }

    public static RegexHandler getRegexHandlerFromFile(String path) {
        String content = null;
        content = DirectoryCrawler.getFileContent(path);


        var regexHandler = new RegexHandler();
        if (content != null) {
            String[] rawEntries = content.split("###\\r?\\n");
            for (String entry : rawEntries) {
                regexHandler.addRegex(entry);
            }
        }
        return regexHandler;
    }

    public static WbmDictionary getDictionaryFromFile(String path) {
        String content = null;
        content = DirectoryCrawler.getFileContent(path);

        var dictionary = new WbmDictionary();
        if (content != null) {
            String[] rawEntries = content.split("\\r?\\n");
            for (String entry : rawEntries) {

                String[] singleValues = entry.split("\\$");
                dictionary.putNewKey(singleValues[0], singleValues[1]);
            }
        }
        return dictionary;
    }

    public static void saveDictionaryToFile(String path, WbmDictionary dictionary) {
        var stringBuilder = new StringBuilder();
        Object[] keys = dictionary.getDict().keySet().toArray();
        for (var i = 0; i < dictionary.getDict().size(); i++) {
            stringBuilder.append(keys[i].toString());
            stringBuilder.append("$");
            stringBuilder.append(dictionary.getDict().get(keys[i].toString()));
            stringBuilder.append("\n");
        }
        try {
            Files.deleteIfExists(Paths.get(path));
            Files.writeString(Paths.get(path), stringBuilder.toString(), StandardOpenOption.CREATE_NEW);
        } catch (Exception e) {
           e.printStackTrace();
        }
    }
}
