Program is designed to translate config web page of WAGO PLC controller.

Page is based on numerous php,html and js files. Labels for all elements are therefore hardly accesible.

There is a need to translate it into Polish several times a year(as new FW appears).

There is no possibility to prepare page in a way that allows multiple languages so another solution has to be found.

Program searches all files in specified directory for some regular expressions and tries to translate it using
previously prepared dictionary. 

If there is no matching key in dictionary, new one is added so it can be translated.
