package files;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;

class DirectoryCrawlerTest {

    @Test
    @DisplayName("Test")
    void shouldDetectJsonFile(){
        String path = "D:\\__JAVA\\test.json";
        try{
            Files.writeString(Paths.get(path), "test", StandardOpenOption.CREATE);
        }catch (Exception e){

        }
        while(!Files.exists(Paths.get(path))){

        }
        assertThat(DirectoryCrawler.getFileExtension(path)).isEqualTo("json");
        path = "D:\\__JAVA\\testFolder";
        assertThat(DirectoryCrawler.getFileExtension(path)).isNull();

    }

}
